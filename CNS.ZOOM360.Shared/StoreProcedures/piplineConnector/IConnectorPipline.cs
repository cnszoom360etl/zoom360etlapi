﻿using CNS.ZOOM360.Entities.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Shared.StoreProcedures.piplineConnector
{
   public interface IConnectorPipeline
    {
        
            Task<string> save(ConnectorPiplineModel inputModel);
            Task<List<GETPIPELINEMODEL>> GetData(string pipelineId,string UserId,string WorkspaceId,string ClientId);
    }
}
