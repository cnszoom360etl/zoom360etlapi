﻿using CNS.ZOOM360.Entities.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Shared.StoreProcedures.Extraction
{
   public interface IExtraction
    {
        string CheckUserAccessRights(string UserId, string Account_Id, string Workspaceid, string Clientid, string connectorId);
    }
}
