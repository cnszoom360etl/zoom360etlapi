using CNS.ZOOM360.Entities.Model;
using CNS.ZOOM360.Entities.StoreProcedures.Connectors.Databases.SQL;
using CNS.ZOOM360.Entities.StoreProcedures.SourceDescriptionAndConfiguration;
using CNS.ZOOM360.Entities.StoreProcedures.Transformation;
using CNS.ZOOM360.Shared.StoreProcedures.Connector.Database.SQL.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Shared.StoreProcedures.Connector.Database.SQL
{
    public interface ISqlConnectorService
    {
        string SaveConnection(SourceAccountConnectionModel InputModel);
        string saveEmailAuthenticationForConnection(EmailAuthenticationSourceAccount InputModel);
        string SaveAccountDispaly(SourceAccountModel InputModal);
        Task<IEnumerable<UpdateConnectorListStep2>> GetUpdateConnectedSourceList(string Account_Id, string UserId, string WorkspaceId, string ClientId, string ConnectorId);
        string SaveSourceObjects(SourceObjectModel InputModel);
        Task<IEnumerable<GEtSourceObjectList>> GetSourceObjectList(GetSourceObjectListInput input);
        Task<List<UpdateSourceObjectGridModel>> UpdateGridSourceObjectList(GEtSourceObjectList inputs, string UserId, int Workspaceid, int Clientid, int ConnectorId);
        Task<IEnumerable<EntityObjectFieldsList>> GetObjectFieldsList(GetObjectFieldListInput input);
        Task<List<EntityObjectFieldsList>> UpdateEntityObjectField(UpdateEntityObjectFieldInputs input, string UserId, string Workspaceid, string Clientid, string ConnectorId, string objectName);
        string SaveExtracts(SaveExtractsInputs inputs);
        Task<IEnumerable<ExtractModel>> GetExtractData(GetExtractDataInputs Inputs);
        Task<List<SourceObjectGetList>> GetSourceObjectforUpdate(string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId);
        Task<List<UpdateConnectorListStep2>> GetConnectorData(string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId);
        Task<List<UpdateConnectorListStep2>> ETLGetConnectorData(int Account_Id, string UserId, string Workspaceid, string Clientid, int ConnectorId);

        Task<List<UpdateConnectorListStep2>> GetConnectorListForEdit(string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId);

        Task<List<UpdateConnectorListStep2>> GetConnectorDataForExtraction(extractionModelClass[] Account_Id, string UserId, string Workspaceid, string Clientid);
        List<UpdateConnectorListStep2> GetConnectorDataForDriveFiles(string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId);
        List<GEtSourceObjectList> GetAllMySqlData();
        Task<IEnumerable<TransformationFunctionModel>> getFunctionDetails(functionPerametersValueModel Input);
        string GetConnectorStatus(connectorStatusModel Input);
    }
}
