﻿using CNS.ZOOM360.Entities.StoreProcedures.ConfigureTransformation;
using CNS.ZOOM360.Entities.StoreProcedures.EnrichDynamicModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace CNS.ZOOM360.Shared.StoreProcedures.EnrichDynamivMetadata
{
   public interface IEnrichDynamicMetadata
    {
        public string getFunctionTemplate();
        public string GetEnrichScript(string user_id, string workspace_id, string client_id, string record);
        Task<string> SaveEnrichScript(EnrichDynamicModel collection);
        public string WriteArray(object[] arr);
        string getStatusForETL();
        public string UpdateEnrichScript(EnrichDynamicModel coll, string _id);
        string DeleteEnrichScript(string userId, string scriptid);
        public string GetScriptList(string userId, string workspaceId, string clientId);
        string GetContentMappedList(string userId, string workspaceId, string clientId ,string accountId , string EditscreenName);
        string GetColumnMappedList(string userId, string workspaceId, string clientId, string accountId, string EditscreenName);
        string GetDestinationMappedList(string userId, string workspaceId, string clientId, string accountId);
        List<dynamic> getscriptlistbyusingIds(function_IdsArray[] _functionIds);
        Task<string> GetDestinationList(Destination_Json[] _Des, string userId, string workspaceId, string clientId, string accountId);
        Task<string> SaveTransformation_Mapping_Loading_Script(JsonAppledFor_Mapping_Entirichment_Load collection,string accountId);
    }
}
