﻿using CNS.ZOOM360.Entities.StoreProcedures.AllExtract.extract_count;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Shared.StoreProcedures.AllExtract.ExtractionCount
{
   public interface IConnectionListExtration
    {
        Task<List<connectionListExtractionModel>> GetconnectionListExtraction(conListExtractionInputModel conListExtractionInput);
    }
}
