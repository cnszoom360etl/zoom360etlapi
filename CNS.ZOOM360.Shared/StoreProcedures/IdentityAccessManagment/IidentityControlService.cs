﻿using CNS.ZOOM360.Entities.StoreProcedures.IdentityAccessManagment;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Shared.StoreProcedures.IdentityAccessManagment
{
    public interface IidentityControlService
    {
        Task<string> SaveIdentityControlSetup(IdentityControlModel identityControlModel);
    }
}
