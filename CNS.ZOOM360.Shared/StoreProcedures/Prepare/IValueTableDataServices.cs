﻿using CNS.ZOOM360.Entities.StoreProcedures;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Shared.StoreProcedures
{
    public interface IValueTableDataServices
    {
        //Task<List<valueTableIDataModel>> GetValueTableData(valueTableInputModel valueTableInput);
        Task<List<valueTableIDataModel>> GetValueTableData(string userId,string workspaceId,string clientId,string valueTable);
        //////Task<string> SaveValueTable(saveValueTableModel _saveValueTableModel);

        Task<string> SaveValueTable(valueTableIDataModel valueTableIDataMode, string UserId, string workspaceId, string clientId, string valuetable, string ClientDate, string ClientTime, string ClientTimeZone);

    }
}
