﻿
using CNS.ZOOM360.Entities.StoreProcedures;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Shared.StoreProcedures
{
   public interface IvalueTableList
    {
        Task<List<valueTableListModel>> GetValueTablelist(valueTableInputModel valueTableInput);
    }
}
