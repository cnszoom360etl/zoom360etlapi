﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Shared.StoreProcedures.Common.Dto
{
  public  class FileModelListcs
    {
        [NotMapped]
        public string    userid { get; set; }
        [NotMapped]
        public string workspaceid { get; set; }
        [NotMapped]
        public string clientid { get; set; }
        [NotMapped]
        public string modeltype { get; set; }
        [Column("DROPDOWN_TEXT")]
        public string dropdownText { get; set; }
        [Column("DROPDOWN_VALUE")]
        public string dropdownValue { get; set; }
        
    }
}
