﻿using CNS.ZOOM360.Entities.StoreProcedures.ChangeLog;
using CNS.ZOOM360.Shared.StoreProcedures.AllExtract.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Shared.StoreProcedures.ChangeLog
{
    public interface IChangeLogService
    {
        Task<List<ChangeLogListModel>> getChangeLogList(ExtractListInputModel ListInputmodel);
    }
}
