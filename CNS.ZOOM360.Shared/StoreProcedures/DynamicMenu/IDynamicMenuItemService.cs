﻿using CNS.ZOOM360.Entities.StoreProcedures.DynamicMenu;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Shared.StoreProcedures.DynamicMenu
{
    public interface IDynamicMenuItemService
    {
        Task<List<MainMenuModel>> GetMenulist(string UserId, string WorkspaceId, string Client_Id,string Mode_Id, string SubUserId);
        List<SubMenusectionModel> GetSubMenuSection(string UserId, string WorkspaceId, string Client_Id, string MainMenuID, string TreeLevel, string TreeNode, string SubUserId);
        Task<List<SubMenusectionModel>> GetSubMenuSectionItems(string UserId, string WorkspaceId, string Client_Id, string MainMenuID, string TreeLevel, string TreeNode , string SubUserId);
        Task<List<sectionModelForSourceEditPages>> SourceListPagesSectionItems(string UserId, string SubUserId, string Client_Id, string WorkspaceId,  string MainMenuID, string TreeLevel, string TreeNode);
    }
}
