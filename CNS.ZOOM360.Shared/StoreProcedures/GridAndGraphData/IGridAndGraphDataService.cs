﻿using CNS.ZOOM360.Entities.StoreProcedures.GridAndGraphData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Shared.StoreProcedures.GridAndGraphData
{
   public interface IGridAndGraphDataService
    {
        Task<List<GraphDataModel>> getGraphData();
        Task<List<GridDataModel>> getGridData();
        Task<DataTable> dynamicGridData(string userID, string WorkSpaceId, string Client_Id, string analysisType);
        
    }
}
