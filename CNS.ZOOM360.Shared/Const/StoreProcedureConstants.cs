﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Shared.Const
{
    
    public static class StoreProcedureConstants
    {
        #region WorkSpaces
        public const string Sp_SaveChildWorkSpaces = "SAVECHILDWORKSPACES";
        public const string Sp_SaveWorkspaceSetup = "SAVEWORKSPACESETUP";
        public const string Sp_GetWorkspaceSetup = "GETWORKSPACESETUP";
        public const string Sp_GetWorkspaceSetupList = "GETWORKSPACELIST";
        public const string Sp_GetDropdownList = "GETDR0PDOWNLIST";
        public const string Sp_GetUAMDropdown = "GETUAMDROPDOWN";

        public const string Sp_TestDropList = "GETTESTINGLIST";
        #endregion

        #region //Prepare
        public const string Sp_GetDataQualityList = "GETDATAQUALITYLIST";
        public const string Sp_GetLookupList = "GETLOOKUPVALUELIST";
        public const string Sp_GetSourceAccountList = "GETSOURCEACCOUNTLIST";
        public const string Sp_getSourceObjectList = "GETSOURCEOBJECTLIST";
        public const string Sp_getObjectFieldsList = "GETOBJECTFIELDSLIST";
        public const string Sp_SaveDataLabeling = "SAVEDATALABELING";
        public const string Sp_GETVALUETABLELIST = "GETVALUETABLELIST";
        public const string Sp_GETVALUETABLEDATA = "GETVALUETABLEDATA";
        public const string Sp_SAVEVALUETABLEDATA = "SAVEVALUETABLEDATA";
        #endregion
        //extractionCount
        public const string Sp_GETEXTRACTIONCOUNT = "GETEXTRACTIONCOUNT";
        public const string Sp_GETCONNECTIONLIST = "GETCONNECTIONLIST"; 
        public const string Sp_GetuserLoginStatus = "GETUSERLOGINSTATUS";
        public const string Sp_GetuserLoginLogs = "GETUSERLOGINLOGS";
        //endextractioncount

        #region //Prepare
        public const string Sp_GETFILEMODELLIST = "GETFILEMODELLIST";
        public const string Sp_GETFIELDLISTLIST = "GETDR0PDOWNLIST";

        #endregion


        #region //checkRights For Extraction
        public const string Sp_CheckUserRights = "RUNEXTRACT";
        

        #endregion

        #region CalenderSetup
        public const string Sp_SaveCalenderSetup = "SAVECALENDERSETUP";
        #endregion

        #region // CurrencySetup
        public const string Sp_SaveCurrencySetup = "SAVECURRENCYSETUP";
        #endregion

        #region // DataGouvernance
        public const string Sp_SaveDataGovernance = "SAVEDATAGOVERNANCE";
        #endregion

        #region // DisplaySettings
        public const string Sp_SaveDisplaySettings = "SAVEDISPLAYSETTINGS";
        #endregion
        #region // NumeralSetup
        public const string Sp_SaveNumeralSetup = "SAVENUMERALSSETUP";
        #endregion

        #region // QuotaSetting
        public const string Sp_SaveQuotaSetting = "SAVEQUOTASETTINGS";
        #endregion
        #region // TimeZoneSetup
        public const string Sp_SaveTimeZoneSetup = "SAVETIMEZONESETUP";
        #endregion

        #region // ChangeLog
        public const string Sp_ChangeLogList = "GETCHANGESLOGLIST";
        #endregion

        #region // ExtractList
        public const string Sp_GetAllExtractList = "GETALLEXTRACTLIST";
        public const string Sp_GetAllIssueList = "GETALLISSUESLIST";
        public const string Sp_GetConnectionList = "GETCONNECTIONLIST";
        public const string Sp_GetSourceList = "GETSOURCESLIST";
        public const string Sp_GETENRICHLOG = "GETENRICHLOG";
        public const string Sp_GETButtonGroup = "GETBUTTONGROUPLABEL";
        #endregion
        #region
        public const string Sp_GETEXTRACTSUMMARY = "GETEXTRACTSUMMARY";
        #endregion
        #region //DynamicMenu
        public const string Sp_GetMainMenuList = "GETMAINMENULIST";
        public const string Sp_GetSubMenuList = "GETSUBMENULIST";

        #endregion
        #region // source list pages menu section and items 
        public const string Sp_GETSUBMENULIST = "GETSUBMENULIST";
        
        #endregion
        #region
        public const string Sp_GetMapTempList = "GETMAPTEMPLIST";
        public const string Sp_GetMapRules = "GETMAPRULES";
        public const string Sp_SaveMapRules = "SAVEMAPRULES";
        #endregion

        #region
        public const string Sp_SaveICSetup = "SAVEICSETUP";
        public const string Sp_SavePCSetup = "SAVEPCSETUP";
        public const string Sp_SaveALSetup = "SAVEALSETUP"; 
        public const string Sp_SaveMFASetup = "SAVEMFAETUP";
        public const string Sp_SaveRBASetup = "SAVERBASETUP";
        #endregion
        #region 
        //UAM Save Procedures
        public const string Sp_SaveUAM = "SAVEUAM";
        public const string Sp_UserList = "GETUSERSLIST";
        public const string Sp_GetTreeDropDownParentList = "GETTREEPARENTLIST";
        public const string Sp_GetTreeDropDownChildList = "GETTREECHILDLIST";
        //public const string Sp_SaveUserProfile = "SAVEUSERPROFILE";
        //public const string Sp_SaveUserPassword = "SAVEUSERPASSWORD";
        //public const string Sp_SavePermissions = "SAVEUSERPERMISSIONS";
        //public const string Sp_SaveUserNotification = "SAVEUSERNOTIFICATIONS";
        //public const string Sp_SaveUserRestrictions = "SAVEUSERRESTRICTIONS";
        //public const string Sp_SaveUserSettings = "SAVEUSERSETTINGS";
        #endregion 
        #region
        public const string Sp_GetAnalyzeData = "GETANALYZEDATA";
        #endregion
        /// Hafiz umer Work

        #region //ConnectorPipeline
         
        public const string Sp_SaveConnectorPipline = "SAVECONNECTORPIPELINE";
        public const string Sp_GETConnectorPipline = "GETCONNECTORPIPELINElLIST";
        #endregion


        #region //ConnectorDataSave

        public const string Sp_SAVESOURCEOBJECTLIST = "SAVESOURCEOBJECTLIST";
        public const string Sp_SAVEOBJECTFIELDSLIST = "SAVEOBJECTFIELDSLIST";
        #endregion




        #region // sql connector Source Account
        public const string Sp_SourceAccountCon = "SOURCEACCOUNTCON";
        public const string Sp_SourceAccountReq = "SOURCEACCOUNTREQ";
        public const string Sp_SourceAccount = "SOURCEACCOUNT";
        public const string Sp_UpdateConnectedSourceList = "UPDATECONNECTEDSOURCELIST";
        public const string Sp_SaveSourceObject = "SAVESOURCEOBJECTS";
        public const string Sp_GetSourceObjectList = "GETSOURCEOBJECTLIST";
        public const string Sp_UpdateSourceObjectList = "UPDATESOURCEOBJECTLIST";
        public const string Sp_GetObjectFieldList = "GETOBJECTFIELDSLIST";
        public const string Sp_UpdateObjectFieldList = "UPDATEOBJECTFIELDSLIST";
        public const string Sp_SaveExtracts = "SAVEEXTRACTDATA";
        public const string Sp_GetExtractData = "GETEXTRACTDATA";
        public const string Sp_GETDATALABELINGLIST = " GETDATALABELINGLIST";
       
        public const string Sp_UpdateConnectionData = "UPDATECONNECTEDSOURCELIST";
        public const string Sp_UpdateSourceObjectData = "GETSOURCEOBJECTS";
        public const string Sp_RUNEXTRACT = "RUNEXTRACT";
        public const string Sp_GETUSERCONNECTORSTATUS = "GETUSERCONNECTORSTATUS";


        #endregion
        #region // Destination connector   Account
        public const string Sp_GetDestinationDataList = "GETDESTINATIONLISTFORLOAD";
        #endregion
        #region // SourceDescriptionConfiguration
        public const string Sp_SaveSourceCounfiguration = "SOURCEACCOUNT";
        public const string Sp_SavedbCredential="SOURCEACCOUNTCON";
        public const string Sp_Savefile="SOURCEACCOUNTCON";
        public const string Sp_SaveSocialMedia= "SOURCEACCOUNTREQ";
        public const string Sp_SaveLookup="SAVELOOKUPVALUE";
        public const string Sp_sETLOOKUPTABLE = "SETLOOKUPTABLE";
        public const string Sp_LoadData="LOAD_DATA";
        /////  single proc for save,create table ,and enrichment
        public const string Sp_Lookup = "LOAD_DATA";

        #endregion


        #region // Load
        public const string Sp_GetDestinationList = "GETDESTINATIONLIST";
         

        #endregion


        #region // SourceDestination
        public const string Sp_SavedestinationDescription="DESTINATIONACCOUNT";
        public const string Sp_dESTINATIONACCOUNTCON = "DESTINATIONACCOUNTCON";
        //public const string Sp_Savefile = "SOURCEACCOUNTCON";
        //public const string Sp_SaveSocialMedia = "SOURCEACCOUNTREQ";
        //public const string Sp_SaveLookup = "SAVELOOKUPVALUE";
        //public const string Sp_sETLOOKUPTABLE = "SETLOOKUPTABLE";
        //public const string Sp_LoadData = "LOAD_DATA";
        /////  single proc for save,create table ,and enrichment


        #endregion

        #region   usman work
        public const string Sp_GetFunctionList = "GETFUNCTIONLIST";
        #endregion

    }
}
