using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Shared.Const
{
    public static class ServiceConstants
    {
        public const string ApiPrefix = "api/";
        public class ChildWorkSpace
        {
            public const string RouteName = "ChildWorkSpace";
            public const string GetAllWorkSpaces = "GetAllWorkSpaces";
            public const string SaveChildWorkspaces = "SaveChildWorkspaces";

        }
        public class checkMongoCon
        {
            public const string RouteName = "MongoDBConnectionStatus";
            public const string getstatus = "getstatus";
             

        }
        public class CSVHelper
        {
            public const string RouteName = "CSVHelper";
            public const string GetCSVFileData = "GetCSVFileData";
            public const string SaveJsonDataToCSV = "SaveJsonDataToCSV";
            public const string getCSVExtractFileDataToJson = "getCSVExtractFileDataToJson";
        }

        public class CommonDropdown
        {
            public const string RouteName = "CommonDropdownList";
            public const string GetDropdownList = "GetDropdownList";
            public const string GetUAMDropdown = "GetUAMDropdown";
            public const string GetTreeDropdown = "GetTreeropdown";
            public const string GetModelNameDropdown = "GetModelNameDropdown";
            public const string GetFIELDNameDropdown = "GetFIELDNameDropdown";
            public const string GetDropdownWithCategory = "GetDropdownWithCategory";
        }

        public class WorkspaceSetup
        {
            public const string RouteName = "WorkspaceSetup";
            public const string GetWorkspaceSetupSettings = "GetWorkspaceSetupSettings";
            public const string SaveWorkspaceSetup = "SaveWorkspaceSetup";
            public const string GetWorkspaceSetupList = "GetWorkspaceSetupList";
            public const string  GetParentWorkspaceDropdown  = "GetParentWorkspaceList";
        }
        public class CalenderSetup {
            public const string RouteName = "CalenderSetup";
            public const string SaveCalenderSetup = "SaveCalenderSetup";
        }

        public class CurrencySetup
        {
            public const string RouteName = "CurrencySetup";
            public const string SaveCurrencySetup = "SaveCurrencySetup";
        }

        public class DataGovernance
        {
            public const string RouteName = "DataGovernance";
            public const string SaveDataGovernance = "Save";
        }
        public class DisplaySettings
        {
            public const string RouteName = "DisplaySettings";
            public const string SaveDisplaySetting = "SaveDisplaySetting";
        }
        public class NumeralSetup
        {
            public const string RouteName = "NumeralSetup";
            public const string SaveNumeralSetup = "SaveNumeralSetup";
        }

        public class QuotaSetting
        {
            public const string RouteName = "QuotaSetting";
            public const string SaveQuotaSetting = "SaveQuotaSetting";
        }

        public class TimeZoneSetup
        {
            public const string RouteName = "TimeZoneSetup";
            public const string SaveTimeZoneSetup = "SaveTimeZoneSetup";
        }

        public class ChangeLog
        {
            public const string RouteName = "ChangeLog";
            public const string GetChangeLogList = "GetChangeLogList";
        }
        public class AllExtract
        {
            public const string RouteName = "AllExtract";
            public const string GetAllExtractList = "getAllExtractList";
            public const string GetAllIssueList = "GetAllIssueList";
            public const string GetConnectionList = "GetConnectionList";
            public const string GetSourceList = "GetSourceList";
            public const string GetEnrichLog = "GetEnrichLog";
            public const string GetButtonGroupList = "GetButtonGroupList";
            public const string GetExtractionCount = "getExtractionCount";
        }
        public class SourceDescAndConfiration
        {
            
            public const string RouteName = "SourceDescriptionAndConfigurtionController";
            public const string saveDescriptionInfo = "saveDescriptionInfo";
            public const string saveDbCredentialInfo = "saveDbCredentialInfo";
            public const string tokeninfo = "tokeninfo";
            public const string savefile  = "savefile";
            public const string uploadfile = "uploadfile";
            public const string saveSocialMedia = "saveSocialMedia";
            public const string UploadFIle = "UploadFIle";
            public const string GetFileList = "GetFileList";
            public const string Getsheetlist = "Getsheetlist";
            public const string DownloadFile = "DownloadFile"; 
            public const string uploadOneDrive = "uploadOneDrive";
            public const string Get90DaysDataListFromMong = "Get90DaysDataListFromMong";
            public const string DriveFiles = "DriveFiles";
            public const string FilesUploader = "FilesUploader";

        }
        public class DynamicMenuItem
        {
            public const string RouteName = "DynamicMenuItem";
            public const string GetMenulist = "GetMenulist";
            public const string GetSubMenuSection = "GetSubMenuSection";
            public const string GetSubMenuSectionItems = "GetSubMenuSectionItems";
            public const string SectionItemsForSourcePageList = "SectionItemsForSourcePageList";
 
        }
        public class DynamicEnrichScript
        {
            public const string RouteName = "DynamicEnrichScript";
            public const string getFunctionTemplate = "getfunctiontemplate";
            public const string saveEnrichScript = "saveEnrichScript";
            public const string getEnrichScript = "getEnrichScript";
            public const string getScriptList = "getScriptList";
            public const string updateEnrichScript = "updateEnrichScript";
            public const string GetTransformationList = "GetTransformationList";
            public const string DestinationList = "DestinationList";
            public const string ETLStatus = "ETLStatus";
            public const string GetMappedListForEditMode = "GetMappedListForEditMode";
            public const string ColumnMappingEditMode = "ColumnMappingEditMode";
            public const string LoadMappingEditMode = "LoadMappingEditMode";
            public const string DeleteScript = "DeleteScript";
            public const string savescriptFor_Transformation_Mapping_Loading_Data = "savescriptFor_Transformation_Mapping_Loading_Data";
        }

        //Prepare Modules 
        public class prepare
        {
            public const string RouteName = "Prepare";
            public const string GetDataQualityList = "GetDataQualityList";
            public const string GetLookupList = "GetLookupList";
            public const string GetAccountList = "GetAccountList";
            public const string GetTableList = "GetTableList";
            public const string GetobjectFieldList = "GetobjectFieldList";
            public const string SaveLookup = "SaveLookup";
            public const string GenerateCustomTable = "GenerateCustomTable";
            public const string InsertionOptions = "InsertionOptions";
            public const string addLookupScreen ="addLookupScreen";
            public const string saveDataLabeling = "saveDataLabeling";

        }
        public class SourceDestination
        {

            public const string RouteName = "Destination";
            public const string saveDescriptionInfo = "saveDescriptionInfo";
            public const string saveDbCredentialInfo = "saveDbCredentialInfo";
            public const string savefile = "savefile";
            public const string saveSocialMedia = "saveSocialMedia";
            public const string GetDestinationConnectorList = "GetDestinationConnectorList";

        }

        public class upload
        {
            public const string RouteName = "DocFilesUpload";
            public const string Save = "Save";
        }

        public class SqlConnector
        {
            public const string RouteName = "SqlConnector";
            
            public const string SaveStep1Data = "SaveStep1Data";
            public const string SaveSourceObjects = "SaveSourceObjects";
            public const string GetSourceObjectListForStep4Grid = "GetSourceObjectListForStep4Grid";
            public const string GetEntityNameDropdownStep6 = "GetEntityNameDropdownStep6";
            public const string GetObjectFieldsListStep6 = "GetObjectFieldsListStep6";
            public const string ObjectEntityFieldUpdateStep6 = "ObjectEntityFieldUpdateStep6";
            public const string SourceObjectGridListUpdateStep4 = "SourceObjectGridListUpdateStep4";
            public const string ExtractPageDataSaveForStep8 = "ExtractPageDataSaveForStep8";
            public const string GetSqlExtractPageList = "GetSqlExtractPageList";
            public const string GetSqlConnectorList = "GetSqlConnectorList";
            public const string GetConnectorListForEdit = "GetConnectorListForEdit";

            public const string GetSqlConnectorListforDriveFile = "GetSqlConnectorListForDriveFile";
            public const string GetSqlConnectorListForExtraction = "GetSqlConnectorListForExtraction";
            public const string UpdateSourceObject = "UpdateSourceObject";
            public const string getFunctionList = "getFunctionList";
            public const string testgoogle = "testgoogle";
            public const string GetConnectorStatus = "GetConnectorStatus";
            public const string ETLConnectorList = "ETLConnectorList";

    }
        public class Extract
        {
            public const string RouteName = "Extraction";
            public const string checkuserRights = "checkuserRights";
        }
        public class ConnectionLog
        {
            public const string RouteName = "ConnectionLog";
            public const string RecentConnectionLog = "RecentConnectionLog";
        }
        public class GridAndGraph
        {
            public const string RouteName = "GridAndGraph";
            public const string GraphData = "Graphdata";
            public const string GridData = "Griddata";
            public const string GridDynamicData = "GridDynamicData";
        }
        public class SourceEdit
        {
            public const string RouteName = "SourceEdit";
            public const string GetMapTempList = "GetMapTempList";
            public const string GetMappingRule = "GetMappingRule";
            public const string SaveMappingRule = "SaveMappingRule";
        }

        public class Load
        {
            public const string RouteName = "Load";
            public const string GetdestinationList = "GetdestinationList";
             
        }
        public class IndentityAndAccessmanagement
        {
            public const string RouteName = "IndentityAndAccessmanagement";
            public const string SaveIdentityControlSetup = "SaveIdentityControlSetup";
            public const string SavePasswordControlSetup = "SavePasswordControlSetup";
            public const string SaveAccessLockingSetup = "SaveAccessLockingSetup";
            public const string SaveMultiFactorAuthenticationSetup = "SaveMultiFactorAuthenticationSetup";
            public const string SaveRiskBasedAuthenticationSetup = "SaveRiskBasedAuthenticationSetup";
        }
        public class UserAccessManagement
        {
            public const string RouteName = "UserAccessManagement";
            public const string SaveUserAccessManagmentSetup = "SaveUserAccesManagmentSetup";
            public const string GetUserList = "GetUserList";

        }
        ///Pipeline Controller
        public class ConnectorPipeline
        {
            public const string RouteName = "ConnectorPipeline";
            public const string SaveConnectorType = "SaveConnectorType";
            public const string GetConnectorList = "GetConnectorList";

        }
        
        public class Enrich
        {
            public const string RouteName = "Enrich";
            public const string GetValueTableList = "getValueTableList";
            public const string GetValueTableData = "getValueTableData";
            public const string SaveValeTableData = "SaveValeTableData";
        }
        public class extractionCount
        {
            public const string RouteName = "extractionCount";
            public const string GetExtractionCount = "getExtractionCount";
        }
    }

}
