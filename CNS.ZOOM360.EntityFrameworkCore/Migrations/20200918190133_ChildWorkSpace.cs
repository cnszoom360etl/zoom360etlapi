﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CNS.ZOOM360.EntityFrameworkCore.Migrations
{
    public partial class ChildWorkSpace : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AllExtractListModels",
                columns: table => new
                {
                    userId = table.Column<string>(nullable: true),
                    workSpaceId = table.Column<int>(nullable: false),
                    CLIENT_ID = table.Column<string>(nullable: true),
                    childWorkspace = table.Column<string>(nullable: true),
                    childSelectedOption = table.Column<bool>(nullable: false),
                    childChange = table.Column<bool>(nullable: false),
                    childOverrideSelectedOption = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "de0720c6-8273-4125-81a8-3a0b7b59ddd0", "de55f20b-027a-42bf-82c3-6d59981ebfa8", "ADMIN", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "63a442d3-b310-432a-866a-70dcde3ce99a", "e70dcc21-1fe5-46d1-88ec-8f3caecfc05a", "Manager", "MANAGER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "2f5226a3-c440-4b6e-8e61-d7e9cad80fa0", "cb57bd97-f369-4640-bee4-e909336116a5", "Administrator", "ADMINISTRATOR" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AllExtractListModels");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2f5226a3-c440-4b6e-8e61-d7e9cad80fa0");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "63a442d3-b310-432a-866a-70dcde3ce99a");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "de0720c6-8273-4125-81a8-3a0b7b59ddd0");
        }
    }
}
