﻿using CNS.ZOOM360.Entities;
using CNS.ZOOM360.Entities.Configuration;
using CNS.ZOOM360.Entities.Model;
using CNS.ZOOM360.Entities.StoreProcedures;
using CNS.ZOOM360.Entities.StoreProcedures.AllExtract;
using CNS.ZOOM360.Entities.StoreProcedures.AllExtract.extract_count;
using CNS.ZOOM360.Entities.StoreProcedures.CalenderSetup;
using CNS.ZOOM360.Entities.StoreProcedures.ChangeLog;
using CNS.ZOOM360.Entities.StoreProcedures.Common;
using CNS.ZOOM360.Entities.StoreProcedures.ConnectionStatus;
using CNS.ZOOM360.Entities.StoreProcedures.Connectors.Databases.SQL;
using CNS.ZOOM360.Entities.StoreProcedures.CurrencySetup;
using CNS.ZOOM360.Entities.StoreProcedures.DataGovernance;
using CNS.ZOOM360.Entities.StoreProcedures.DisplaySettings;
using CNS.ZOOM360.Entities.StoreProcedures.DynamicMenu;
using CNS.ZOOM360.Entities.StoreProcedures.GridAndGraphData;
using CNS.ZOOM360.Entities.StoreProcedures.IdentityAccessManagment;
using CNS.ZOOM360.Entities.StoreProcedures.Load;
using CNS.ZOOM360.Entities.StoreProcedures.NumeralSetup;
using CNS.ZOOM360.Entities.StoreProcedures.Prepare;
using CNS.ZOOM360.Entities.StoreProcedures.QuotaSettings;
using CNS.ZOOM360.Entities.StoreProcedures.sourceDescriptionAndConfiguration;
using CNS.ZOOM360.Entities.StoreProcedures.SourceDescriptionAndConfiguration;
using CNS.ZOOM360.Entities.StoreProcedures.SourceEdit;
using CNS.ZOOM360.Entities.StoreProcedures.Transformation;
using CNS.ZOOM360.Entities.StoreProcedures.UserAccessManagment;
using CNS.ZOOM360.Entities.StoreProcedures.Workspace;
using CNS.ZOOM360.Entities.TimeZoneSetup;
using CNS.ZOOM360.Shared.StoreProcedures.Common.Dto;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace CNS.ZOOM360.EntityFrameworkCore
{
    public class ZOOM360DbContext: IdentityDbContext<User>
    {
        /* Define an IDbSet for each entity of the application */

        

          public virtual DbSet<Employee> emplyee { get; set; }
          public virtual DbSet<ChildWorkspaces> AllExtractListModels { get; set; }
        public virtual DbSet<WorkspaceSetup> workspaceSpModal { get; set; }

        public ZOOM360DbContext(DbContextOptions options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new RoleConfiguration());

            modelBuilder.Entity<Employee>(b => { });
            modelBuilder.Entity<ChildWorkspaces>(
                ex => { ex.HasNoKey(); }
                )  ;
            modelBuilder.Entity<WorkspaceSetup>(
                ex => { ex.HasNoKey(); }
                );
            modelBuilder.Entity<WorkspaceSetupList>(
               ex => { ex.HasNoKey(); }
               );
            
            modelBuilder.Entity<DropdownList>(
               ex => { ex.HasNoKey(); }
               );
            modelBuilder.Entity<UAMDropdownListModel>(
              ex => { ex.HasNoKey(); }
              );
            modelBuilder.Entity<CalenderSetupModel>(
               ex => { ex.HasNoKey(); }
               );
            modelBuilder.Entity<CurrencySetupModel>(
               ex => { ex.HasNoKey(); }
               );

            modelBuilder.Entity<DataGovernanceModel>(
               ex => { ex.HasNoKey(); }
               );
            
            modelBuilder.Entity<DisplaySettingsModel>(
               ex => { ex.HasNoKey(); }
               );

            modelBuilder.Entity<NumeralSetupModel>(
               ex => { ex.HasNoKey(); }
               );

            modelBuilder.Entity<QuotaSettingsModel>(
               ex => { ex.HasNoKey(); }
               );

            modelBuilder.Entity<TimeZoneSetupModel>(
               ex => { ex.HasNoKey(); }
               );

            modelBuilder.Entity<ChangeLogListModel>(
               ex => { ex.HasNoKey(); }
               );

            modelBuilder.Entity<AllExtractListModel>(
               ex => { ex.HasNoKey(); }
               );
            modelBuilder.Entity<AllIssuesModel>(
               ex => { ex.HasNoKey(); }
               );

            modelBuilder.Entity<ConnectionListModel>(
              ex => { ex.HasNoKey(); }
              );

            modelBuilder.Entity<SourceListModel>(
              ex => { ex.HasNoKey(); }
              );
            modelBuilder.Entity<MainMenuModel>(
                ex => { ex.HasNoKey(); }

                );
            

            modelBuilder.Entity<SubMenuSectionItemsModel>(
              ex => { ex.HasNoKey(); }
              );
            modelBuilder.Entity<SubMenusectionModel>(
              ex => { ex.HasNoKey(); }
              );
            modelBuilder.Entity<ConnectionLogModel>(
              ex => { ex.HasNoKey(); }
              );
            modelBuilder.Entity<GraphDataModel>(
              ex => { ex.HasNoKey(); }
              );
            modelBuilder.Entity<GridDataModel>(
             ex => { ex.HasNoKey(); }
             );
            modelBuilder.Entity<FieldMappingRuleTemplateModel>(
             ex => { ex.HasNoKey(); }
             );
            modelBuilder.Entity<SaveSourceFieldMappingRuleTemplateModel>(
            ex => { ex.HasNoKey(); }
            );

            modelBuilder.Entity<IdentityControlModel>(
           ex => { ex.HasNoKey(); }
           );
            modelBuilder.Entity<PasswordControlSetupModel>(
           ex => { ex.HasNoKey(); }
           );
            modelBuilder.Entity<AccessLockingModel>(
          ex => { ex.HasNoKey(); }
          );
            modelBuilder.Entity<MultiFactorAuthenticationModel>(
          ex => { ex.HasNoKey(); }
          );
            modelBuilder.Entity<RiskBaseAuthenticationModel>(
        ex => { ex.HasNoKey(); }
        );
            modelBuilder.Entity<RiskBaseAuthenticationModel>(
        ex => { ex.HasNoKey(); }
        );
            modelBuilder.Entity<SaveUserAccessManagementModel>(
       ex => { ex.HasNoKey(); }
       );
            modelBuilder.Entity<UserListModel>(
       ex => { ex.HasNoKey(); }
       );
            modelBuilder.Entity<TreeDropDownParentModel>(
        ex => { ex.HasNoKey(); }
        );
            modelBuilder.Entity<TreeDropDownChildModel>(
        ex => { ex.HasNoKey(); }
        );
            modelBuilder.Entity<UserLoginInfomartionModel>(
            ex => { ex.HasNoKey(); }
        );

            // Umer Work
            //modelBuilder.Entity<SourceAccountConnectionModel>(
            //  ex => { ex.HasNoKey(); }
            //  );
            //modelBuilder.Entity<EmailAuthenticationSourceAccount>(
            //  ex => { ex.HasNoKey(); }
            //  );
            //modelBuilder.Entity<SourceAccountModel>(
            //  ex => { ex.HasNoKey(); }
            //  );
            modelBuilder.Entity<UpdateConnectorListStep2>(
              ex => { ex.HasNoKey(); }
              );
            modelBuilder.Entity<GEtSourceObjectList>(
              ex => { ex.HasNoKey(); }
              );  
            modelBuilder.Entity<SourceObjectModel>(
              ex => { ex.HasNoKey(); }
              );
            modelBuilder.Entity<UpdateSourceObjectGridModel>(
              ex => { ex.HasNoKey(); }
              );
            modelBuilder.Entity<EntityObjectFieldsList>(
              ex => { ex.HasNoKey(); }
              );
            modelBuilder.Entity<ExtractModel>(
              ex => { ex.HasNoKey(); }
              );
            modelBuilder.Entity<DataQualityModel>(
             ex => { ex.HasNoKey(); }
             ); 
             modelBuilder.Entity<LookupListModel>(
             ex => { ex.HasNoKey(); }
             ); 
             modelBuilder.Entity<SourceTableListModel>(
             ex => { ex.HasNoKey(); }
             ); 
              modelBuilder.Entity<ObjectListModelForLookupInsert>(
             ex => { ex.HasNoKey(); }
             );
            modelBuilder.Entity<DestinationList>(
           ex => { ex.HasNoKey(); }
           );
            modelBuilder.Entity<SourceObjectGetList>(
              ex => { ex.HasNoKey(); }
              ); 
            modelBuilder.Entity<GetDectinationListForLoad>(
              ex => { ex.HasNoKey(); }
              ); 
            modelBuilder.Entity<GETPIPELINEMODEL>(
              ex => { ex.HasNoKey(); }
              ); 
            modelBuilder.Entity<TransformationFunctionModel>(
              ex => { ex.HasNoKey(); }
              );

            modelBuilder.Entity<FileModelListcs>(
             ex => { ex.HasNoKey(); }
             );
            modelBuilder.Entity<sectionModelForSourceEditPages>(
            ex => { ex.HasNoKey(); }
            );

            modelBuilder.Entity<SectionItemsModel>(
            ex => { ex.HasNoKey(); }
            );
            modelBuilder.Entity<DropDownWithCategoryModel>(
            ex => { ex.HasNoKey(); }
            );
            modelBuilder.Entity<SocialMediaBit>(
            ex => { ex.HasNoKey(); }
            );
            modelBuilder.Entity<valueTableListModel>(
       ex => { ex.HasNoKey(); }
       );
            modelBuilder.Entity<valueTableIDataModel>(
        ex => { ex.HasNoKey(); }
        );
            modelBuilder.Entity<extractionCountModel>(
    ex => { ex.HasNoKey(); }
    
    );
            modelBuilder.Entity<ButtonGroupModel>(
    ex => { ex.HasNoKey(); }

    );
            
        }
    }
}
