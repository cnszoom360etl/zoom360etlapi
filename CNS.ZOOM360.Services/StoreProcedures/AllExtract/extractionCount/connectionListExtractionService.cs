﻿using CNS.ZOOM360.Entities.StoreProcedures.AllExtract.extract_count;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.AllExtract.ExtractionCount;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.AllExtract.extractionCount
{
   public class connectionListExtractionService: IConnectionListExtration
    {
        private readonly IRepositoryBase<connectionListExtractionModel> _connectionListextractionRepository;
        public connectionListExtractionService(IRepositoryBase<connectionListExtractionModel> connectionListextractionRepository)
        {
            _connectionListextractionRepository = connectionListextractionRepository;
        }

        public async Task<List<connectionListExtractionModel>> GetconnectionListExtraction(conListExtractionInputModel conListExtractionInput)
        {
            object[] parameters = {
            new SqlParameter("@USER_ID", conListExtractionInput.UserId),
            new SqlParameter("@WORKSPACE_ID", conListExtractionInput.WorkSpaceId),
            new SqlParameter("@CLIENT_ID", conListExtractionInput.Client_Id),
            new SqlParameter("@ACCOUNT_ID",string.IsNullOrEmpty(conListExtractionInput.AccountId) ? (object)DBNull.Value: conListExtractionInput.AccountId ),
            new SqlParameter("@WORKSPACE_NAME", string.IsNullOrEmpty(conListExtractionInput.WorkspaceName) ? (object)DBNull.Value: conListExtractionInput.WorkspaceName ),
            new SqlParameter("@CONNECTION_NAME", string.IsNullOrEmpty(conListExtractionInput.ConnectionName) ? (object)DBNull.Value : conListExtractionInput.ConnectionName),
            new SqlParameter("@SOURCE_NAME", string.IsNullOrEmpty(conListExtractionInput.SourceName) ? (object)DBNull.Value : conListExtractionInput.SourceName),
            new SqlParameter("@ACCESS_GRANTED", string.IsNullOrEmpty(conListExtractionInput.AccessGranted) ? (object)DBNull.Value: conListExtractionInput.AccessGranted ),
            new SqlParameter("@CREATED_BY", string.IsNullOrEmpty(conListExtractionInput.CreatedBy) ? (object)DBNull.Value: conListExtractionInput.CreatedBy),
            new SqlParameter("@IS_ACTIVE", string.IsNullOrEmpty(conListExtractionInput.IsActive) ? (object)DBNull.Value: conListExtractionInput.IsActive ),
            new SqlParameter("@DESTINATION_ENABLED", string.IsNullOrEmpty(conListExtractionInput.DestinationEnabled) ? (object)DBNull.Value: conListExtractionInput.DestinationEnabled ),
            new SqlParameter("@LAST_ACCESSED", string.IsNullOrEmpty(conListExtractionInput.LastAccessed) ? (object)DBNull.Value: conListExtractionInput.LastAccessed ),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
        };

            string spQuery = StoreProcedureConstants.Sp_GETCONNECTIONLIST + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                " @ACCOUNT_ID, @WORKSPACE_NAME,@CONNECTION_NAME, @SOURCE_NAME," +
                " @ACCESS_GRANTED, @CREATED_BY,@IS_ACTIVE, " +
                "@DESTINATION_ENABLED," +
                " @LAST_ACCESSED, @V_MESSAGE OUTPUT";

            List<connectionListExtractionModel> conExtractionlist = _connectionListextractionRepository.ExecuteQuery(spQuery, parameters).ToList();

            return conExtractionlist;
        }
    }
}
