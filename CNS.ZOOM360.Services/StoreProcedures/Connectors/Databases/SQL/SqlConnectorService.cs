using CNS.ZOOM360.Entities.Model;
using CNS.ZOOM360.Entities.StoreProcedures.Connectors.Databases.SQL;
using CNS.ZOOM360.Entities.StoreProcedures.Transformation;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.Connector.Database.SQL;
using CNS.ZOOM360.Shared.StoreProcedures.Connector.Database.SQL.Dto;
using Microsoft.Data.SqlClient;
using MongoDB.Bson;
using MongoDB.Driver;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.Connectors.Databases.SQL
{
    public class SqlConnectorService : ISqlConnectorService 
    {
        public IMongoDatabase _db { get; set; }
         
        public MongoClient _mongoClient { get; set; }
        
        private readonly IRepositoryBase<TransformationFunctionModel> _transformationFunctionRepository;
        private readonly IRepositoryBase<SourceAccountConnectionModel> _SourceAccountConRepository;
        private readonly IRepositoryBase<EmailAuthenticationSourceAccount> _EmailAuthenticationRepository;
        private readonly IRepositoryBase<SourceAccountModel> _SourceAccountRepository;
        private readonly IRepositoryBase<UpdateConnectorListStep2> _UpdateConnectorRepository;
        private readonly IRepositoryBase<GEtSourceObjectList> _SourceObjectListRepository;
         private readonly IRepositoryBase<SourceObjectModel> _SourceObjectRepository;
        private readonly IRepositoryBase<UpdateSourceObjectGridModel> _UpdateSourceObjectGridRepository;
        private readonly IRepositoryBase<string> _savefieldobject;
        private readonly IRepositoryBase<EntityObjectFieldsList> _EntityObjectFieldListRepository;
        private readonly IRepositoryBase<ExtractModel> _ExtractRepository;
        private readonly IRepositoryBase<SourceObjectGetList> _SourceObjectupdate;
        
        private readonly IRepositoryBase<UpdateConnectorListStep2> _ConnectorRepository;

        public SqlConnectorService(IRepositoryBase<SourceAccountConnectionModel> SourceAccountConRepository,
            IRepositoryBase<EmailAuthenticationSourceAccount> EmailAuthenticationRepository,
            IRepositoryBase<SourceObjectGetList> SourceObjectupdate,
            IRepositoryBase<TransformationFunctionModel> transformationFunctionRepository,
        IRepositoryBase<SourceAccountModel> SourceAccountRepository,
            IRepositoryBase<UpdateConnectorListStep2> UpdateConnectorRepository,
            IRepositoryBase<string> savefieldobject,
            IRepositoryBase<GEtSourceObjectList> SourceObjectListRepository,
             IRepositoryBase<SourceObjectModel> SourceObjectRepository,
            IRepositoryBase<UpdateSourceObjectGridModel> UpdateSourceObjectGridRepository,
            IRepositoryBase<EntityObjectFieldsList> EntityObjectFieldListRepository,
            IRepositoryBase<ExtractModel> ExtractRepository, IRepositoryBase<UpdateConnectorListStep2> ConnectorRepository)
        {
            _SourceObjectupdate = SourceObjectupdate;
            _SourceAccountConRepository = SourceAccountConRepository;
            _EmailAuthenticationRepository = EmailAuthenticationRepository;
            _SourceAccountRepository = SourceAccountRepository;
            _savefieldobject = savefieldobject;
            _transformationFunctionRepository = transformationFunctionRepository;
            _UpdateConnectorRepository = UpdateConnectorRepository;
            _SourceObjectListRepository = SourceObjectListRepository;
            _SourceObjectRepository = SourceObjectRepository;
            _UpdateSourceObjectGridRepository = UpdateSourceObjectGridRepository;
            _EntityObjectFieldListRepository = EntityObjectFieldListRepository;
            _ExtractRepository = ExtractRepository;
            _ConnectorRepository = ConnectorRepository;
        }

        #region// Source Account Step 2
        //SQL Connector Save
        //Source Account
        //SaveDataForConnection
        public string SaveConnection(SourceAccountConnectionModel InputModel) 
        {
            var Client_Insert_Date = System.DateTime.Now.ToShortDateString();
            var Client_Insert_Time = System.DateTime.Now.ToShortTimeString();
            var Client_Insert_TimeZone = "null";
            //SqlCommand SqlCmd = new SqlCommand("SOURCEACCOUNTCON", cn);

            object[] parameters = {
            new SqlParameter("@USER_ID", InputModel.UserId),
            new SqlParameter("@WORKSPACE_ID", InputModel.WorkspaceId),
            new SqlParameter("@CLIENT_ID", InputModel.ClientId),
            new SqlParameter("@CONNECTOR_ID", InputModel.Connector_ID),
            new SqlParameter("@ACCOUNT_ID", InputModel.Account_Id),
            new SqlParameter("@FIELD_NAME", InputModel.HostName),
            new SqlParameter("@FIELD_VALUE", InputModel.FieldValue==null ? (object)DBNull.Value : InputModel.FieldValue),
            new SqlParameter("@CLIENT_DATE", string.IsNullOrEmpty(Client_Insert_Date) ? (object)DBNull.Value: Client_Insert_Date),
            new SqlParameter("@CLIENT_TIME", string.IsNullOrEmpty(Client_Insert_Time) ? (object)DBNull.Value: Client_Insert_Time),
            new SqlParameter("@CLIENT_TIME_ZONE", string.IsNullOrEmpty(Client_Insert_TimeZone) ? (object)DBNull.Value: Client_Insert_TimeZone),
            new SqlParameter("@CONNECTIVITY_STATUS",InputModel.connectivitystatus),
            new SqlParameter("@TOKEN_EXPIRY_TIME", string.IsNullOrEmpty(InputModel.tokenExpireTime) ? (object)DBNull.Value: InputModel.tokenExpireTime),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
        };


            string spQuery = StoreProcedureConstants.Sp_SourceAccountCon + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID, @CONNECTOR_ID,  @ACCOUNT_ID, @FIELD_NAME, @FIELD_VALUE,@CLIENT_DATE ,@CLIENT_TIME,@CLIENT_TIME_ZONE,@CONNECTIVITY_STATUS , @TOKEN_EXPIRY_TIME , @V_MESSAGE OUTPUT";
            return _SourceAccountConRepository.ExecuteCommand(spQuery, parameters);

            

        }
        //Email Save For Authurization
        //Source Account
        public string saveEmailAuthenticationForConnection(EmailAuthenticationSourceAccount InputModel)
        {

            //SqlCommand SqlCmd = new SqlCommand("SOURCEACCOUNTREQ", cn);

            object[] parameters = {
                new SqlParameter("@USER_ID", InputModel.UserId),
                new SqlParameter("@WORKSPACE_ID", InputModel.WorkspaceId),
                new SqlParameter("@CLIENT_ID", InputModel.ClientId),
                new SqlParameter("@ACCOUNT_ID", InputModel.Account_Id),
                new SqlParameter("@ACCOUNT_AUTHORIZATION", InputModel.AccountAuthurization),
                new SqlParameter("@EMAIL_ID", InputModel.Email),
                new SqlParameter("@EMAIL_MESSAGE", SqlDbType.NVarChar, 4000) {Direction=ParameterDirection.Output },
                new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction=ParameterDirection.Output }
            };


            string spQuery = StoreProcedureConstants.Sp_SourceAccountReq + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID,@ACCOUNT_ID, @ACCOUNT_AUTHORIZATION, @EMAIL_ID,@EMAIL_MESSAGE OUTPUT,  @V_MESSAGE OUTPUT";
            return _EmailAuthenticationRepository.ExecuteCommand(spQuery, parameters);
        }
        //step2AccountDsplayDataSave
        //Source Account
        //SaveDataForAccountDispaly
        public string SaveAccountDispaly(SourceAccountModel InputModal)
        {

            //SqlCommand SqlCmd = new SqlCommand("SOURCEACCOUNT", cn);
            var Client_Insert_Date = System.DateTime.Now.ToShortDateString();
            var Client_Insert_Time = System.DateTime.Now.ToShortTimeString();
            var Client_Insert_TimeZone = "null";
            object[] parameters = {
            new SqlParameter("@USER_ID", InputModal.UserId),
            new SqlParameter("@WORKSPACE_ID", InputModal.WorkspaceId),
            new SqlParameter("@WORKSPACE_NAME",InputModal.WorkspaceName),
            new SqlParameter("@CLIENT_ID", InputModal.ClientId),
            new SqlParameter("@ACCOUNT_ID", InputModal.Account_Id),
            new SqlParameter("@CONNECTOR_ID", InputModal.ConnectorId),
            new SqlParameter("@ACCOUNT_DISPLAY_NAME", InputModal.DisplayName),
            new SqlParameter("@ENABLED_STATUS", InputModal.EnableStatus),
            new SqlParameter("@VISIBILITY_STATUS", string.IsNullOrEmpty(InputModal.visibilitystatue) ? (object)DBNull.Value: InputModal.visibilitystatue),
             new SqlParameter("@COMMENTS", string.IsNullOrEmpty(InputModal.comments) ? (object)DBNull.Value: InputModal.comments),
            new SqlParameter("@SPECIAL_COMMENTS", string.IsNullOrEmpty(InputModal.spComments) ? (object)DBNull.Value: InputModal.spComments),
            new SqlParameter("@CLIENT_DATE", string.IsNullOrEmpty(Client_Insert_Date) ? (object)DBNull.Value: Client_Insert_Date),
            new SqlParameter("@CLIENT_TIME", string.IsNullOrEmpty(Client_Insert_Time) ? (object)DBNull.Value: Client_Insert_Time),
            new SqlParameter("@CLIENT_TIME_ZONE", string.IsNullOrEmpty(Client_Insert_TimeZone) ? (object)DBNull.Value: Client_Insert_TimeZone),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output},
            };

            string spQuery = StoreProcedureConstants.Sp_SourceAccount + " @USER_ID,@WORKSPACE_ID,  @CLIENT_ID, @ACCOUNT_ID, @CONNECTOR_ID, @ACCOUNT_DISPLAY_NAME, @WORKSPACE_NAME, @ENABLED_STATUS, @VISIBILITY_STATUS, @COMMENTS, @SPECIAL_COMMENTS,@CLIENT_DATE ,@CLIENT_TIME,@CLIENT_TIME_ZONE,@V_MESSAGE OUTPUT";
            return _SourceAccountRepository.ExecuteCommand(spQuery, parameters);


        }
        //Step2EditMode
        //Source Account
        public async Task<IEnumerable<UpdateConnectorListStep2>> GetUpdateConnectedSourceList(string Account_Id, string UserId, string WorkspaceId, string ClientId, string ConnectorId)
        {

            //SqlCommand SqlCmd = new SqlCommand("UPDATECONNECTEDSOURCELIST", SqlConn);
            object[] parameters = {
                new SqlParameter("@USER_ID", UserId),
            new SqlParameter("@WORKSPACE_ID", WorkspaceId),
            new SqlParameter("@CLIENT_ID", ClientId),
            new SqlParameter("@ACCOUNT_ID", Account_Id),
            new SqlParameter("@CONNECTOR_ID", ConnectorId),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output},
        };

            string spQuery = StoreProcedureConstants.Sp_UpdateConnectedSourceList + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                " @ACCOUNT_ID, @CONNECTOR_ID, @V_MESSAGE OUTPUT";

            List<UpdateConnectorListStep2> updateConnector = _UpdateConnectorRepository.ExecuteQuery(spQuery, parameters).ToList();
            return updateConnector;
        }

        #endregion

        #region // Source Object
        //source Object
        // SaveDataForStep4
        public string SaveSourceObjects(SourceObjectModel InputModel)
        {

            //SqlCommand SqlCmd = new SqlCommand("SAVESOURCEOBJECTS", cn);
            var Client_Insert_Date = System.DateTime.Now.ToShortDateString();
            var Client_Insert_Time = System.DateTime.Now.ToShortTimeString();
            var Client_Insert_TimeZone = "null";
            object[] parameters = {
            new SqlParameter("@USER_ID", InputModel.UserId),
            new SqlParameter("@WORKSPACE_ID", InputModel.WorkSpaceID),
            new SqlParameter("@CLIENT_ID", InputModel.ClientId),
            new SqlParameter("@OBJECT_NAME", InputModel.ObjectName),
            new SqlParameter("@WORKSPACE_NAME", InputModel.WorkspaceName),
            new SqlParameter("@ACCESS_GRANTED", InputModel.AccessGrant),
            new SqlParameter("@ACCOUNT_ID", InputModel.AccountId),
            new SqlParameter("@CONNECTOR_ID", InputModel.ConnectorId),
            new SqlParameter("@CLIENT_DATE", string.IsNullOrEmpty(Client_Insert_Date) ? (object)DBNull.Value: Client_Insert_Date),
            new SqlParameter("@CLIENT_TIME", string.IsNullOrEmpty(Client_Insert_Time) ? (object)DBNull.Value: Client_Insert_Time),
            new SqlParameter("@CLIENT_TIME_ZONE", string.IsNullOrEmpty(Client_Insert_TimeZone) ? (object)DBNull.Value: Client_Insert_TimeZone),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output},

            };
            
            string spQuery = StoreProcedureConstants.Sp_SaveSourceObject + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID,  @OBJECT_NAME,@WORKSPACE_NAME, @ACCESS_GRANTED,@ACCOUNT_ID,@CONNECTOR_ID,@CLIENT_DATE ,@CLIENT_TIME,@CLIENT_TIME_ZONE,  @V_MESSAGE OUTPUT";
            return _SourceObjectRepository.ExecuteCommand(spQuery, parameters);



        }
        //StepForDrigDataList
        //Source Object
        public async Task<IEnumerable<GEtSourceObjectList>> GetSourceObjectList(GetSourceObjectListInput input)
        {
            //SqlCommand SqlCmd = new SqlCommand("GETSOURCEOBJECTLIST", SqlConn);
            object[] parameters = {
            new SqlParameter("@USER_ID", input.UserId),
            new SqlParameter("@WORKSPACE_ID", input.WorkspaceId),
            new SqlParameter("@CLIENT_ID", input.ClientId),
            new SqlParameter("@CONNECTOR_ID", input.ConnectorId),
            new SqlParameter("@DATABASE_NAME", input.databasename),
            new SqlParameter("@MAPPED_TABLE", string.IsNullOrEmpty(input.Mappedtable) ? (object)DBNull.Value: input.Mappedtable),
            new SqlParameter("@ACCOUNT_ID",string.IsNullOrEmpty(input.Accountid) ? (object)DBNull.Value: input.Accountid),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output}
            };
             string spQuery = StoreProcedureConstants.Sp_GetSourceObjectList + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
             " @CONNECTOR_ID, @DATABASE_NAME, @MAPPED_TABLE,@ACCOUNT_ID,@V_MESSAGE OUTPUT";

            List<GEtSourceObjectList> GEtSourceObjectList = _SourceObjectListRepository.ExecuteQuery(spQuery, parameters).ToList();
            foreach (var item in GEtSourceObjectList)
            {
                item.SERVER_INSERT_DATE = Convert.ToDateTime(item.SERVER_INSERT_DATE).ToString();
            }
            return GEtSourceObjectList;
        }

        //Step4UpdateGridList
        //Source Object
        public async Task<List<UpdateSourceObjectGridModel>> UpdateGridSourceObjectList(GEtSourceObjectList inputs, string UserId, int Workspaceid, int Clientid, int ConnectorId)
        {

            //SqlCommand SqlCmd = new SqlCommand("UPDATESOURCEOBJECTLIST", SqlConn);
             
                        object[] parameters = {
            new SqlParameter("@USER_ID",UserId),
            new SqlParameter("@WORKSPACE_ID", Workspaceid),
            new SqlParameter("@CLIENT_ID", Clientid),
            new SqlParameter("@CONNECTOR_ID", ConnectorId),
            new SqlParameter("@OBJECT_NAME", inputs.OBJECT_NAME),
            new SqlParameter("@DISPLAY_NAME",string.IsNullOrEmpty(inputs.DISPLAY_NAME) ? (object)DBNull.Value: inputs.DISPLAY_NAME),
            new SqlParameter("@VISIBILITY_STATUS", string.IsNullOrEmpty(inputs.OBJECT_VISIBILITY) ? (object)DBNull.Value: inputs.OBJECT_VISIBILITY),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output}
            };

            string spQuery = StoreProcedureConstants.Sp_UpdateSourceObjectList + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                "  @CONNECTOR_ID, @OBJECT_NAME, @DISPLAY_NAME,  @VISIBILITY_STATUS, @V_MESSAGE OUTPUT";

            List<UpdateSourceObjectGridModel> SourceObjectList = _UpdateSourceObjectGridRepository.ExecuteQuery(spQuery, parameters).ToList();
            return SourceObjectList;
            
        }
        #endregion

        //EntityDropdownForStep6 
        //Field Section
        // EntityDropdownList method skiped from this service you can use  GetSourceObjectList
       
       //Step6GetObjectList
        //Field Section
        public async Task<IEnumerable<EntityObjectFieldsList>> GetObjectFieldsList(GetObjectFieldListInput input)
        {

            //SqlCommand SqlCmd = new SqlCommand("GETOBJECTFIELDSLIST", SqlConn);
            if(input.ConnectorId=="216")
            {

                List<string> list = new List<string>();
                string connect = "server=192.168.223.111;user=CNSE_TEST;password = CNSE@12345;database=ZMDB";
                using (MySqlConnection conn = new MySqlConnection(connect))
                {
                    conn.Open();
                    MySqlCommand cmdName = new MySqlCommand("SELECT column_name FROM information_schema.columns WHERE table_name = '" + input.ObjectName + "'; ", conn);
                    MySqlDataReader reader = cmdName.ExecuteReader();
                    while (reader.Read())
                    {
                        list.Add(reader.GetString(0));
                    }
                    reader.Close();
                }
                var str = String.Join(",", list);

                object[] parameterss = {
            new SqlParameter("@USER_ID", input.UserId),
            new SqlParameter("@WORKSPACE_ID", input.WorkspaceId),
            new SqlParameter("@CLIENT_ID", input.ClientId),
            new SqlParameter("@CONNECTOR_ID",input.ConnectorId),
            new SqlParameter("@ACCOUNT_ID",input.AccountId),
            new SqlParameter("@OBJECT_NAME", input.ObjectName),
            new SqlParameter("@COLUMN_NAME", str),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output},
            };

                string spQueryy = StoreProcedureConstants.Sp_SAVEOBJECTFIELDSLIST + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                  "  @CONNECTOR_ID, @ACCOUNT_ID, @OBJECT_NAME, @COLUMN_NAME,  @V_MESSAGE OUTPUT";


                _savefieldobject.ExecuteCommand(spQueryy, parameterss);


            }
            else if(input.ConnectorId == "224")
            {
                List<string> list = new List<string>();
               
                try
                {
                    
                    string connect = "mongodb://sa:CNSE%4012345@192.168.223.111:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false";
                    _mongoClient = new MongoClient(connect);
                    _db = _mongoClient.GetDatabase("ZMDB");
                    //string conne = "mongodb://sa:CNSE%4012345@192.168.223.111:27017/ZMDB?replicaSet=rs01&ssl=false&connectTimeoutMS=100000";
                    MongoClient dbClient = new MongoClient(connect);
                    var servers = dbClient.GetServer();
                    servers.Ping();
                   
                    var docs = _db.GetCollection<BsonDocument>(input.ObjectName).Find(new BsonDocument()).FirstOrDefault();
                     
                    //var list = docs.Names.ToList();
                    foreach (var key in docs)
                    {
                        list.Add(key.Name.ToString());
                    };
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw;
                }
                

                var str = String.Join(",", list);

                object[] parameterss = {
            new SqlParameter("@USER_ID", input.UserId),
            new SqlParameter("@WORKSPACE_ID", input.WorkspaceId),
            new SqlParameter("@CLIENT_ID", input.ClientId),
            new SqlParameter("@CONNECTOR_ID",input.ConnectorId),
            new SqlParameter("@ACCOUNT_ID",input.AccountId),
            new SqlParameter("@OBJECT_NAME", input.ObjectName),
            new SqlParameter("@COLUMN_NAME", str),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output},
            };

                string spQueryy = StoreProcedureConstants.Sp_SAVEOBJECTFIELDSLIST + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                  "  @CONNECTOR_ID, @ACCOUNT_ID, @OBJECT_NAME, @COLUMN_NAME,  @V_MESSAGE OUTPUT";


                _savefieldobject.ExecuteCommand(spQueryy, parameterss);

            }
            else if(input.ConnectorId == "214")
            {
                object[] parameterssql = {
            new SqlParameter("@USER_ID", input.UserId),
            new SqlParameter("@WORKSPACE_ID", input.WorkspaceId),
            new SqlParameter("@CLIENT_ID", input.ClientId),
            new SqlParameter("@CONNECTOR_ID", input.ConnectorId),
            new SqlParameter("@ACCOUNT_ID", input.AccountId),
            new SqlParameter("@OBJECT_NAME", input.ObjectName),
            new SqlParameter("@DATABASE_NAME", input.databasename),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output},
            };

                string spQuerysql= StoreProcedureConstants.Sp_GetObjectFieldList + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                    "  @CONNECTOR_ID,@ACCOUNT_ID, @OBJECT_NAME, @DATABASE_NAME,  @V_MESSAGE OUTPUT";

                List<EntityObjectFieldsList> SourceObjectListsql = _EntityObjectFieldListRepository.ExecuteQuery(spQuerysql, parameterssql).ToList();
                return SourceObjectListsql;


            }

            object[] parameters = {
            new SqlParameter("@USER_ID", input.UserId),
            new SqlParameter("@WORKSPACE_ID", input.WorkspaceId),
            new SqlParameter("@CLIENT_ID", input.ClientId),
            new SqlParameter("@CONNECTOR_ID", input.ConnectorId),
            new SqlParameter("@ACCOUNT_ID", input.AccountId),
            new SqlParameter("@OBJECT_NAME", input.ObjectName),
            new SqlParameter("@DATABASE_NAME", input.databasename),
           
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output},
            };

        string spQuery = StoreProcedureConstants.Sp_GetObjectFieldList + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
            "  @CONNECTOR_ID, @ACCOUNT_ID, @OBJECT_NAME, @DATABASE_NAME,  @V_MESSAGE OUTPUT";

        List<EntityObjectFieldsList> SourceObjectList = _EntityObjectFieldListRepository.ExecuteQuery(spQuery, parameters).ToList();
            return SourceObjectList;
           
        }

        //Step6UpdateGridFields
        // Field Section
        public async Task<List<EntityObjectFieldsList>> UpdateEntityObjectField(UpdateEntityObjectFieldInputs input, string UserId, string Workspaceid, string Clientid, string ConnectorId, string objectName)
        {

            //   SqlCommand SqlCmd = new SqlCommand("UPDATEOBJECTFIELDSLIST", SqlConn);
            object[] parameters = {
            new SqlParameter("@USER_ID", UserId),
            new SqlParameter("@WORKSPACE_ID", Workspaceid),
            new SqlParameter("@CLIENT_ID",Clientid),
            new SqlParameter("@CONNECTOR_ID",ConnectorId),
            new SqlParameter("@OBJECT_NAME", objectName),
            new SqlParameter("@FIELD_NAME", input.OBJECT_FIELD_NAME),
            new SqlParameter("@FIELD_TYPE", input.OBJECT_FIELD_TYPE),
            new SqlParameter("@DISPLAY_NAME", input.DISPLAY_NAME),
            new SqlParameter("@AGGREGATION_STATUS", input.AGGREGATION_STATUS),
            new SqlParameter("@ISKEY_STATUS", input.ISKEY_STATUS),
            new SqlParameter("@VISIBILITY_STATUS", input.VISIBILITY_STATUS),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output }
            };

            string spQuery = StoreProcedureConstants.Sp_UpdateObjectFieldList + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
            "  @CONNECTOR_ID,  @OBJECT_NAME, @FIELD_NAME,@FIELD_TYPE, @DISPLAY_NAME, @AGGREGATION_STATUS," +
            " @ISKEY_STATUS, @VISIBILITY_STATUS, @V_MESSAGE OUTPUT";

            List<EntityObjectFieldsList> SourceObjectList = _EntityObjectFieldListRepository.ExecuteQuery(spQuery, parameters).ToList();
            return SourceObjectList;

        }

        //Step8DataSaveForExtarct
        //Extract 
        public string SaveExtracts(SaveExtractsInputs inputs)
        {
            //SqlCommand SqlCmd = new SqlCommand("SAVEEXTRACTDATA", cn);

            object[] parameters = {
            new SqlParameter("@USER_ID", inputs.UserId),
            new SqlParameter("@WORKSPACE_ID", inputs.Workspaceid),
            new SqlParameter("@CLIENT_ID", inputs.Clientid),
            new SqlParameter("@CONNECTOR_ID", inputs.CONNECTORID),
            new SqlParameter("@ACCOUNT_ID", inputs.ACCOUNT_Id),
            new SqlParameter("@DATE_LINK", inputs.FromandTo),
            new SqlParameter("@DATA_RAW_STATE", inputs.data_RAW_STATE),
            new SqlParameter("@EXTRACT_PROCESS_RUNNING", inputs.extract_PROCESS_RUNNING),
            new SqlParameter("@DATA_COLLECTION", inputs.data_COLLECTION),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output }
            };

            string spQuery = StoreProcedureConstants.Sp_SaveExtracts + " @USER_ID, @CLIENT_ID, @WORKSPACE_ID, " +
                "  @CONNECTOR_ID,@ACCOUNT_ID, @DATE_LINK,@DATA_RAW_STATE, @EXTRACT_PROCESS_RUNNING," +
                " @DATA_COLLECTION, @V_MESSAGE OUTPUT";

            return _ExtractRepository.ExecuteCommand(spQuery, parameters);
        }

        //Step8EditMode
        //Extract
        public async Task<IEnumerable<ExtractModel>> GetExtractData(GetExtractDataInputs Inputs)
        {

            //SqlCommand SqlCmd = new SqlCommand("GETEXTRACTDATA", SqlConn);

            object[] parameters = {
            new SqlParameter("@USER_ID", Inputs.UserId),
            new SqlParameter("@WORKSPACE_ID", Inputs.WorkspaceId),
            new SqlParameter("@CLIENT_ID", Inputs.ClientId),
            new SqlParameter("@ACCOUNT_ID", Inputs.Account_Id),
            new SqlParameter("@CONNECTOR_ID", Inputs.ConnectorId),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output}

        };

            string spQuery = StoreProcedureConstants.Sp_GetExtractData + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
            " @ACCOUNT_ID, @CONNECTOR_ID, @V_MESSAGE OUTPUT";
            List<ExtractModel> extractlist = _ExtractRepository.ExecuteQuery(spQuery, parameters).ToList();
            return extractlist;

            
        }


        public async Task<List<UpdateConnectorListStep2>> GetConnectorData(string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId)
        {

            //   SqlCommand SqlCmd = new SqlCommand("UPDATEOBJECTFIELDSLIST", SqlConn);
            object[] parameters = {
            new SqlParameter("@USER_ID", UserId),
            new SqlParameter("@WORKSPACE_ID", Workspaceid),
            new SqlParameter("@CLIENT_ID",Clientid),
            new SqlParameter("@ACCOUNT_ID",Account_Id),
            new SqlParameter("@CONNECTOR_ID", ConnectorId),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output }
            };

            string spQuery = StoreProcedureConstants.Sp_RUNEXTRACT + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
            "  @ACCOUNT_ID,  @CONNECTOR_ID, @V_MESSAGE ";

            List<UpdateConnectorListStep2> ConnectorInfoList = _ConnectorRepository.ExecuteQuery(spQuery, parameters).ToList();
            foreach (var item in ConnectorInfoList)
            {
                if(item.Emailid!=null)
                {
                    item.Fieldvalue = "Token Expired";
                    var msg = SendingEmial(ConnectorInfoList);
                }
                
            }
            
            return ConnectorInfoList;




        }




    public async Task<List<UpdateConnectorListStep2>> ETLGetConnectorData(int Account_Id, string UserId, string Workspaceid, string Clientid, int ConnectorId)
    {

      //   SqlCommand SqlCmd = new SqlCommand("UPDATEOBJECTFIELDSLIST", SqlConn);
      object[] parameters = {
            new SqlParameter("@USER_ID", UserId),
            new SqlParameter("@WORKSPACE_ID", Workspaceid),
            new SqlParameter("@CLIENT_ID",Clientid),
            new SqlParameter("@ACCOUNT_ID",Account_Id),
            new SqlParameter("@CONNECTOR_ID", ConnectorId),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output }
            };

      string spQuery = StoreProcedureConstants.Sp_RUNEXTRACT + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
      "  @ACCOUNT_ID,  @CONNECTOR_ID, @V_MESSAGE ";

      List<UpdateConnectorListStep2> ConnectorInfoList = _ConnectorRepository.ExecuteQuery(spQuery, parameters).ToList();
      foreach (var item in ConnectorInfoList)
      {
        if (item.Emailid != null)
        {
          item.Fieldvalue = "Token Expired";
          var msg = SendingEmial(ConnectorInfoList);
        }

      }

      return ConnectorInfoList;




    }




    public async Task<List<UpdateConnectorListStep2>> GetConnectorDataForExtraction(extractionModelClass[] Account_Id, string UserId, string Workspaceid, string Clientid)
        {
            List<UpdateConnectorListStep2> Conlist = new List<UpdateConnectorListStep2>();

            //   SqlCommand SqlCmd = new SqlCommand("UPDATEOBJECTFIELDSLIST", SqlConn);
            foreach (var item in Account_Id)
            {
                object[] parameters = {
            new SqlParameter("@USER_ID", UserId),
            new SqlParameter("@WORKSPACE_ID", Workspaceid),
            new SqlParameter("@CLIENT_ID",Clientid),
            new SqlParameter("@ACCOUNT_ID",item.accountid),
            new SqlParameter("@CONNECTOR_ID",item.connectorId),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output }
            };

                string spQuery = StoreProcedureConstants.Sp_UpdateConnectionData + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                "  @ACCOUNT_ID,  @CONNECTOR_ID, @V_MESSAGE ";

                Conlist = _ConnectorRepository.ExecuteQuery(spQuery, parameters).ToList();

                Conlist.AddRange(Conlist);


            }
            
            return Conlist;

        }
        public async Task<List<SourceObjectGetList>> GetSourceObjectforUpdate(string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId)
        {
            object[] parameters = {
            new SqlParameter("@USER_ID", UserId),
            new SqlParameter("@WORKSPACE_ID", Workspaceid),
            new SqlParameter("@CLIENT_ID",Clientid),
            new SqlParameter("@ACCOUNT_ID",Account_Id),
            new SqlParameter("@CONNECTOR_ID", ConnectorId),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output }
            };

            string spQuery = StoreProcedureConstants.Sp_UpdateSourceObjectData + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
            "@ACCOUNT_ID,  @CONNECTOR_ID, @V_MESSAGE ";

            List<SourceObjectGetList> SourceInfoInfoList = _SourceObjectupdate.ExecuteQuery(spQuery,parameters).ToList();
            return SourceInfoInfoList;

        }






        public  List<UpdateConnectorListStep2> GetConnectorDataForDriveFiles(string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId)
        {

            //   SqlCommand SqlCmd = new SqlCommand("UPDATEOBJECTFIELDSLIST", SqlConn);
            object[] parameters = {
            new SqlParameter("@USER_ID", UserId),
            new SqlParameter("@WORKSPACE_ID", Workspaceid),
            new SqlParameter("@CLIENT_ID",Clientid),
            new SqlParameter("@ACCOUNT_ID",Account_Id),
            new SqlParameter("@CONNECTOR_ID", ConnectorId),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output }
            };

            string spQuery = StoreProcedureConstants.Sp_UpdateConnectionData + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
            "  @ACCOUNT_ID,  @CONNECTOR_ID, @V_MESSAGE ";

            List<UpdateConnectorListStep2> ConnectorInfoList = _ConnectorRepository.ExecuteQuery(spQuery, parameters).ToList();
            return ConnectorInfoList;

        }


       public List<GEtSourceObjectList> GetAllMySqlData()
        {
            var count = 1;
            List<GEtSourceObjectList> list = new List<GEtSourceObjectList>();
            string connect = "server=192.168.223.111;user=CNSE_TEST;password = CNSE@12345;database=ZMDB";
            using (MySqlConnection conn = new MySqlConnection(connect))
            {
                conn.Open();
                MySqlCommand cmdName = new MySqlCommand("SHOW FULL TABLES", conn);
                MySqlDataReader reader = cmdName.ExecuteReader();
                while (reader.Read())
                {
                    
                    GEtSourceObjectList myentity = new GEtSourceObjectList();
                    myentity.OBJECT_ID = count;
                    myentity.OBJECT_NAME=reader[0].ToString()+"-"+ reader[1].ToString();
                    myentity.OBJECT_TYPE = reader[1].ToString();
                    myentity.SERVER_INSERT_DATE = DateTime.Now.ToString("dd-mm-yyyy");
                    list.Add(myentity);
                    
                  

                }
                reader.Close();
                var str = String.Join(",", list.Select(p => p.OBJECT_NAME));
            }
            return list;
        }




        public async Task<IEnumerable<TransformationFunctionModel>> getFunctionDetails(functionPerametersValueModel Inputs)
        {

            //SqlCommand SqlCmd = new SqlCommand("GETEXTRACTDATA", SqlConn);

            object[] parameters = {
            new SqlParameter("@USER_ID", Inputs.userId),
            new SqlParameter("@WORKSPACE_ID", Inputs.workspaceId),
            new SqlParameter("@CLIENT_ID", Inputs.clientID),
            new SqlParameter("@FUNCTION_GROUP_ID", Inputs.functionGroupId),
            new SqlParameter("@FUNCTION_ID", Inputs.functionId),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output}

        };

            string spQuery = StoreProcedureConstants.Sp_GETDATALABELINGLIST + " @USER_ID, @WORKSPACE_ID, @CLIENT_ID," +
            " @FUNCTION_GROUP_ID, @FUNCTION_ID, @V_MESSAGE OUTPUT";
            List<TransformationFunctionModel> functionList = _transformationFunctionRepository.ExecuteQuery(spQuery, parameters).ToList();
            return functionList;


        }

        private string SendingEmial(List<UpdateConnectorListStep2> input)
        {
            try
            {
                foreach (var item in input)
                {
                    if(item.Emailid!=null)
                    {
                        item.Fieldvalue ="Token Expired";
                        MailMessage message = new MailMessage();
                        SmtpClient smtp = new SmtpClient();
                        message.From = new MailAddress("med360@cns-e.com");
                        message.To.Add(new MailAddress(item.Emailid));
                        message.Subject = "Test";
                        message.IsBodyHtml = true;
                        //message.Body = "test";
                        // var returnpath = "http://localhost:4200/Success/id=" + socialmedia.SourceCommonModel.AccountId;
                        message.Body = "Please Authorize this email" + "<br><br><button><a href='https://www.facebook.com/v9.0/dialog/oauth?client_id=261213182411450&redirect_uri=http://localhost:4200/Success/" + item.accountId + "&state=0' value='" + item.Emailid + "' style='text-decoration:none;color:rgb(33, 155, 156);'>Authorize</a></button> ";
                        //message.Body = "Authorize this email"+ "<br><br><button><a href='https://www.facebook.com/v9.0/dialog/oauth?client_id=763254611045170&redirect_uri=http://192.168.223.111:9095/auth/login&state=0' value='" + socialmedia.Email + "' style='text-decoration:none;color:rgb(33, 155, 156);'>Authorize</a></button> ";
                        smtp.Port = 587;
                        smtp.EnableSsl = true;
                        //smtp.Host = "smtp.gmail.com";
                        smtp.Host = "mail.cns-e.com";
                        smtp.UseDefaultCredentials = false;
                        //smtp.Credentials = new NetworkCredential("cnszoom360@gmail.com", "cnse@12345");
                        smtp.Credentials = new NetworkCredential("med360@cns-e.com", "med360@cns");
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Send(message);
                    }

                }
                return "send Email Successfully";
            }
            catch (Exception ex)
            {
                return "Something went wrong";
                 
            }
            
        }




        public async Task<List<UpdateConnectorListStep2>> GetConnectorListForEdit(string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId)
        {

            //   SqlCommand SqlCmd = new SqlCommand("UPDATEOBJECTFIELDSLIST", SqlConn);
            object[] parameters = {
            new SqlParameter("@USER_ID", UserId),
            new SqlParameter("@WORKSPACE_ID", Workspaceid),
            new SqlParameter("@CLIENT_ID",Clientid),
            new SqlParameter("@ACCOUNT_ID",Account_Id),
            new SqlParameter("@CONNECTOR_ID", ConnectorId),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output }
            };

            string spQuery = StoreProcedureConstants.Sp_UpdateConnectedSourceList + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
            "  @ACCOUNT_ID,  @CONNECTOR_ID, @V_MESSAGE ";

            List<UpdateConnectorListStep2> ConnectorInfoList = _ConnectorRepository.ExecuteQuery(spQuery, parameters).ToList();
            
            return ConnectorInfoList;

        }

        public string GetConnectorStatus(connectorStatusModel Input)
        {
            object[] parameters = {
            new SqlParameter("@USER_ID",Input.userId),
            new SqlParameter("@WORKSPACE_ID",Input.workSpaceId),
            new SqlParameter("@CLIENT_ID",Input.client_id),
            new SqlParameter("@CONNECTOR_ID",Input.ConnectorId),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output }
            };
            string spQuery = StoreProcedureConstants.Sp_GETUSERCONNECTORSTATUS + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
            "@CONNECTOR_ID, @V_MESSAGE OUTPUT";
            return _ExtractRepository.ExecuteCommand(spQuery, parameters);
        }
    }
}

