﻿using CNS.ZOOM360.Entities.StoreProcedures.NumeralSetup;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.NumeralSetup;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.NumeralSetup
{
    public class NumeralSetupService: INumeralSetupService
    {
        private readonly IRepositoryBase<NumeralSetupModel> _numeralSetupRepository;
        public NumeralSetupService(IRepositoryBase<NumeralSetupModel> numeralSetupRepository) {
            _numeralSetupRepository = numeralSetupRepository;
        }


        public async Task<string> SaveNumeralSetup(NumeralSetupModel numeralsSetupModel)
        {

            object[] parameters = {
                new SqlParameter("@USER_ID", numeralsSetupModel.UserId),
            new SqlParameter("@WORKSPACE_ID", numeralsSetupModel.WorkSpaceId),
            new SqlParameter("@CLIENT_ID", numeralsSetupModel.Client_Id),
            new SqlParameter("@NUMBERING_SYSTEM_FORMAT", numeralsSetupModel.NumberingSystemFormat),
            new SqlParameter("@NUMBER_SIGN_TYPE", numeralsSetupModel.NumberSignType),
            new SqlParameter("@SIGN_FORMAT", numeralsSetupModel.SignFormat),
            new SqlParameter("@POSITIVE_NUMBER_COLOR_CODE", numeralsSetupModel.PositiveNumbeColorCode),
            new SqlParameter("@NEGATIVE_NUMBER_COLOR_CODE", numeralsSetupModel.NegitiveNumberColorCode),
            new SqlParameter("@NUMBER_CONVERSION", numeralsSetupModel.NumberConversion),
            new SqlParameter("@NUMBER_VALUE_CONVERSION", numeralsSetupModel.NumberValueConversion),
            new SqlParameter("@NUMBER_VALUE", numeralsSetupModel.NumberValue),
            new SqlParameter("@SELECTIVE_DECIMAL_PLACES", numeralsSetupModel.SelectiveDecimalPlaces),
            new SqlParameter("@FULL_DECIMAL_PLACES", numeralsSetupModel.FullDecimalPlaces),
            new SqlParameter("@ROUND_OFF_NUMBER", numeralsSetupModel.RoundOffNumbers),
            new SqlParameter("@SELECTIVE_ROUND_OFF_PLACES", numeralsSetupModel.SelectiveRoundOffPlace),
            new SqlParameter("@NUMBER_APPLY_AND_ENFORCE", numeralsSetupModel.NumberApplyAndEnforce),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
        };

            string spQuery = StoreProcedureConstants.Sp_SaveNumeralSetup + " @USER_ID,@WORKSPACE_ID," +
                " @CLIENT_ID, @NUMBERING_SYSTEM_FORMAT, @NUMBER_SIGN_TYPE,@SIGN_FORMAT," +
                " @POSITIVE_NUMBER_COLOR_CODE, @NEGATIVE_NUMBER_COLOR_CODE, @NUMBER_CONVERSION," +
                " @NUMBER_VALUE_CONVERSION, @NUMBER_VALUE, @SELECTIVE_DECIMAL_PLACES," +
                " @FULL_DECIMAL_PLACES, @ROUND_OFF_NUMBER, @SELECTIVE_ROUND_OFF_PLACES," +
                " @NUMBER_APPLY_AND_ENFORCE, @V_MESSAGE OUTPUT";
            return _numeralSetupRepository.ExecuteCommand(spQuery, parameters);

        }
    }
}
