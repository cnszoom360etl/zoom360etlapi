﻿using CNS.ZOOM360.Entities.StoreProcedures.ConfigureTransformation;
using CNS.ZOOM360.Entities.StoreProcedures.EnrichDynamicModel;
using CNS.ZOOM360.Host;
using CNS.ZOOM360.Shared.StoreProcedures.EnrichDynamivMetadata;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.enrichDynamicMetadata
{
    public class EnrichDynamicMetadata : IEnrichDynamicMetadata
    {
        private process process;

        public IMongoDatabase _db { get; set; }
        private MongoClient _mongoClient { get; set; }
        public IClientSessionHandle Session { get; set; }
        public Destination_load Destination_load { get; private set; }

        public EnrichDynamicMetadata(IOptions<Mongosettings> configuration)
        {
            _mongoClient = new MongoClient(configuration.Value.ConnectionString);
            _db = _mongoClient.GetDatabase("ZMDB");

        }
        public string GetEnrichScript(string userId, string workspaceId, string clientId, string _id)
        {
            var cars = _db.GetCollection<BsonDocument>("EnrichScripts");
            var filter = Builders<BsonDocument>.Filter.Eq("userId", userId)
                & Builders<BsonDocument>.Filter.Eq("workspaceId", workspaceId)
                & Builders<BsonDocument>.Filter.Eq("clientId", clientId)
                & Builders<BsonDocument>.Filter.Eq("_id", ObjectId.Parse(_id));

            var doc = cars.Find(filter).Project<BsonDocument>("{_id: {$toString : '$_id' },userId:1,workspaceId:1,clientId:1,scriptname:1,enableScript:1,ScriptList:1}")
                .FirstOrDefault().ToString();

            return doc;
        }
        public async Task<string> SaveEnrichScript(EnrichDynamicModel collection)

        {
            var collectionn = _db.GetCollection<EnrichDynamicModel>("EnrichScripts");

            await collectionn.InsertOneAsync(collection);
            return "Save Successfully... ";
          
        }
        public string UpdateEnrichScript(EnrichDynamicModel coll, string _id)
        {
            var collection = _db.GetCollection<EnrichDynamicModel>("EnrichScripts");
            var Filter = Builders<EnrichDynamicModel>.Filter.Eq("_id", ObjectId.Parse(_id));
            var result = collection.ReplaceOne(Filter, coll).ToString();
            return "update Successfully...";
        }

        public string DeleteEnrichScript(string userId, string scriptid)
        {
            var collection = _db.GetCollection<BsonDocument>("EnrichScripts");
            var Filter = Builders<BsonDocument>.Filter.Eq("_id", ObjectId.Parse(scriptid))
                & Builders<BsonDocument>.Filter.Eq("userId", userId);
            var documents = collection.Find(Filter).FirstOrDefault();
            if(documents != null)
            {
                var result = collection.DeleteOne(Filter);

                return "Delete Successfully...";
            }
            else
            {
                return "Script Not Exist...";
            }
           
        }
        public string getFunctionTemplate()

        {
            var fieldsBuilder = Builders<BsonDocument>.Projection;
            var fields = fieldsBuilder.Exclude("_id");
            var cars = _db.GetCollection<BsonDocument>("EnrichDynamicScriptMetadata");
            var documents = cars.Find(new BsonDocument()).Project<BsonDocument>(fields).ToList().ToJson();
            return documents;
            //var cars = _db.GetCollection<BsonDocument>("enrichDynamicMetadata");
            //var documents = cars.Find(new BsonDocument()).ToList().ToJson();
            // var result = JsonConvert.SerializeObject(documents);
            //return documents;
        }
        public string WriteArray(object[] arr)
        {
            List<string> doc = new List<string>();
            for (int i = 0; i < arr.Length; i++)
            {
                doc.Add(arr[i].ToString());
                var document = BsonSerializer.Deserialize<BsonDocument>(doc[i]);
                var collectionn = _db.GetCollection<BsonDocument>("test_arraystore");
                collectionn.InsertOneAsync(document);
            };
            return "Store to mongoDB complete...";
        }


        public List<dynamic> getscriptlistbyusingIds(function_IdsArray[] _id)
        {
            List<object> li = new List<object>();
            List<dynamic> list1 = new List<dynamic>();
            for (int i = 0; i < _id.Length; i++)
            {

                var collection = _db.GetCollection<BsonDocument>("EnrichScripts");
                var filter = Builders<BsonDocument>.Filter.Eq("_id", ObjectId.Parse(_id[i].scriptId));
                var documents = collection.Find(filter).FirstOrDefault();
                
            }
            return li ;
        }
        public string GetScriptList(string userId, string workspaceId, string clientId)
        {
            var collection = _db.GetCollection<BsonDocument>("EnrichScripts");
            var filter = Builders<BsonDocument>.Filter.Eq("userId", userId)
                & Builders<BsonDocument>.Filter.Eq("workspaceId", workspaceId)
                & Builders<BsonDocument>.Filter.Eq("clientId", clientId);

            var documents = collection.Find(filter).Project<BsonDocument>("{_id: {$toString : '$_id' },scriptname:1,enableScript:1,UpdatedOn: {$toString : '$UpdatedOn' },CreatedOn: {$toString : '$CreatedOn' }}").ToList().ToJson();
            return documents;
        }

        public async Task<string> SaveTransformation_Mapping_Loading_Script(JsonAppledFor_Mapping_Entirichment_Load saving_collection,string accountId)
          {
             
            var msg = "";
            if (saving_collection.column_mapping != null)
            {
                var collection = _db.GetCollection<BsonDocument>("JsonappledFor_Mapping_Enrichment_Loading");

                try
                {

                    var Filter = Builders<BsonDocument>.Filter.Eq("accountId", accountId);
                    var documents = collection.Find(Filter).FirstOrDefault();
                    if (documents != null)
                    {
                        var update_column_mapping = Builders<BsonDocument>.Update.Set("column_mapping", saving_collection.column_mapping).Set("modelName", saving_collection.modelName);
                         //var result = collection.ReplaceOne(Filter, coll).ToString();
                         collection.UpdateOneAsync(Filter, update_column_mapping);
                         msg = "Update Successfully...!";
                         
                    }
                    else
                    {
                        var collectionn = _db.GetCollection<JsonAppledFor_Mapping_Entirichment_Load>("JsonappledFor_Mapping_Enrichment_Loading");
                        collectionn.InsertOneAsync(saving_collection);
                        msg = "Save Successfully...!";
                    }
                     
                }
                catch (NullReferenceException ex)
                {
                   
                     
                }
              }
            if (saving_collection.content_mapping != null)
            {
                var collection = _db.GetCollection<BsonDocument>("JsonappledFor_Mapping_Enrichment_Loading");

                 try
                {
                    var Filter = Builders<BsonDocument>.Filter.Eq("accountId", saving_collection.accountId);

                   var documents = collection.Find(Filter).FirstOrDefault();
                    if (documents != null)
                    {
                         var update_content_mapping = Builders<BsonDocument>.Update.Set("content_mapping", saving_collection.content_mapping);
                        //var result = collection.ReplaceOne(Filter, coll).ToString();
                         collection.UpdateOneAsync(Filter, update_content_mapping);
                        msg = "update successfully...!";
                        
                    }
                    else
                    {
                        var collectionn = _db.GetCollection<JsonAppledFor_Mapping_Entirichment_Load>("JsonappledFor_Mapping_Enrichment_Loading");
                        collectionn.InsertOneAsync(saving_collection);
                        msg = "save successfully...!";
                    }

                }
                catch (NullReferenceException ex)
                {

                    
                    
                }





            }
            if (saving_collection.transformation != null)
            {
                var collection = _db.GetCollection<BsonDocument>("JsonappledFor_Mapping_Enrichment_Loading");

                try
                {
                    var Filter = Builders<BsonDocument>.Filter.Eq("accountId", saving_collection.accountId);

                    var documents = collection.Find(Filter).FirstOrDefault();
                    if (documents != null)
                    {
                        var update_transformation = Builders<BsonDocument>.Update.Set("transformation", saving_collection.transformation);
                        //var result = collection.ReplaceOne(Filter, coll).ToString();
                        collection.UpdateOneAsync(Filter, update_transformation);
                        msg = "update successfully...!";

                    }
                    else
                    {
                        var collectionn = _db.GetCollection<JsonAppledFor_Mapping_Entirichment_Load>("JsonappledFor_Mapping_Enrichment_Loading");
                        collectionn.InsertOneAsync(saving_collection);
                        msg = "save successfully...!";
                    }

                }
                catch (NullReferenceException ex)
                {

                   

                }
                }

            if (saving_collection.load != null)
            {
                var collection = _db.GetCollection<BsonDocument>("JsonappledFor_Mapping_Enrichment_Loading");

                try
                {
                    var Filter = Builders<BsonDocument>.Filter.Eq("accountId", saving_collection.accountId);
                     var documents = collection.Find(Filter).FirstOrDefault();
                    if (documents != null)
                    {
                        var update_load = Builders<BsonDocument>.Update.Set("load", saving_collection.load);
                        //var result = collection.ReplaceOne(Filter, coll).ToString();
                        collection.UpdateOneAsync(Filter, update_load);
                        msg = "update successfully...!";
                        

                    }
                    else
                    {
                        var collectionn = _db.GetCollection<JsonAppledFor_Mapping_Entirichment_Load>("JsonappledFor_Mapping_Enrichment_Loading");
                        collectionn.InsertOneAsync(saving_collection);
                        msg = "save successfully...!";
                    }

                }
                catch (NullReferenceException ex)
                {

                     

                }





            }
            return msg;
          }

        public  async Task<string> GetDestinationList(Destination_Json[] _Des,string userId,string workspaceId,string clientId,string accountId)
        {
          
           List<object> li = new List<object>();
           List<dynamic> list1 = new List<dynamic>();
            process  = new process();
             
            var dictionary = new Dictionary<string, object> {};
            List<Destination_load> s = new List<Destination_load>();
            for (int i = 0; i< 2; i++)
            {
               var Destination_load = new Destination_load
                {
                    Database = "test",
                    serviceName = "orcl.CNSE.COM.PK",
                    host = "test",
                    dbname = "test",
                    user = "test",
                    password = "test",
                    tablename = "test",
                    port = "test"


               };
                s.Add(Destination_load);
             }

            dictionary.Add( "Load",
                s
                //    Destination_load = new Destination_load
                //{
                //    Database = _Des[i].DesConnectorType,
                //    serviceName = "orcl.CNSE.COM.PK",
                //    host = _Des[i].Desfieldvalue.DeshostName,
                //    dbname = _Des[i].Desfieldvalue.DesdatabaseName,
                //    user = _Des[i].Desfieldvalue.Desusername,
                //    password = _Des[i].Desfieldvalue.Despassword,
                //    tablename = _Des[i].Destablename,
                //    port = _Des[i].Desfieldvalue.Desport


                //}

               );

            return JsonConvert.SerializeObject(dictionary);
        }


        public  string getStatusForETL()

        {
            var fieldsBuilder = Builders<BsonDocument>.Projection;
            var fields = fieldsBuilder.Exclude("_id");
            var status = _db.GetCollection<BsonDocument>("ETL_Status");
            var documents = status.Find(new BsonDocument()).Project<BsonDocument>(fields).ToList().ToJson();
            return documents;

             
           // return JsonConvert.SerializeObject(collection); 
           
        }
        public string GetContentMappedList(string userId, string workspaceId, string clientId, string accountId ,string EditscreenName)
         {
            string documents = string.Empty;
            
                var collection = _db.GetCollection<BsonDocument>("JsonappledFor_Mapping_Enrichment_Loading");
                var Filter = Builders<BsonDocument>.Filter.Eq("accountId", accountId)
                    & Builders<BsonDocument>.Filter.Eq("userId", userId)
                    & Builders<BsonDocument>.Filter.Eq("workspaceId", workspaceId)
                    & Builders<BsonDocument>.Filter.Eq("clientId", clientId);
                //var documents = collection.Find(Filter).FirstOrDefault();
                 documents = collection.Find(Filter).Project<BsonDocument>("{_id: {$toString : '$_id' },content_mapping:1}").ToList().ToJson();
                
            
            
             
            return documents;

           

        }
        public string GetColumnMappedList(string userId, string workspaceId, string clientId, string accountId, string EditscreenName)
        {
            string documents = string.Empty;
             
             
                var collection = _db.GetCollection<BsonDocument>("JsonappledFor_Mapping_Enrichment_Loading");
                var Filter = Builders<BsonDocument>.Filter.Eq("accountId", accountId)
                    & Builders<BsonDocument>.Filter.Eq("userId", userId)
                    & Builders<BsonDocument>.Filter.Eq("workspaceId", workspaceId)
                    & Builders<BsonDocument>.Filter.Eq("clientId", clientId);
                //var documents = collection.Find(Filter).FirstOrDefault();
                documents = collection.Find(Filter).Project<BsonDocument>("{_id: {$toString : '$_id' },column_mapping:1,modelName:1}").ToList().ToJson();
             
            return documents;



        }
        public string GetDestinationMappedList(string userId, string workspaceId, string clientId, string accountId)
        {
            string documents = string.Empty;


            var collection = _db.GetCollection<BsonDocument>("JsonappledFor_Mapping_Enrichment_Loading");
            var Filter = Builders<BsonDocument>.Filter.Eq("accountId", accountId)
                & Builders<BsonDocument>.Filter.Eq("userId", userId)
                & Builders<BsonDocument>.Filter.Eq("workspaceId", workspaceId)
                & Builders<BsonDocument>.Filter.Eq("clientId", clientId);
            //var documents = collection.Find(Filter).FirstOrDefault();
            documents = collection.Find(Filter).Project<BsonDocument>("{_id: {$toString : '$_id' },load:1}").ToList().ToJson();

            return documents;



        }

    }
}
