﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CNS.ZOOM360.Entities.StoreProcedures.AllExtract;
using CNS.ZOOM360.Entities.StoreProcedures.ConnectionStatus;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.AllExtract;
using CNS.ZOOM360.Shared.StoreProcedures.AllExtract.Dto;
using CNS.ZOOM360.Shared.StoreProcedures.ConnectionStatus;
using Microsoft.Data.SqlClient;

namespace CNS.ZOOM360.Services.StoreProcedures.ConnectionStatus
{
    public class ConnectionLogService: IConnectionLogService
    {
        private readonly IRepositoryBase<ConnectionListModel> _ConnectionListRepository;
        private readonly IRepositoryBase<ConnectionLogModel> _ConnectionLogRepository;
        private readonly IAllExtractService _allExtractService;
        public ConnectionLogService(IRepositoryBase<ConnectionListModel> ConnectionListRepository,
          IRepositoryBase<ConnectionLogModel> ConnectionLogRepository, IAllExtractService allExtractService)
        {
            _ConnectionListRepository = ConnectionListRepository;
            _ConnectionLogRepository = ConnectionLogRepository;
            _allExtractService = allExtractService;
        }
        public async Task<List<ConnectionLogModel>> GetAllConnections(ExtractListInputModel ListInputmodel)
        {
            List<ConnectionListModel> connectionlist = await _allExtractService.GetConnectionlist(ListInputmodel);
            List<int> sourceAccountidList = new List<int>();
            foreach (var item in connectionlist)
            {
                int sourceId;
                sourceId = item.SourceConnectorId;
                sourceAccountidList.Add(sourceId);
            }
            var list = sourceAccountidList.Distinct().ToList();
            List<ConnectionLogModel> connectionlogList = new List<ConnectionLogModel>();
            connectionlogList = await GetConnectorTypeDash(ListInputmodel.UserId, ListInputmodel.WorkSpaceId, ListInputmodel.clientId, list);
            return connectionlogList;
        }
        public async Task<List<ConnectionLogModel>> GetConnectorTypeDash(string userId, string workspace, string clientId, List<int> sourceConnectorId)
        {
            List <ConnectionLogModel> connectionlogList=new List<ConnectionLogModel>();
            foreach (var item in sourceConnectorId)
            {
                object[] parameters = {
            new SqlParameter("@USER_ID", userId),
            new SqlParameter("@WORKSPACE_ID", workspace),
            new SqlParameter("@CLIENT_ID",clientId),
            new SqlParameter("@SOURCE_CONNECTOR_ID",item),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
            };
                string spQuery = StoreProcedureConstants.Sp_GETEXTRACTSUMMARY + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
               " @SOURCE_CONNECTOR_ID," +
               " @V_MESSAGE OUTPUT";
                List<ConnectionLogModel> connectionlog = _ConnectionLogRepository.ExecuteQuery(spQuery, parameters).ToList();
                foreach(var item2 in connectionlog)
                {
                    connectionlogList.Add(item2);
                }
            }
            return connectionlogList;
        }

    }
}
