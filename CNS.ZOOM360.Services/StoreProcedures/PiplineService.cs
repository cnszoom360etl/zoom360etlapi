﻿using CNS.ZOOM360.Entities.Model;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.piplineConnector;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures
{
   public class PiplineService: IConnectorPipeline
    {
        private readonly IRepositoryBase<ConnectorPiplineModel> _inputModel;
        private readonly IRepositoryBase<GETPIPELINEMODEL> _GETPIPELINEMODEL;
        public PiplineService(IRepositoryBase<ConnectorPiplineModel> inputModel, IRepositoryBase<GETPIPELINEMODEL> GETPIPELINEMODEL)
        {
            _inputModel = inputModel;
            _GETPIPELINEMODEL = GETPIPELINEMODEL;
        }
        public async Task<string> save(ConnectorPiplineModel inpoutModel)
        { 
            object[] parameters = {

            new SqlParameter("@USER_ID", inpoutModel.USER_ID),
            new SqlParameter("@WORKSPACE_ID", inpoutModel.WORKSPACE_ID),
            new SqlParameter("@CLIENT_ID", inpoutModel.CLIENT_ID),
            new SqlParameter("@PIPELINE_ID", string.IsNullOrEmpty(inpoutModel.PIPELINE_ID) ? (object)DBNull.Value: inpoutModel.PIPELINE_ID),
            new SqlParameter("@CONNECTOR_ID", inpoutModel.CONNECTOR_ID),
            new SqlParameter("@CONNECTORS_NAME", inpoutModel.CONNECTORS_NAME),
            new SqlParameter("@CONNECTOR_DISPLAY_NAME", inpoutModel.CONNECTOR_DISPLAY_NAME),
            new SqlParameter("@ACCOUNT_ID", inpoutModel.ACCOUNT_ID),
            new SqlParameter("@CLIENT_DATE", inpoutModel.CLIENT_DATE),
            new SqlParameter("@CLIENT_TIME", inpoutModel.CLIENT_TIME),
            new SqlParameter("@CLIENT_TIME_ZONE", string.IsNullOrEmpty(inpoutModel.CLIENT_TIME_ZONE) ? (object)DBNull.Value: inpoutModel.CLIENT_TIME_ZONE),
             new SqlParameter("@REMARKS_1", string.IsNullOrEmpty(inpoutModel.REMARKS_1) ? (object)DBNull.Value: inpoutModel.REMARKS_1),
            new SqlParameter("@REMARKS_2", string.IsNullOrEmpty(inpoutModel.REMARKS_2) ? (object)DBNull.Value: inpoutModel.REMARKS_2),
            new SqlParameter("@REMARKS_3", string.IsNullOrEmpty(inpoutModel.REMARKS_3) ? (object)DBNull.Value: inpoutModel.REMARKS_3),
             new SqlParameter("@REMARKS_4", string.IsNullOrEmpty(inpoutModel.REMARKS_4) ? (object)DBNull.Value: inpoutModel.REMARKS_4),
            new SqlParameter("@FLEX_1", string.IsNullOrEmpty(inpoutModel.FLEX_1) ? (object)DBNull.Value: inpoutModel.FLEX_1),
            new SqlParameter("@FLEX_2", string.IsNullOrEmpty(inpoutModel.FLEX_2) ? (object)DBNull.Value: inpoutModel.FLEX_2),
             new SqlParameter("@FLEX_3", string.IsNullOrEmpty(inpoutModel.FLEX_3) ? (object)DBNull.Value: inpoutModel.FLEX_3),
            new SqlParameter("@FLEX_4", string.IsNullOrEmpty(inpoutModel.FLEX_4) ? (object)DBNull.Value: inpoutModel.FLEX_4),
            new SqlParameter("@FLEX_5", string.IsNullOrEmpty(inpoutModel.FLEX_5) ? (object)DBNull.Value: inpoutModel.FLEX_5),
             new SqlParameter("@FLEX_6", string.IsNullOrEmpty(inpoutModel.FLEX_6) ? (object)DBNull.Value: inpoutModel.FLEX_6),
            new SqlParameter("@FLEX_7", string.IsNullOrEmpty(inpoutModel.FLEX_7) ? (object)DBNull.Value: inpoutModel.FLEX_7),
            new SqlParameter("@FLEX_8", string.IsNullOrEmpty(inpoutModel.FLEX_8) ? (object)DBNull.Value: inpoutModel.FLEX_8),
             new SqlParameter("@FLEX_9", string.IsNullOrEmpty(inpoutModel.FLEX_9) ? (object)DBNull.Value: inpoutModel.FLEX_9),
            new SqlParameter("@FLEX_10", string.IsNullOrEmpty(inpoutModel.FLEX_10) ? (object)DBNull.Value: inpoutModel.FLEX_10),
            new SqlParameter("@FLEX_11", string.IsNullOrEmpty(inpoutModel.FLEX_11) ? (object)DBNull.Value: inpoutModel.FLEX_11),
            new SqlParameter("@FLEX_12", string.IsNullOrEmpty(inpoutModel.FLEX_12) ? (object)DBNull.Value: inpoutModel.FLEX_12),
             new SqlParameter("@FLEX_13", string.IsNullOrEmpty(inpoutModel.FLEX_13) ? (object)DBNull.Value: inpoutModel.FLEX_13),
            new SqlParameter("@FLEX_14", string.IsNullOrEmpty(inpoutModel.FLEX_14) ? (object)DBNull.Value: inpoutModel.FLEX_14),
            new SqlParameter("@FLEX_15", string.IsNullOrEmpty(inpoutModel.FLEX_15) ? (object)DBNull.Value: inpoutModel.FLEX_15),
            new SqlParameter("@FLEX_16", string.IsNullOrEmpty(inpoutModel.FLEX_16) ? (object)DBNull.Value: inpoutModel.FLEX_16),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){Direction = ParameterDirection.Output }

        };

            string spQuery = StoreProcedureConstants.Sp_SaveConnectorPipline +" @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                " @PIPELINE_ID, @CONNECTOR_ID, @CONNECTORS_NAME, @CONNECTOR_DISPLAY_NAME, @ACCOUNT_ID," +
                " @CLIENT_DATE, @CLIENT_TIME,@CLIENT_TIME_ZONE, " +
                "@REMARKS_1,@REMARKS_2,@REMARKS_3,@REMARKS_4," +
                " @FLEX_1,@FLEX_2,@FLEX_3,@FLEX_4,@FLEX_5,@FLEX_6,@FLEX_7,@FLEX_8,@FLEX_9,@FLEX_10,@FLEX_11,@FLEX_12,@FLEX_13,@FLEX_14,@FLEX_15,@FLEX_16 , " +
                "  @V_MESSAGE OUTPUT";

            return _inputModel.ExecuteCommand(spQuery, parameters);


        }




        public async Task<List<GETPIPELINEMODEL>> GetData(string pipelineId, string UserId, string WorkspaceId, string ClientId)
        {
            object[] parameters = {
            new SqlParameter("@USER_ID", UserId),
            new SqlParameter("@WORKSPACE_ID",WorkspaceId),
            new SqlParameter("@CLIENT_ID", ClientId),
            new SqlParameter("@PIPELINE_ID", string.IsNullOrEmpty(pipelineId) ? (object)DBNull.Value: pipelineId),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
            };
            string spQuery = StoreProcedureConstants.Sp_GETConnectorPipline + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                " @PIPELINE_ID ," +
                " @V_MESSAGE OUTPUT";

            List<GETPIPELINEMODEL> PipelineList = _GETPIPELINEMODEL.ExecuteQuery(spQuery, parameters).ToList();

            return PipelineList;
        }

    }
}
