﻿using CNS.ZOOM360.Entities.StoreProcedures.CurrencySetup;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.CurrencySetup;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.CurrencySetup
{
    public class CurrencySetupService: ICurrencySetupService
    {
        private readonly IRepositoryBase<CurrencySetupModel> _CurrencySetupRepository;
        public CurrencySetupService(IRepositoryBase<CurrencySetupModel> CurrencySetupRepository) {
            _CurrencySetupRepository = CurrencySetupRepository;
        }

        public async Task<string> SaveCurrencySetup(CurrencySetupModel currencySetupModel)
        {

            object[] parameters = {

            new SqlParameter("@USER_ID", currencySetupModel.userId),
            new SqlParameter("@WORKSPACE_ID", currencySetupModel.workSpaceId),
            new SqlParameter("@CLIENT_ID", currencySetupModel.CLIENT_ID),
            new SqlParameter("@CURRENCY_TYPE", currencySetupModel.currencyType),
            new SqlParameter("@CURRENCY_TYPE_SIGN", currencySetupModel.currenceyTypeSign),
            new SqlParameter("@CURRENCY_IMAGE", currencySetupModel.currencyImage),
            new SqlParameter("@CURRENCY_COLLECTED_DATA", currencySetupModel.currencyCollectedData),
            new SqlParameter("@CURRENCY_PREPARING_DATA", currencySetupModel.currencyPrepareData),
            new SqlParameter("@CURRENCY_PRESENTING_DATA", currencySetupModel.currencyPresentingData),
            new SqlParameter("@CURRENCY_CONVERSION", currencySetupModel.currencyConversion),
            new SqlParameter("@EXCHANGE_RATE_AND_DATA_CONVERSION", currencySetupModel.exchangeRateAndDataConversion),
            new SqlParameter("@CURRENCY_REPORT_HEADERS", currencySetupModel.currencyReportHeaders),
            new SqlParameter("@CURRENCY_VISUALIZATION", currencySetupModel.currencyVisulization),
            new SqlParameter("@CURRENCY_VALUE", currencySetupModel.currencyValue),
            new SqlParameter("@CURRENCY_APPLY_AND_ENFORCE", currencySetupModel.currencyApplyAndEnforce),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){Direction = ParameterDirection.Output }

        };

            string spQuery = StoreProcedureConstants.Sp_SaveCurrencySetup + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                " @CURRENCY_TYPE, @CURRENCY_TYPE_SIGN,@CURRENCY_IMAGE, @CURRENCY_COLLECTED_DATA," +
                " @CURRENCY_PREPARING_DATA, @CURRENCY_PRESENTING_DATA,@CURRENCY_CONVERSION, " +
                "@EXCHANGE_RATE_AND_DATA_CONVERSION," +
                " @CURRENCY_REPORT_HEADERS, @CURRENCY_VISUALIZATION,@CURRENCY_VALUE, @CURRENCY_APPLY_AND_ENFORCE," +
                "  @V_MESSAGE OUTPUT";

            return _CurrencySetupRepository.ExecuteCommand(spQuery, parameters);
            

        }
    }
}
