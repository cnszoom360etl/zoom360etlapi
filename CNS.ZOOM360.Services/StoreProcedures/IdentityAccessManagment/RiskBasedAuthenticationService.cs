﻿using CNS.ZOOM360.Entities.StoreProcedures.IdentityAccessManagment;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.IdentityAccessManagment;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.IdentityAccessManagment
{
    public class RiskBasedAuthenticationService: IRiskBaseAuthenticationService

    {
		private readonly IRepositoryBase<RiskBaseAuthenticationModel> _riskBasedAuthSetupRepository;
		public RiskBasedAuthenticationService(IRepositoryBase<RiskBaseAuthenticationModel> riskBasedAuthSetupRepository)
		{
			_riskBasedAuthSetupRepository = riskBasedAuthSetupRepository;
		}
	public async Task<string> SaveRiskBaseAuthenticationSetup(RiskBaseAuthenticationModel riskBaseAuthenticationModel) {
			object[] parameters = {
			new SqlParameter("@USER_ID", string.IsNullOrEmpty(riskBaseAuthenticationModel.UserId) ? (object)DBNull.Value: riskBaseAuthenticationModel.UserId),
			new SqlParameter("@WORKSPACE_ID",string.IsNullOrEmpty((riskBaseAuthenticationModel.WorkspaceId).ToString()) ? (object)DBNull.Value: riskBaseAuthenticationModel.WorkspaceId),
			new SqlParameter("@CLIENT_ID", string.IsNullOrEmpty((riskBaseAuthenticationModel.ClientId).ToString()) ? (object)DBNull.Value: riskBaseAuthenticationModel.ClientId),
			new SqlParameter("@RBA_ACTIVATION", string.IsNullOrEmpty(riskBaseAuthenticationModel.RBAActivation) ? (object)DBNull.Value: riskBaseAuthenticationModel.RBAActivation),
			new SqlParameter("@RBA_ACCESS", string.IsNullOrEmpty(riskBaseAuthenticationModel.RBAAccess) ? (object)DBNull.Value: riskBaseAuthenticationModel.RBAAccess),
			new SqlParameter("@SUPERVISOR_RISKSUMMARY", riskBaseAuthenticationModel.SupervisorSummary),
			new SqlParameter("@USER_RISKSUMMARY",riskBaseAuthenticationModel.UserRiskSummary),
			new SqlParameter("@IP_ACCESS",riskBaseAuthenticationModel.IPAccess),
			new SqlParameter("@IP_VERIFICATION", string.IsNullOrEmpty(riskBaseAuthenticationModel.IPVerification) ? (object)DBNull.Value: riskBaseAuthenticationModel.IPVerification),
			new SqlParameter("@MAC_ACCESS",riskBaseAuthenticationModel.MacAccess),
			new SqlParameter("@MAC_VERIFICATION", string.IsNullOrEmpty(riskBaseAuthenticationModel.MacVerification) ? (object)DBNull.Value: riskBaseAuthenticationModel.MacVerification),
			new SqlParameter("@TIMEZONE_ACCESS",riskBaseAuthenticationModel.TimeZoneAccess),
			new SqlParameter("@TIMEZONE_VERIFICATION", string.IsNullOrEmpty(riskBaseAuthenticationModel.TimeZoneVerification) ? (object)DBNull.Value: riskBaseAuthenticationModel.TimeZoneVerification),
			new SqlParameter("@TIMESLOT_ACCESS",riskBaseAuthenticationModel.TimeSlotAccess),
			new SqlParameter("@STARTTIME_VERIFICATION", string.IsNullOrEmpty(riskBaseAuthenticationModel.TimeZoneVerification) ? (object)DBNull.Value: riskBaseAuthenticationModel.TimeZoneVerification),
			new SqlParameter("@ENDTIME_VERIFICATION", string.IsNullOrEmpty(riskBaseAuthenticationModel.EndTimeVerification) ? (object)DBNull.Value: riskBaseAuthenticationModel.EndTimeVerification),
			new SqlParameter("@DEVICETYPE_ACCESS",riskBaseAuthenticationModel.DeviceTypeAcces),
			new SqlParameter("@DEVICETYPE_VERIFICATION", string.IsNullOrEmpty(riskBaseAuthenticationModel.DeviceTypeVerification) ? (object)DBNull.Value: riskBaseAuthenticationModel.DeviceTypeVerification),
			new SqlParameter("@CONNECTIONTYPE_ACCESS",riskBaseAuthenticationModel.ConnectionTypeAccess),
			new SqlParameter("@CONNECTIONTYPE_VERIFICATION", string.IsNullOrEmpty(riskBaseAuthenticationModel.ConnectionTypeVerification) ? (object)DBNull.Value: riskBaseAuthenticationModel.ConnectionTypeVerification),
			new SqlParameter("@SUPERVISOR_ACCESS",riskBaseAuthenticationModel.SupervisorAccess),
			new SqlParameter("@SUPERVISOR_VERIFICATION",riskBaseAuthenticationModel.ConnectionTypeVerification),
			new SqlParameter("@APPEARANCE_LOGO",string.IsNullOrEmpty(riskBaseAuthenticationModel.AppearanceLogo) ? (object)DBNull.Value: riskBaseAuthenticationModel.AppearanceLogo),
			new SqlParameter("@APPEARANCE_COLOR",string.IsNullOrEmpty(riskBaseAuthenticationModel.AppearanceColor) ? (object)DBNull.Value: riskBaseAuthenticationModel.AppearanceColor),
			new SqlParameter("@USER_NAME_INSERT",string.IsNullOrEmpty(riskBaseAuthenticationModel.UsernameInsert) ? (object)DBNull.Value: riskBaseAuthenticationModel.UsernameInsert),
			new SqlParameter("@USER_NAME_UPDATE",string.IsNullOrEmpty(riskBaseAuthenticationModel.UsernameUpdate) ? (object)DBNull.Value: riskBaseAuthenticationModel.UsernameUpdate),
			new SqlParameter("@SERVER_INSERT_DATE",string.IsNullOrEmpty(riskBaseAuthenticationModel.ServerInsertDate) ? (object)DBNull.Value: riskBaseAuthenticationModel.ServerInsertDate),
			new SqlParameter("@SERVER_INSERT_TIME",string.IsNullOrEmpty(riskBaseAuthenticationModel.ServerInsertTime) ? (object)DBNull.Value: riskBaseAuthenticationModel.ServerInsertTime),
			new SqlParameter("@SERVER_INSERT_TIME_ZONE",string.IsNullOrEmpty(riskBaseAuthenticationModel.ServerInsertTimeZone) ? (object)DBNull.Value: riskBaseAuthenticationModel.ServerInsertTimeZone),
			new SqlParameter("@SERVER_UPDATE_DATE",string.IsNullOrEmpty(riskBaseAuthenticationModel.ServerUpdateDate) ? (object)DBNull.Value: riskBaseAuthenticationModel.ServerUpdateDate),
			new SqlParameter("@SERVER_UPDATE_TIME",string.IsNullOrEmpty(riskBaseAuthenticationModel.ServerUpdateTime) ? (object)DBNull.Value: riskBaseAuthenticationModel.ServerUpdateTime),
			new SqlParameter("@SERVER_UPDATE_TIME_ZONE",string.IsNullOrEmpty(riskBaseAuthenticationModel.ServerUpdateTimeZone) ? (object)DBNull.Value: riskBaseAuthenticationModel.ServerUpdateTimeZone),
			new SqlParameter("@CLIENT_INSERT_DATE",string.IsNullOrEmpty(riskBaseAuthenticationModel.ClientInsertDate) ? (object)DBNull.Value: riskBaseAuthenticationModel.ClientInsertDate),
			new SqlParameter("@CLIENT_INSERT_TIME",string.IsNullOrEmpty(riskBaseAuthenticationModel.ClientInsertTime) ? (object)DBNull.Value: riskBaseAuthenticationModel.ClientInsertTime),
			new SqlParameter("@CLIENT_INSERT_TIME_ZONE",string.IsNullOrEmpty(riskBaseAuthenticationModel.ClientInsertTimeZone) ? (object)DBNull.Value: riskBaseAuthenticationModel.ClientInsertTimeZone),
			new SqlParameter("@CLIENT_UPDATE_DATE",string.IsNullOrEmpty(riskBaseAuthenticationModel.ClientUpdateDate) ? (object)DBNull.Value: riskBaseAuthenticationModel.ClientUpdateDate),
			new SqlParameter("@CLIENT_UPDATE_TIME",string.IsNullOrEmpty(riskBaseAuthenticationModel.ClientUpdateTime) ? (object)DBNull.Value: riskBaseAuthenticationModel.ClientUpdateTime),
			new SqlParameter("@CLIENT_UPDATE_TIME_ZONE",string.IsNullOrEmpty(riskBaseAuthenticationModel.ClientUpdateTimeZone) ? (object)DBNull.Value: riskBaseAuthenticationModel.ClientUpdateTimeZone),
			new SqlParameter("@BSTATUS",string.IsNullOrEmpty(riskBaseAuthenticationModel.BStatus) ? (object)DBNull.Value: riskBaseAuthenticationModel.BStatus),
			new SqlParameter("@BDELETE",string.IsNullOrEmpty(riskBaseAuthenticationModel.BDelete) ? (object)DBNull.Value: riskBaseAuthenticationModel.BDelete),
			new SqlParameter("@BMAP",string.IsNullOrEmpty(riskBaseAuthenticationModel.BMap) ? (object)DBNull.Value: riskBaseAuthenticationModel.BMap),
			new SqlParameter("@REMARKS_1",string.IsNullOrEmpty(riskBaseAuthenticationModel.Remark1) ? (object)DBNull.Value: riskBaseAuthenticationModel.Remark1),
			new SqlParameter("@REMARKS_2",string.IsNullOrEmpty(riskBaseAuthenticationModel.Remark2) ? (object)DBNull.Value: riskBaseAuthenticationModel.Remark2),
			new SqlParameter("@REMARKS_3",string.IsNullOrEmpty(riskBaseAuthenticationModel.Remark3) ? (object)DBNull.Value: riskBaseAuthenticationModel.Remark3),
			new SqlParameter("@REMARKS_4",string.IsNullOrEmpty(riskBaseAuthenticationModel.Remark4) ? (object)DBNull.Value: riskBaseAuthenticationModel.Remark4),
			new SqlParameter("@FLEX_1",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex1) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex1),
			new SqlParameter("@FLEX_2",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex2) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex2),
			new SqlParameter("@FLEX_3",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex3) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex3),
			new SqlParameter("@FLEX_4",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex4) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex4),
			new SqlParameter("@FLEX_5",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex5) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex5),
			new SqlParameter("@FLEX_6",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex6) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex6),
			new SqlParameter("@FLEX_7",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex7) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex7),
			new SqlParameter("@FLEX_8",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex8) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex8),
			new SqlParameter("@FLEX_9",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex9) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex8),
			new SqlParameter("@FLEX_10",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex10) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex10),
			new SqlParameter("@FLEX_11",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex11) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex11),
			new SqlParameter("@FLEX_12",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex12) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex12),
			new SqlParameter("@FLEX_13",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex13) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex13),
			new SqlParameter("@FLEX_14",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex14) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex14),
			new SqlParameter("@FLEX_15",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex15) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex15),
			new SqlParameter("@FLEX_16",string.IsNullOrEmpty(riskBaseAuthenticationModel.Flex16) ? (object)DBNull.Value: riskBaseAuthenticationModel.Flex16),
			new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
		};

			string spQuery = StoreProcedureConstants.Sp_SaveRBASetup + " @USER_ID, @WORKSPACE_ID, @CLIENT_ID," +
			" @RBA_ACTIVATION, @RBA_ACCESS, @SUPERVISOR_RISKSUMMARY, @USER_RISKSUMMARY, @IP_ACCESS, @IP_VERIFICATION," +
			" @MAC_ACCESS, @MAC_VERIFICATION, @TIMEZONE_ACCESS, @TIMEZONE_VERIFICATION,@TIMESLOT_ACCESS,@STARTTIME_VERIFICATION," +
			" @ENDTIME_VERIFICATION, @DEVICETYPE_ACCESS, @DEVICETYPE_VERIFICATION, @CONNECTIONTYPE_ACCESS,@CONNECTIONTYPE_VERIFICATION,@SUPERVISOR_ACCESS,@SUPERVISOR_VERIFICATION," +
			" @APPEARANCE_LOGO, @APPEARANCE_COLOR, @USER_NAME_INSERT, @USER_NAME_UPDATE, @SERVER_INSERT_DATE, @SERVER_INSERT_TIME, @SERVER_INSERT_TIME_ZONE, @SERVER_UPDATE_DATE," +
			" @SERVER_UPDATE_TIME, @SERVER_UPDATE_TIME_ZONE, @CLIENT_INSERT_DATE, @CLIENT_INSERT_TIME, @CLIENT_INSERT_TIME_ZONE," +
			" @CLIENT_UPDATE_DATE, @CLIENT_UPDATE_TIME, @CLIENT_UPDATE_TIME_ZONE, @BSTATUS,	@BDELETE, @BMAP," +
			" @REMARKS_1, @REMARKS_2, @REMARKS_3, @REMARKS_4, @FLEX_1, @FLEX_2, @FLEX_3,@FLEX_4,@FLEX_5, @FLEX_6," +
			" @FLEX_7, @FLEX_8, @FLEX_9, @FLEX_10, @FLEX_11, @FLEX_12, @FLEX_13, @FLEX_14, @FLEX_15, @FLEX_16 ," +
			" @V_MESSAGE OUTPUT";
			return _riskBasedAuthSetupRepository.ExecuteCommand(spQuery, parameters);
		}
    }
}
