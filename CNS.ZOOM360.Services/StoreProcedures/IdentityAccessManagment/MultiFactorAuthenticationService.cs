﻿using CNS.ZOOM360.Entities.StoreProcedures.IdentityAccessManagment;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.IdentityAccessManagment;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.IdentityAccessManagment
{
    public class MultiFactorAuthenticationService: IMultiFactorAuthenticationService
    {
        private readonly IRepositoryBase<MultiFactorAuthenticationModel> _multiFactorAuthenticationRepository;
        public MultiFactorAuthenticationService(IRepositoryBase<MultiFactorAuthenticationModel> multiFactorAuthenticationRepository)
        {
            _multiFactorAuthenticationRepository = multiFactorAuthenticationRepository;
        }
		public async Task<string> SaveMultiFactorAuthenticationSetup(MultiFactorAuthenticationModel multiFactorAuthenticationModel)
		{
			object[] parameters = {
			new SqlParameter("@USER_ID", string.IsNullOrEmpty(multiFactorAuthenticationModel.UserId) ? (object)DBNull.Value: multiFactorAuthenticationModel.UserId),
			new SqlParameter("@WORKSPACE_ID",string.IsNullOrEmpty((multiFactorAuthenticationModel.WorkspaceId).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.WorkspaceId),
			new SqlParameter("@CLIENT_ID", string.IsNullOrEmpty((multiFactorAuthenticationModel.ClientId).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.ClientId),
			new SqlParameter("@SECURITY_QUESTION", string.IsNullOrEmpty(multiFactorAuthenticationModel.SecurityQuestion) ? (object)DBNull.Value: multiFactorAuthenticationModel.SecurityQuestion),
			new SqlParameter("@SQ_FIRSTTIME_LOGIN",multiFactorAuthenticationModel.SQFirstTimeLogin),
			new SqlParameter("@SQ_PASSWORD_UPDATION",multiFactorAuthenticationModel.SQPasswordUpdation),
			new SqlParameter("@SECURITYQUESTION_OPTIONS", string.IsNullOrEmpty(multiFactorAuthenticationModel.SecurityQestionOption) ? (object)DBNull.Value:multiFactorAuthenticationModel.SecurityQestionOption),
			new SqlParameter("@SECURITYQUESTION_ANSWER",string.IsNullOrEmpty(multiFactorAuthenticationModel.SecurityQestionAnswer) ? (object)DBNull.Value: multiFactorAuthenticationModel.SecurityQestionAnswer),
			new SqlParameter("@PASSCODE_AUTHENTICATION",string.IsNullOrEmpty(multiFactorAuthenticationModel.PasscodeAuthentication) ? (object)DBNull.Value: multiFactorAuthenticationModel.PasscodeAuthentication),
			new SqlParameter("@PA_FIRSTTIME_LOGIN",multiFactorAuthenticationModel.PAFirsttimeLogin),
			new SqlParameter("@PA_PASSWORD_UPDATION", string.IsNullOrEmpty((multiFactorAuthenticationModel.PAPasswordUpdation).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.PAPasswordUpdation),
			new SqlParameter("@PASSCODE_EMAIL", string.IsNullOrEmpty((multiFactorAuthenticationModel.PasscodeEmail).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.PasscodeEmail),
			new SqlParameter("@PASSCODE_SMS", string.IsNullOrEmpty((multiFactorAuthenticationModel.PasscodeSMS).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.PasscodeSMS),
			new SqlParameter("@PASSCODE_SINGLEPART", string.IsNullOrEmpty((multiFactorAuthenticationModel.PasscodeSinglepart).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.PasscodeSinglepart),
			new SqlParameter("@PASSCODE_TWOPARTS", string.IsNullOrEmpty((multiFactorAuthenticationModel.PasscodeTwopart).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.PasscodeTwopart),
			new SqlParameter("@PASSCODE_VALIDITY_TIME", string.IsNullOrEmpty((multiFactorAuthenticationModel.PasscodeValidityTime).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.PasscodeValidityTime),
			new SqlParameter("@PASSCODE_VALIDITY_ATTEMPTS", string.IsNullOrEmpty((multiFactorAuthenticationModel.PasscodeValidityAttempts).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.PasscodeValidityAttempts),
			new SqlParameter("@CRYPTOGRAPHIC_TOKENS", string.IsNullOrEmpty((multiFactorAuthenticationModel.CryptographicTokens).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.CryptographicTokens),
			new SqlParameter("@CT_FIRSTTIME_LOGIN", string.IsNullOrEmpty((multiFactorAuthenticationModel.CTFirsttimeLogin).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.CTFirsttimeLogin),
			new SqlParameter("@CT_PASSWORD_UPDATION", string.IsNullOrEmpty((multiFactorAuthenticationModel.CTPasswordUpdation).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.CTPasswordUpdation),
			new SqlParameter("@QR_CODE", string.IsNullOrEmpty((multiFactorAuthenticationModel.QRCode).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.QRCode),
			new SqlParameter("@QR_FIRSTTIME_LOGIN", string.IsNullOrEmpty((multiFactorAuthenticationModel.QRFirsttimeLogin).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.QRFirsttimeLogin),
			new SqlParameter("@QR_PASSWORD_UPDATION", string.IsNullOrEmpty((multiFactorAuthenticationModel.QRPasswordUpdation).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.QRPasswordUpdation),
			new SqlParameter("@FACE_ID", string.IsNullOrEmpty((multiFactorAuthenticationModel.FaceID).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.FaceID),
			new SqlParameter("@FI_FIRSTTIME_LOGIN", string.IsNullOrEmpty((multiFactorAuthenticationModel.FIFirsttimeLogin).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.FIFirsttimeLogin),
			new SqlParameter("@FI_PASSWORD_UPDATION", string.IsNullOrEmpty((multiFactorAuthenticationModel.FIPasswordUpdation).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.FIPasswordUpdation),
			new SqlParameter("@SUPERVISORY_APPROVAL", string.IsNullOrEmpty((multiFactorAuthenticationModel.SupervisoryApproval).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.SupervisoryApproval),
			new SqlParameter("@SA_FIRSTTIME_LOGIN", string.IsNullOrEmpty((multiFactorAuthenticationModel.SAFirsttimeLogin).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.SAFirsttimeLogin),
			new SqlParameter("@SA_PASSWORD_UPDATION", string.IsNullOrEmpty((multiFactorAuthenticationModel.SAPasswordUpdation).ToString()) ? (object)DBNull.Value: multiFactorAuthenticationModel.SAPasswordUpdation),
			new SqlParameter("@APPEARANCE_LOGO",string.IsNullOrEmpty(multiFactorAuthenticationModel.AppearanceLogo) ? (object)DBNull.Value: multiFactorAuthenticationModel.AppearanceLogo),
			new SqlParameter("@APPEARANCE_COLOR",string.IsNullOrEmpty(multiFactorAuthenticationModel.AppearanceColor) ? (object)DBNull.Value: multiFactorAuthenticationModel.AppearanceColor),
			new SqlParameter("@USER_NAME_INSERT",string.IsNullOrEmpty(multiFactorAuthenticationModel.UsernameInsert) ? (object)DBNull.Value: multiFactorAuthenticationModel.UsernameInsert),
			new SqlParameter("@USER_NAME_UPDATE",string.IsNullOrEmpty(multiFactorAuthenticationModel.UsernameUpdate) ? (object)DBNull.Value: multiFactorAuthenticationModel.UsernameUpdate),
			new SqlParameter("@SERVER_INSERT_DATE",string.IsNullOrEmpty(multiFactorAuthenticationModel.ServerInsertDate) ? (object)DBNull.Value: multiFactorAuthenticationModel.ServerInsertDate),
			new SqlParameter("@SERVER_INSERT_TIME",string.IsNullOrEmpty(multiFactorAuthenticationModel.ServerInsertTime) ? (object)DBNull.Value: multiFactorAuthenticationModel.ServerInsertTime),
			new SqlParameter("@SERVER_INSERT_TIME_ZONE",string.IsNullOrEmpty(multiFactorAuthenticationModel.ServerInsertTimeZone) ? (object)DBNull.Value: multiFactorAuthenticationModel.ServerInsertTimeZone),
			new SqlParameter("@SERVER_UPDATE_DATE",string.IsNullOrEmpty(multiFactorAuthenticationModel.ServerUpdateDate) ? (object)DBNull.Value: multiFactorAuthenticationModel.ServerUpdateDate),
			new SqlParameter("@SERVER_UPDATE_TIME",string.IsNullOrEmpty(multiFactorAuthenticationModel.ServerUpdateTime) ? (object)DBNull.Value: multiFactorAuthenticationModel.ServerUpdateTime),
			new SqlParameter("@SERVER_UPDATE_TIME_ZONE",string.IsNullOrEmpty(multiFactorAuthenticationModel.ServerUpdateTimeZone) ? (object)DBNull.Value: multiFactorAuthenticationModel.ServerUpdateTimeZone),
			new SqlParameter("@CLIENT_INSERT_DATE",string.IsNullOrEmpty(multiFactorAuthenticationModel.ClientInsertDate) ? (object)DBNull.Value: multiFactorAuthenticationModel.ClientInsertDate),
			new SqlParameter("@CLIENT_INSERT_TIME",string.IsNullOrEmpty(multiFactorAuthenticationModel.ClientInsertTime) ? (object)DBNull.Value: multiFactorAuthenticationModel.ClientInsertTime),
			new SqlParameter("@CLIENT_INSERT_TIME_ZONE",string.IsNullOrEmpty(multiFactorAuthenticationModel.ClientInsertTimeZone) ? (object)DBNull.Value: multiFactorAuthenticationModel.ClientInsertTimeZone),
			new SqlParameter("@CLIENT_UPDATE_DATE",string.IsNullOrEmpty(multiFactorAuthenticationModel.ClientUpdateDate) ? (object)DBNull.Value: multiFactorAuthenticationModel.ClientUpdateDate),
			new SqlParameter("@CLIENT_UPDATE_TIME",string.IsNullOrEmpty(multiFactorAuthenticationModel.ClientUpdateTime) ? (object)DBNull.Value: multiFactorAuthenticationModel.ClientUpdateTime),
			new SqlParameter("@CLIENT_UPDATE_TIME_ZONE",string.IsNullOrEmpty(multiFactorAuthenticationModel.ClientUpdateTimeZone) ? (object)DBNull.Value: multiFactorAuthenticationModel.ClientUpdateTimeZone),
			new SqlParameter("@BSTATUS",string.IsNullOrEmpty(multiFactorAuthenticationModel.BStatus) ? (object)DBNull.Value: multiFactorAuthenticationModel.BStatus),
			new SqlParameter("@BDELETE",string.IsNullOrEmpty(multiFactorAuthenticationModel.BDelete) ? (object)DBNull.Value: multiFactorAuthenticationModel.BDelete),
			new SqlParameter("@BMAP",string.IsNullOrEmpty(multiFactorAuthenticationModel.BMap) ? (object)DBNull.Value: multiFactorAuthenticationModel.BMap),
			new SqlParameter("@REMARKS_1",string.IsNullOrEmpty(multiFactorAuthenticationModel.Remark1) ? (object)DBNull.Value: multiFactorAuthenticationModel.Remark1),
			new SqlParameter("@REMARKS_2",string.IsNullOrEmpty(multiFactorAuthenticationModel.Remark2) ? (object)DBNull.Value: multiFactorAuthenticationModel.Remark2),
			new SqlParameter("@REMARKS_3",string.IsNullOrEmpty(multiFactorAuthenticationModel.Remark3) ? (object)DBNull.Value: multiFactorAuthenticationModel.Remark3),
			new SqlParameter("@REMARKS_4",string.IsNullOrEmpty(multiFactorAuthenticationModel.Remark4) ? (object)DBNull.Value: multiFactorAuthenticationModel.Remark4),
			new SqlParameter("@FLEX_1",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex1) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex1),
			new SqlParameter("@FLEX_2",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex2) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex2),
			new SqlParameter("@FLEX_3",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex3) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex3),
			new SqlParameter("@FLEX_4",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex4) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex4),
			new SqlParameter("@FLEX_5",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex5) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex5),
			new SqlParameter("@FLEX_6",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex6) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex6),
			new SqlParameter("@FLEX_7",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex7) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex7),
			new SqlParameter("@FLEX_8",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex8) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex8),
			new SqlParameter("@FLEX_9",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex9) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex8),
			new SqlParameter("@FLEX_10",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex10) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex10),
			new SqlParameter("@FLEX_11",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex11) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex11),
			new SqlParameter("@FLEX_12",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex12) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex12),
			new SqlParameter("@FLEX_13",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex13) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex13),
			new SqlParameter("@FLEX_14",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex14) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex14),
			new SqlParameter("@FLEX_15",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex15) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex15),
			new SqlParameter("@FLEX_16",string.IsNullOrEmpty(multiFactorAuthenticationModel.Flex16) ? (object)DBNull.Value: multiFactorAuthenticationModel.Flex16),
			new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
		};

			string spQuery = StoreProcedureConstants.Sp_SaveMFASetup + " @USER_ID, @WORKSPACE_ID, @CLIENT_ID," +
			" @SECURITY_QUESTION, @SQ_FIRSTTIME_LOGIN, @SQ_PASSWORD_UPDATION, @SECURITYQUESTION_OPTIONS, @SECURITYQUESTION_ANSWER," +
			" @PASSCODE_AUTHENTICATION, @PA_FIRSTTIME_LOGIN, @PA_PASSWORD_UPDATION, @PASSCODE_EMAIL, @PASSCODE_SMS, @PASSCODE_SINGLEPART," +
			" @PASSCODE_TWOPARTS, @PASSCODE_VALIDITY_TIME, @PASSCODE_VALIDITY_ATTEMPTS, @CRYPTOGRAPHIC_TOKENS, @CT_FIRSTTIME_LOGIN," +
			" @CT_PASSWORD_UPDATION, @QR_CODE, @QR_FIRSTTIME_LOGIN, @QR_PASSWORD_UPDATION, @FACE_ID, @FI_FIRSTTIME_LOGIN, @FI_PASSWORD_UPDATION, @SUPERVISORY_APPROVAL, @SA_FIRSTTIME_LOGIN, @SA_PASSWORD_UPDATION," +
			" @APPEARANCE_LOGO, @APPEARANCE_COLOR, @USER_NAME_INSERT, @USER_NAME_UPDATE, @SERVER_INSERT_DATE, @SERVER_INSERT_TIME, @SERVER_INSERT_TIME_ZONE, @SERVER_UPDATE_DATE," +
			" @SERVER_UPDATE_TIME, @SERVER_UPDATE_TIME_ZONE, @CLIENT_INSERT_DATE, @CLIENT_INSERT_TIME, @CLIENT_INSERT_TIME_ZONE," +
			" @CLIENT_UPDATE_DATE, @CLIENT_UPDATE_TIME, @CLIENT_UPDATE_TIME_ZONE, @BSTATUS,	@BDELETE, @BMAP," +
			" @REMARKS_1, @REMARKS_2, @REMARKS_3, @REMARKS_4, @FLEX_1, @FLEX_2, @FLEX_3,@FLEX_4,@FLEX_5, @FLEX_6," +
			" @FLEX_7, @FLEX_8, @FLEX_9, @FLEX_10, @FLEX_11, @FLEX_12, @FLEX_13, @FLEX_14, @FLEX_15, @FLEX_16 ," +
			" @V_MESSAGE OUTPUT";
			return _multiFactorAuthenticationRepository.ExecuteCommand(spQuery, parameters);
		}

	}
}
