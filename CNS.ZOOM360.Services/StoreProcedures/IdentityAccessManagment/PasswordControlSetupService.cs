﻿using CNS.ZOOM360.Entities.StoreProcedures.IdentityAccessManagment;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.IdentityAccessManagment;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.IdentityAccessManagment
{
    public class PasswordControlSetupService: IPasswordControlSetupService
    {
		private readonly IRepositoryBase<PasswordControlSetupModel> _passwordControlSetupRepository;
		public PasswordControlSetupService(IRepositoryBase<PasswordControlSetupModel> passwordControlSetupRepository)
		{
			_passwordControlSetupRepository = passwordControlSetupRepository;
		}
		public async Task<string> SavePasswordControlSetup(PasswordControlSetupModel passwordControlSetupModel)
		{
			object[] parameters = {
			new SqlParameter("@USER_ID", passwordControlSetupModel.UserId),
			new SqlParameter("@WORKSPACE_ID", passwordControlSetupModel.WorkspaceId),
			new SqlParameter("@CLIENT_ID", passwordControlSetupModel.ClientId),
			new SqlParameter("@PASSWORD_LENGTH",string.IsNullOrEmpty(passwordControlSetupModel.PasswordLength) ? (object)DBNull.Value: passwordControlSetupModel.PasswordLength),
			new SqlParameter("@STRONG_PASSWORD",string.IsNullOrEmpty(passwordControlSetupModel.StrongPassword) ? (object)DBNull.Value: passwordControlSetupModel.StrongPassword),
			new SqlParameter("@PASSWORD_FIELD",string.IsNullOrEmpty(passwordControlSetupModel.PasswordField) ? (object)DBNull.Value: passwordControlSetupModel.PasswordField),
			new SqlParameter("@PASSWORD_EXPIRED",string.IsNullOrEmpty(passwordControlSetupModel.PasswordExpired) ? (object)DBNull.Value: passwordControlSetupModel.PasswordExpired),
			new SqlParameter("@FIRST_LOGIN_OPTION",passwordControlSetupModel.FirstLoginOption),
			new SqlParameter("@FIRST_LOGIN_DAYS",passwordControlSetupModel.FirstLoginDays ),
			new SqlParameter("@USER_CREATED_DAYS",passwordControlSetupModel.UserCreatedDays),
			new SqlParameter("@LOGIN_ATTEMPTS",passwordControlSetupModel.LoginAttempts),
			new SqlParameter("@LAST_PASSWORD_CHANGED",passwordControlSetupModel.LastPasswordChanged),
			new SqlParameter("@NOTIFY_USER_PASSWORDEXPIRY",passwordControlSetupModel.NotifyUserPasswordExpiry),
			new SqlParameter("@NOTIFY_USER_PASSWORDEXPIRY_DAYS",string.IsNullOrEmpty(passwordControlSetupModel.NotifyUserPasswordExpiry) ? (object)DBNull.Value:  passwordControlSetupModel.NotifyUserPasswordExpiryDays),
			new SqlParameter("@NOTIFY_SUPERVISOR_PASSWORDEXPIRY",string.IsNullOrEmpty(passwordControlSetupModel.NotifySupervisorPasswordExpiry) ? (object)DBNull.Value:  passwordControlSetupModel.NotifySupervisorPasswordExpiry),
			new SqlParameter("@NOTIFY_SUPERVISOR_PASSWORDEXPIRY_DAYS",passwordControlSetupModel.NotifySupervisorPasswordExpiryDays),
			new SqlParameter("@APPEARANCE_LOGO",string.IsNullOrEmpty(passwordControlSetupModel.AppearanceLogo) ? (object)DBNull.Value: passwordControlSetupModel.AppearanceLogo),
			new SqlParameter("@APPEARANCE_COLOR",string.IsNullOrEmpty(passwordControlSetupModel.AppearanceColor) ? (object)DBNull.Value: passwordControlSetupModel.AppearanceColor),
			new SqlParameter("@USER_NAME_INSERT",string.IsNullOrEmpty(passwordControlSetupModel.UsernameInsert) ? (object)DBNull.Value: passwordControlSetupModel.UsernameInsert),
			new SqlParameter("@USER_NAME_UPDATE",string.IsNullOrEmpty(passwordControlSetupModel.UsernameUpdate) ? (object)DBNull.Value: passwordControlSetupModel.UsernameUpdate),
			new SqlParameter("@SERVER_INSERT_DATE",string.IsNullOrEmpty(passwordControlSetupModel.ServerInsertDate) ? (object)DBNull.Value: passwordControlSetupModel.ServerInsertDate),
			new SqlParameter("@SERVER_INSERT_TIME",string.IsNullOrEmpty(passwordControlSetupModel.ServerInsertTime) ? (object)DBNull.Value: passwordControlSetupModel.ServerInsertTime),
			new SqlParameter("@SERVER_INSERT_TIME_ZONE",string.IsNullOrEmpty(passwordControlSetupModel.ServerInsertTimeZone) ? (object)DBNull.Value: passwordControlSetupModel.ServerInsertTimeZone),
			new SqlParameter("@SERVER_UPDATE_DATE",string.IsNullOrEmpty(passwordControlSetupModel.ServerUpdateDate) ? (object)DBNull.Value: passwordControlSetupModel.ServerUpdateDate),
			new SqlParameter("@SERVER_UPDATE_TIME",string.IsNullOrEmpty(passwordControlSetupModel.ServerUpdateTime) ? (object)DBNull.Value: passwordControlSetupModel.ServerUpdateTime),
			new SqlParameter("@SERVER_UPDATE_TIME_ZONE",string.IsNullOrEmpty(passwordControlSetupModel.ServerUpdateTimeZone) ? (object)DBNull.Value: passwordControlSetupModel.ServerUpdateTimeZone),
			new SqlParameter("@CLIENT_INSERT_DATE",string.IsNullOrEmpty(passwordControlSetupModel.ClientInsertDate) ? (object)DBNull.Value: passwordControlSetupModel.ClientInsertDate),
			new SqlParameter("@CLIENT_INSERT_TIME",string.IsNullOrEmpty(passwordControlSetupModel.ClientInsertTime) ? (object)DBNull.Value: passwordControlSetupModel.ClientInsertTime),
			new SqlParameter("@CLIENT_INSERT_TIME_ZONE",string.IsNullOrEmpty(passwordControlSetupModel.ClientInsertTimeZone) ? (object)DBNull.Value: passwordControlSetupModel.ClientInsertTimeZone),
			new SqlParameter("@CLIENT_UPDATE_DATE",string.IsNullOrEmpty(passwordControlSetupModel.ClientUpdateDate) ? (object)DBNull.Value: passwordControlSetupModel.ClientUpdateDate),
			new SqlParameter("@CLIENT_UPDATE_TIME",string.IsNullOrEmpty(passwordControlSetupModel.ClientUpdateTime) ? (object)DBNull.Value: passwordControlSetupModel.ClientUpdateTime),
			new SqlParameter("@CLIENT_UPDATE_TIME_ZONE",string.IsNullOrEmpty(passwordControlSetupModel.ClientUpdateTimeZone) ? (object)DBNull.Value: passwordControlSetupModel.ClientUpdateTimeZone),
			new SqlParameter("@BSTATUS",string.IsNullOrEmpty(passwordControlSetupModel.BStatus) ? (object)DBNull.Value: passwordControlSetupModel.BStatus),
			new SqlParameter("@BDELETE",string.IsNullOrEmpty(passwordControlSetupModel.BDelete) ? (object)DBNull.Value: passwordControlSetupModel.BDelete),
			new SqlParameter("@BMAP",string.IsNullOrEmpty(passwordControlSetupModel.BMap) ? (object)DBNull.Value: passwordControlSetupModel.BMap),
			new SqlParameter("@REMARKS_1",string.IsNullOrEmpty(passwordControlSetupModel.Remark1) ? (object)DBNull.Value: passwordControlSetupModel.Remark1),
			new SqlParameter("@REMARKS_2",string.IsNullOrEmpty(passwordControlSetupModel.Remark2) ? (object)DBNull.Value: passwordControlSetupModel.Remark2),
			new SqlParameter("@REMARKS_3",string.IsNullOrEmpty(passwordControlSetupModel.Remark3) ? (object)DBNull.Value: passwordControlSetupModel.Remark3),
			new SqlParameter("@REMARKS_4",string.IsNullOrEmpty(passwordControlSetupModel.Remark4) ? (object)DBNull.Value: passwordControlSetupModel.Remark4),
			new SqlParameter("@FLEX_1",string.IsNullOrEmpty(passwordControlSetupModel.Flex1) ? (object)DBNull.Value: passwordControlSetupModel.Flex1),
			new SqlParameter("@FLEX_2",string.IsNullOrEmpty(passwordControlSetupModel.Flex2) ? (object)DBNull.Value: passwordControlSetupModel.Flex2),
			new SqlParameter("@FLEX_3",string.IsNullOrEmpty(passwordControlSetupModel.Flex3) ? (object)DBNull.Value: passwordControlSetupModel.Flex3),
			new SqlParameter("@FLEX_4",string.IsNullOrEmpty(passwordControlSetupModel.Flex4) ? (object)DBNull.Value: passwordControlSetupModel.Flex4),
			new SqlParameter("@FLEX_5",string.IsNullOrEmpty(passwordControlSetupModel.Flex5) ? (object)DBNull.Value: passwordControlSetupModel.Flex5),
			new SqlParameter("@FLEX_6",string.IsNullOrEmpty(passwordControlSetupModel.Flex6) ? (object)DBNull.Value: passwordControlSetupModel.Flex6),
			new SqlParameter("@FLEX_7",string.IsNullOrEmpty(passwordControlSetupModel.Flex7) ? (object)DBNull.Value: passwordControlSetupModel.Flex7),
			new SqlParameter("@FLEX_8",string.IsNullOrEmpty(passwordControlSetupModel.Flex8) ? (object)DBNull.Value: passwordControlSetupModel.Flex8),
			new SqlParameter("@FLEX_9",string.IsNullOrEmpty(passwordControlSetupModel.Flex9) ? (object)DBNull.Value: passwordControlSetupModel.Flex8),
			new SqlParameter("@FLEX_10",string.IsNullOrEmpty(passwordControlSetupModel.Flex10) ? (object)DBNull.Value: passwordControlSetupModel.Flex10),
			new SqlParameter("@FLEX_11",string.IsNullOrEmpty(passwordControlSetupModel.Flex11) ? (object)DBNull.Value: passwordControlSetupModel.Flex11),
			new SqlParameter("@FLEX_12",string.IsNullOrEmpty(passwordControlSetupModel.Flex12) ? (object)DBNull.Value: passwordControlSetupModel.Flex12),
			new SqlParameter("@FLEX_13",string.IsNullOrEmpty(passwordControlSetupModel.Flex13) ? (object)DBNull.Value: passwordControlSetupModel.Flex13),
			new SqlParameter("@FLEX_14",string.IsNullOrEmpty(passwordControlSetupModel.Flex14) ? (object)DBNull.Value: passwordControlSetupModel.Flex14),
			new SqlParameter("@FLEX_15",string.IsNullOrEmpty(passwordControlSetupModel.Flex15) ? (object)DBNull.Value: passwordControlSetupModel.Flex15),
			new SqlParameter("@FLEX_16",string.IsNullOrEmpty(passwordControlSetupModel.Flex16) ? (object)DBNull.Value: passwordControlSetupModel.Flex16),
			new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
		};

			string spQuery = StoreProcedureConstants.Sp_SavePCSetup + " @USER_ID, @WORKSPACE_ID, @CLIENT_ID," +
			" @PASSWORD_LENGTH , @STRONG_PASSWORD, @PASSWORD_FIELD, @PASSWORD_EXPIRED," +
			" @FIRST_LOGIN_OPTION, @FIRST_LOGIN_DAYS, @USER_CREATED_DAYS, LOGIN_ATTEMPTS, @LAST_PASSWORD_CHANGED," +
			" @NOTIFY_USER_PASSWORDEXPIRY, @NOTIFY_USER_PASSWORDEXPIRY_DAYS , @NOTIFY_SUPERVISOR_PASSWORDEXPIRY ,"+
			" @NOTIFY_SUPERVISOR_PASSWORDEXPIRY_DAYS, @APPEARANCE_LOGO, @APPEARANCE_COLOR, @USER_NAME_INSERT," +
			" @USER_NAME_UPDATE, @SERVER_INSERT_DATE, @SERVER_INSERT_TIME, @SERVER_INSERT_TIME_ZONE, @SERVER_UPDATE_DATE," +
			" @SERVER_UPDATE_TIME, @SERVER_UPDATE_TIME_ZONE, @CLIENT_INSERT_DATE, @CLIENT_INSERT_TIME, @CLIENT_INSERT_TIME_ZONE," +
			" @CLIENT_UPDATE_DATE, @CLIENT_UPDATE_TIME, @CLIENT_UPDATE_TIME_ZONE, @BSTATUS,	@BDELETE, @BMAP," +
			" @REMARKS_1, @REMARKS_2, @REMARKS_3, @REMARKS_4, @FLEX_1, @FLEX_2, @FLEX_3,@FLEX_4,@FLEX_5, @FLEX_6," +
			" @FLEX_7, @FLEX_8, @FLEX_9, @FLEX_10, @FLEX_11, @FLEX_12, @FLEX_13, @FLEX_14, @FLEX_15, @FLEX_16 ," +
			" @V_MESSAGE OUTPUT";
			return _passwordControlSetupRepository.ExecuteCommand(spQuery, parameters);
		}
	}
}
