﻿using CNS.ZOOM360.Entities.StoreProcedures.IdentityAccessManagment;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.IdentityAccessManagment;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.IdentityAccessManagment
{
   public class AccessLockingService: IAccessLockingService
    {
		private readonly IRepositoryBase<AccessLockingModel> _accessLockingSetupRepository;
		public AccessLockingService(IRepositoryBase<AccessLockingModel> accessLockingSetupRepository)
		{
			_accessLockingSetupRepository = accessLockingSetupRepository;
		}
		public async Task<string> SaveAccessLookingSetup(AccessLockingModel accessLockingModel)
        {
			object[] parameters = {
			new SqlParameter("@USER_ID", string.IsNullOrEmpty(accessLockingModel.UserId) ? (object)DBNull.Value: accessLockingModel.UserId),
			new SqlParameter("@WORKSPACE_ID",string.IsNullOrEmpty((accessLockingModel.WorkspaceId).ToString()) ? (object)DBNull.Value: accessLockingModel.WorkspaceId),
			new SqlParameter("@CLIENT_ID", string.IsNullOrEmpty((accessLockingModel.ClientId).ToString()) ? (object)DBNull.Value: accessLockingModel.ClientId),
			new SqlParameter("@USER_LOCKED", accessLockingModel.UserLocked),
			new SqlParameter("@FAILED_ATTEMPTS",accessLockingModel.FailedAttempt),
			new SqlParameter("@FAILED_TIMEINTERVAL",accessLockingModel.FailedTimeInterval),
			new SqlParameter("@UNLOCK_OPTIONS", string.IsNullOrEmpty(accessLockingModel.UnlockOption) ? (object)DBNull.Value:accessLockingModel.UnlockOption),
			new SqlParameter("@UNLOCK_TIMEINTERVAL",string.IsNullOrEmpty(accessLockingModel.UnlockTimeInterval) ? (object)DBNull.Value: accessLockingModel.UnlockTimeInterval),
			new SqlParameter("@UNLOCK_ADMINISTRATOR",accessLockingModel.UnlockAdministrator),
			new SqlParameter("@UNLOCK_SUPERVISIOR",accessLockingModel.UnlockSupervisor),
			new SqlParameter("@APPEARANCE_LOGO",string.IsNullOrEmpty(accessLockingModel.AppearanceLogo) ? (object)DBNull.Value: accessLockingModel.AppearanceLogo),
			new SqlParameter("@APPEARANCE_COLOR",string.IsNullOrEmpty(accessLockingModel.AppearanceColor) ? (object)DBNull.Value: accessLockingModel.AppearanceColor),
			new SqlParameter("@USER_NAME_INSERT",string.IsNullOrEmpty(accessLockingModel.UsernameInsert) ? (object)DBNull.Value: accessLockingModel.UsernameInsert),
			new SqlParameter("@USER_NAME_UPDATE",string.IsNullOrEmpty(accessLockingModel.UsernameUpdate) ? (object)DBNull.Value: accessLockingModel.UsernameUpdate),
			new SqlParameter("@SERVER_INSERT_DATE",string.IsNullOrEmpty(accessLockingModel.ServerInsertDate) ? (object)DBNull.Value: accessLockingModel.ServerInsertDate),
			new SqlParameter("@SERVER_INSERT_TIME",string.IsNullOrEmpty(accessLockingModel.ServerInsertTime) ? (object)DBNull.Value: accessLockingModel.ServerInsertTime),
			new SqlParameter("@SERVER_INSERT_TIME_ZONE",string.IsNullOrEmpty(accessLockingModel.ServerInsertTimeZone) ? (object)DBNull.Value: accessLockingModel.ServerInsertTimeZone),
			new SqlParameter("@SERVER_UPDATE_DATE",string.IsNullOrEmpty(accessLockingModel.ServerUpdateDate) ? (object)DBNull.Value: accessLockingModel.ServerUpdateDate),
			new SqlParameter("@SERVER_UPDATE_TIME",string.IsNullOrEmpty(accessLockingModel.ServerUpdateTime) ? (object)DBNull.Value: accessLockingModel.ServerUpdateTime),
			new SqlParameter("@SERVER_UPDATE_TIME_ZONE",string.IsNullOrEmpty(accessLockingModel.ServerUpdateTimeZone) ? (object)DBNull.Value: accessLockingModel.ServerUpdateTimeZone),
			new SqlParameter("@CLIENT_INSERT_DATE",string.IsNullOrEmpty(accessLockingModel.ClientInsertDate) ? (object)DBNull.Value: accessLockingModel.ClientInsertDate),
			new SqlParameter("@CLIENT_INSERT_TIME",string.IsNullOrEmpty(accessLockingModel.ClientInsertTime) ? (object)DBNull.Value: accessLockingModel.ClientInsertTime),
			new SqlParameter("@CLIENT_INSERT_TIME_ZONE",string.IsNullOrEmpty(accessLockingModel.ClientInsertTimeZone) ? (object)DBNull.Value: accessLockingModel.ClientInsertTimeZone),
			new SqlParameter("@CLIENT_UPDATE_DATE",string.IsNullOrEmpty(accessLockingModel.ClientUpdateDate) ? (object)DBNull.Value: accessLockingModel.ClientUpdateDate),
			new SqlParameter("@CLIENT_UPDATE_TIME",string.IsNullOrEmpty(accessLockingModel.ClientUpdateTime) ? (object)DBNull.Value: accessLockingModel.ClientUpdateTime),
			new SqlParameter("@CLIENT_UPDATE_TIME_ZONE",string.IsNullOrEmpty(accessLockingModel.ClientUpdateTimeZone) ? (object)DBNull.Value: accessLockingModel.ClientUpdateTimeZone),
			new SqlParameter("@BSTATUS",string.IsNullOrEmpty(accessLockingModel.BStatus) ? (object)DBNull.Value: accessLockingModel.BStatus),
			new SqlParameter("@BDELETE",string.IsNullOrEmpty(accessLockingModel.BDelete) ? (object)DBNull.Value: accessLockingModel.BDelete),
			new SqlParameter("@BMAP",string.IsNullOrEmpty(accessLockingModel.BMap) ? (object)DBNull.Value: accessLockingModel.BMap),
			new SqlParameter("@REMARKS_1",string.IsNullOrEmpty(accessLockingModel.Remark1) ? (object)DBNull.Value: accessLockingModel.Remark1),
			new SqlParameter("@REMARKS_2",string.IsNullOrEmpty(accessLockingModel.Remark2) ? (object)DBNull.Value: accessLockingModel.Remark2),
			new SqlParameter("@REMARKS_3",string.IsNullOrEmpty(accessLockingModel.Remark3) ? (object)DBNull.Value: accessLockingModel.Remark3),
			new SqlParameter("@REMARKS_4",string.IsNullOrEmpty(accessLockingModel.Remark4) ? (object)DBNull.Value: accessLockingModel.Remark4),
			new SqlParameter("@FLEX_1",string.IsNullOrEmpty(accessLockingModel.Flex1) ? (object)DBNull.Value: accessLockingModel.Flex1),
			new SqlParameter("@FLEX_2",string.IsNullOrEmpty(accessLockingModel.Flex2) ? (object)DBNull.Value: accessLockingModel.Flex2),
			new SqlParameter("@FLEX_3",string.IsNullOrEmpty(accessLockingModel.Flex3) ? (object)DBNull.Value: accessLockingModel.Flex3),
			new SqlParameter("@FLEX_4",string.IsNullOrEmpty(accessLockingModel.Flex4) ? (object)DBNull.Value: accessLockingModel.Flex4),
			new SqlParameter("@FLEX_5",string.IsNullOrEmpty(accessLockingModel.Flex5) ? (object)DBNull.Value: accessLockingModel.Flex5),
			new SqlParameter("@FLEX_6",string.IsNullOrEmpty(accessLockingModel.Flex6) ? (object)DBNull.Value: accessLockingModel.Flex6),
			new SqlParameter("@FLEX_7",string.IsNullOrEmpty(accessLockingModel.Flex7) ? (object)DBNull.Value: accessLockingModel.Flex7),
			new SqlParameter("@FLEX_8",string.IsNullOrEmpty(accessLockingModel.Flex8) ? (object)DBNull.Value: accessLockingModel.Flex8),
			new SqlParameter("@FLEX_9",string.IsNullOrEmpty(accessLockingModel.Flex9) ? (object)DBNull.Value: accessLockingModel.Flex8),
			new SqlParameter("@FLEX_10",string.IsNullOrEmpty(accessLockingModel.Flex10) ? (object)DBNull.Value: accessLockingModel.Flex10),
			new SqlParameter("@FLEX_11",string.IsNullOrEmpty(accessLockingModel.Flex11) ? (object)DBNull.Value: accessLockingModel.Flex11),
			new SqlParameter("@FLEX_12",string.IsNullOrEmpty(accessLockingModel.Flex12) ? (object)DBNull.Value: accessLockingModel.Flex12),
			new SqlParameter("@FLEX_13",string.IsNullOrEmpty(accessLockingModel.Flex13) ? (object)DBNull.Value: accessLockingModel.Flex13),
			new SqlParameter("@FLEX_14",string.IsNullOrEmpty(accessLockingModel.Flex14) ? (object)DBNull.Value: accessLockingModel.Flex14),
			new SqlParameter("@FLEX_15",string.IsNullOrEmpty(accessLockingModel.Flex15) ? (object)DBNull.Value: accessLockingModel.Flex15),
			new SqlParameter("@FLEX_16",string.IsNullOrEmpty(accessLockingModel.Flex16) ? (object)DBNull.Value: accessLockingModel.Flex16),
			new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
		};

			string spQuery = StoreProcedureConstants.Sp_SaveALSetup + " @USER_ID, @WORKSPACE_ID, @CLIENT_ID," +
			" @USER_LOCKED, @FAILED_ATTEMPTS, @FAILED_TIMEINTERVAL, @UNLOCK_OPTIONS," +
			" @UNLOCK_TIMEINTERVAL, @UNLOCK_ADMINISTRATOR, @UNLOCK_SUPERVISIOR, @APPEARANCE_LOGO, @APPEARANCE_COLOR, @USER_NAME_INSERT," +
			" @USER_NAME_UPDATE, @SERVER_INSERT_DATE, @SERVER_INSERT_TIME, @SERVER_INSERT_TIME_ZONE, @SERVER_UPDATE_DATE," +
			" @SERVER_UPDATE_TIME, @SERVER_UPDATE_TIME_ZONE, @CLIENT_INSERT_DATE, @CLIENT_INSERT_TIME, @CLIENT_INSERT_TIME_ZONE," +
			" @CLIENT_UPDATE_DATE, @CLIENT_UPDATE_TIME, @CLIENT_UPDATE_TIME_ZONE, @BSTATUS,	@BDELETE, @BMAP," +
			" @REMARKS_1, @REMARKS_2, @REMARKS_3, @REMARKS_4, @FLEX_1, @FLEX_2, @FLEX_3,@FLEX_4,@FLEX_5, @FLEX_6," +
			" @FLEX_7, @FLEX_8, @FLEX_9, @FLEX_10, @FLEX_11, @FLEX_12, @FLEX_13, @FLEX_14, @FLEX_15, @FLEX_16 ," +
			" @V_MESSAGE OUTPUT";
			return _accessLockingSetupRepository.ExecuteCommand(spQuery, parameters);
		}
    }
}
