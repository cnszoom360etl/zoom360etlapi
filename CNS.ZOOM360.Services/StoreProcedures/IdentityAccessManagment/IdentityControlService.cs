﻿using CNS.ZOOM360.Entities.StoreProcedures.IdentityAccessManagment;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.IdentityAccessManagment;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.IdentityAccessManagment
{
  public  class IdentityControlService: IidentityControlService
    {
        private readonly IRepositoryBase<IdentityControlModel> _identityControlSetupRepository;
        public IdentityControlService(IRepositoryBase<IdentityControlModel> identityControlSetupRepository)
        {
            _identityControlSetupRepository = identityControlSetupRepository;
        }
		public async Task<string> SaveIdentityControlSetup(IdentityControlModel identityControlModel)
		{
			object[] parameters = {
			new SqlParameter("@USER_ID", string.IsNullOrEmpty(identityControlModel.UserId) ? (object)DBNull.Value: identityControlModel.UserId),
			new SqlParameter("@WORKSPACE_ID",string.IsNullOrEmpty((identityControlModel.WorkspaceId).ToString()) ? (object)DBNull.Value: identityControlModel.WorkspaceId),
			new SqlParameter("@CLIENT_ID", string.IsNullOrEmpty((identityControlModel.ClientId).ToString()) ? (object)DBNull.Value: identityControlModel.ClientId),
			new SqlParameter("@DEFINED_FORMATS", string.IsNullOrEmpty(identityControlModel.DefinedFormats) ? (object)DBNull.Value: identityControlModel.DefinedFormats),
			new SqlParameter("@AVAILABLE_FORMATS",string.IsNullOrEmpty(identityControlModel.AvailableFormats) ? (object)DBNull.Value: identityControlModel.AvailableFormats),
			new SqlParameter("@MANDATORY_FIELD",string.IsNullOrEmpty(identityControlModel.MandtoryField) ? (object)DBNull.Value: identityControlModel.MandtoryField),
			new SqlParameter("@UNIQUE_MANDATORY_FIELD", identityControlModel.UniqueMandtoryField),
			new SqlParameter("@LOGINID_ACTIVATION",string.IsNullOrEmpty(identityControlModel.LoginActivation) ? (object)DBNull.Value: identityControlModel.LoginActivation),
			new SqlParameter("@APPROVAL_SUPERVISOR",identityControlModel.ApprovalSuperVisor),
			new SqlParameter("@APPROVAL_EMAIL",identityControlModel.ApprovalEmail),
			new SqlParameter("@APPROVAL_QRCODE",identityControlModel.ApprovalQRCode),
			new SqlParameter("@LOGINID_CASESENSITIVE",string.IsNullOrEmpty(identityControlModel.LoginCaseSensitive) ? (object)DBNull.Value: identityControlModel.LoginCaseSensitive),
			new SqlParameter("@LOGINID_AUTHENTICATION",identityControlModel.LoginAuthentication),
			new SqlParameter("@SSO_AUTHENTICATION",identityControlModel.SSOAuthentication),
			new SqlParameter("@SSL_CERTIFICATE",string.IsNullOrEmpty(identityControlModel.SSLCertificate) ? (object)DBNull.Value: identityControlModel.SSLCertificate),
			new SqlParameter("@APPEARANCE_LOGO",string.IsNullOrEmpty(identityControlModel.AppearanceLogo) ? (object)DBNull.Value: identityControlModel.AppearanceLogo),
			new SqlParameter("@APPEARANCE_COLOR",string.IsNullOrEmpty(identityControlModel.AppearanceColor) ? (object)DBNull.Value: identityControlModel.AppearanceColor),
			new SqlParameter("@USER_NAME_INSERT",string.IsNullOrEmpty(identityControlModel.UsernameInsert) ? (object)DBNull.Value: identityControlModel.UsernameInsert),
			new SqlParameter("@USER_NAME_UPDATE",string.IsNullOrEmpty(identityControlModel.UsernameUpdate) ? (object)DBNull.Value: identityControlModel.UsernameUpdate),
			new SqlParameter("@SERVER_INSERT_DATE",string.IsNullOrEmpty(identityControlModel.ServerInsertDate) ? (object)DBNull.Value: identityControlModel.ServerInsertDate),
			new SqlParameter("@SERVER_INSERT_TIME",string.IsNullOrEmpty(identityControlModel.ServerInsertTime) ? (object)DBNull.Value: identityControlModel.ServerInsertTime),
			new SqlParameter("@SERVER_INSERT_TIME_ZONE",string.IsNullOrEmpty(identityControlModel.ServerInsertTimeZone) ? (object)DBNull.Value: identityControlModel.ServerInsertTimeZone),
			new SqlParameter("@SERVER_UPDATE_DATE",string.IsNullOrEmpty(identityControlModel.ServerUpdateDate) ? (object)DBNull.Value: identityControlModel.ServerUpdateDate),
			new SqlParameter("@SERVER_UPDATE_TIME",string.IsNullOrEmpty(identityControlModel.ServerUpdateTime) ? (object)DBNull.Value: identityControlModel.ServerUpdateTime),
			new SqlParameter("@SERVER_UPDATE_TIME_ZONE",string.IsNullOrEmpty(identityControlModel.ServerUpdateTimeZone) ? (object)DBNull.Value: identityControlModel.ServerUpdateTimeZone),
			new SqlParameter("@CLIENT_INSERT_DATE",string.IsNullOrEmpty(identityControlModel.ClientInsertDate) ? (object)DBNull.Value: identityControlModel.ClientInsertDate),
			new SqlParameter("@CLIENT_INSERT_TIME",string.IsNullOrEmpty(identityControlModel.ClientInsertTime) ? (object)DBNull.Value: identityControlModel.ClientInsertTime),
			new SqlParameter("@CLIENT_INSERT_TIME_ZONE",string.IsNullOrEmpty(identityControlModel.ClientInsertTimeZone) ? (object)DBNull.Value: identityControlModel.ClientInsertTimeZone),
			new SqlParameter("@CLIENT_UPDATE_DATE",string.IsNullOrEmpty(identityControlModel.ClientUpdateDate) ? (object)DBNull.Value: identityControlModel.ClientUpdateDate),
			new SqlParameter("@CLIENT_UPDATE_TIME",string.IsNullOrEmpty(identityControlModel.ClientUpdateTime) ? (object)DBNull.Value: identityControlModel.ClientUpdateTime),
			new SqlParameter("@CLIENT_UPDATE_TIME_ZONE",string.IsNullOrEmpty(identityControlModel.ClientUpdateTimeZone) ? (object)DBNull.Value: identityControlModel.ClientUpdateTimeZone),
			new SqlParameter("@BSTATUS",string.IsNullOrEmpty(identityControlModel.BStatus) ? (object)DBNull.Value: identityControlModel.BStatus),
			new SqlParameter("@BDELETE",string.IsNullOrEmpty(identityControlModel.BDelete) ? (object)DBNull.Value: identityControlModel.BDelete),
			new SqlParameter("@BMAP",string.IsNullOrEmpty(identityControlModel.BMap) ? (object)DBNull.Value: identityControlModel.BMap),
			new SqlParameter("@REMARKS_1",string.IsNullOrEmpty(identityControlModel.Remark1) ? (object)DBNull.Value: identityControlModel.Remark1),
			new SqlParameter("@REMARKS_2",string.IsNullOrEmpty(identityControlModel.Remark2) ? (object)DBNull.Value: identityControlModel.Remark2),
			new SqlParameter("@REMARKS_3",string.IsNullOrEmpty(identityControlModel.Remark3) ? (object)DBNull.Value: identityControlModel.Remark3),
			new SqlParameter("@REMARKS_4",string.IsNullOrEmpty(identityControlModel.Remark4) ? (object)DBNull.Value: identityControlModel.Remark4),
			new SqlParameter("@FLEX_1",string.IsNullOrEmpty(identityControlModel.Flex1) ? (object)DBNull.Value: identityControlModel.Flex1),
			new SqlParameter("@FLEX_2",string.IsNullOrEmpty(identityControlModel.Flex2) ? (object)DBNull.Value: identityControlModel.Flex2),
			new SqlParameter("@FLEX_3",string.IsNullOrEmpty(identityControlModel.Flex3) ? (object)DBNull.Value: identityControlModel.Flex3),
			new SqlParameter("@FLEX_4",string.IsNullOrEmpty(identityControlModel.Flex4) ? (object)DBNull.Value: identityControlModel.Flex4),
			new SqlParameter("@FLEX_5",string.IsNullOrEmpty(identityControlModel.Flex5) ? (object)DBNull.Value: identityControlModel.Flex5),
			new SqlParameter("@FLEX_6",string.IsNullOrEmpty(identityControlModel.Flex6) ? (object)DBNull.Value: identityControlModel.Flex6),
			new SqlParameter("@FLEX_7",string.IsNullOrEmpty(identityControlModel.Flex7) ? (object)DBNull.Value: identityControlModel.Flex7),
			new SqlParameter("@FLEX_8",string.IsNullOrEmpty(identityControlModel.Flex8) ? (object)DBNull.Value: identityControlModel.Flex8),
			new SqlParameter("@FLEX_9",string.IsNullOrEmpty(identityControlModel.Flex9) ? (object)DBNull.Value: identityControlModel.Flex8),
			new SqlParameter("@FLEX_10",string.IsNullOrEmpty(identityControlModel.Flex10) ? (object)DBNull.Value: identityControlModel.Flex10),
			new SqlParameter("@FLEX_11",string.IsNullOrEmpty(identityControlModel.Flex11) ? (object)DBNull.Value: identityControlModel.Flex11),
			new SqlParameter("@FLEX_12",string.IsNullOrEmpty(identityControlModel.Flex12) ? (object)DBNull.Value: identityControlModel.Flex12),
			new SqlParameter("@FLEX_13",string.IsNullOrEmpty(identityControlModel.Flex13) ? (object)DBNull.Value: identityControlModel.Flex13),
			new SqlParameter("@FLEX_14",string.IsNullOrEmpty(identityControlModel.Flex14) ? (object)DBNull.Value: identityControlModel.Flex14),
			new SqlParameter("@FLEX_15",string.IsNullOrEmpty(identityControlModel.Flex15) ? (object)DBNull.Value: identityControlModel.Flex15),
			new SqlParameter("@FLEX_16",string.IsNullOrEmpty(identityControlModel.Flex16) ? (object)DBNull.Value: identityControlModel.Flex16),
			new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
		};

		string spQuery = StoreProcedureConstants.Sp_SaveICSetup + " @USER_ID, @WORKSPACE_ID, @CLIENT_ID," +
		" @DEFINED_FORMATS, @AVAILABLE_FORMATS, @MANDATORY_FIELD, @UNIQUE_MANDATORY_FIELD," +
		" @LOGINID_ACTIVATION, @APPROVAL_SUPERVISOR, @APPROVAL_EMAIL, APPROVAL_QRCODE, @LOGINID_CASESENSITIVE," +
		" @LOGINID_AUTHENTICATION, @SSO_AUTHENTICATION, @SSL_CERTIFICATE,@APPEARANCE_LOGO, @APPEARANCE_COLOR, @USER_NAME_INSERT," +
		" @USER_NAME_UPDATE, @SERVER_INSERT_DATE, @SERVER_INSERT_TIME, @SERVER_INSERT_TIME_ZONE, @SERVER_UPDATE_DATE," +
		" @SERVER_UPDATE_TIME, @SERVER_UPDATE_TIME_ZONE, @CLIENT_INSERT_DATE, @CLIENT_INSERT_TIME, @CLIENT_INSERT_TIME_ZONE," +
		" @CLIENT_UPDATE_DATE, @CLIENT_UPDATE_TIME, @CLIENT_UPDATE_TIME_ZONE, @BSTATUS,	@BDELETE, @BMAP,"+
		" @REMARKS_1, @REMARKS_2, @REMARKS_3, @REMARKS_4, @FLEX_1, @FLEX_2, @FLEX_3,@FLEX_4,@FLEX_5, @FLEX_6,"+
		" @FLEX_7, @FLEX_8, @FLEX_9, @FLEX_10, @FLEX_11, @FLEX_12, @FLEX_13, @FLEX_14, @FLEX_15, @FLEX_16 ,"+
		" @V_MESSAGE OUTPUT";
         return _identityControlSetupRepository.ExecuteCommand(spQuery, parameters);
        }
    }
}
