﻿using CNS.ZOOM360.Entities.StoreProcedures.DataGovernance;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.DataGovernance;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.DataGovernance
{
    public class DataGovernanceService: IDataGovernanceService
    {

        private readonly IRepositoryBase<DataGovernanceModel> _dataGovernanceRepository;
        public DataGovernanceService(IRepositoryBase<DataGovernanceModel> dataGovernanceRepository) {
            _dataGovernanceRepository = dataGovernanceRepository;
        }

        public async Task<string> SaveDataGovernance(DataGovernanceModel dataGovernanceModel)
        {
            object[] parameters = {

            new SqlParameter("@USER_ID", dataGovernanceModel.userId),
            new SqlParameter("@WORKSPACE_ID", dataGovernanceModel.workSpaceId),
            new SqlParameter("@CLIENT_ID", dataGovernanceModel.CLIENT_ID),
            new SqlParameter("@SCHEMA_MODE", dataGovernanceModel.schemaMode),
            new SqlParameter("@CHILD_WORKSPACE_INHERITANCE", dataGovernanceModel.childWorkspaceInheritance),
            new SqlParameter("@WORKSPACE_SHAREDATA", dataGovernanceModel.workspaceShareData),
            new SqlParameter("@OUTOFFAPP_WEBSHAREDATA", dataGovernanceModel.outOffAppWebShareData),
            new SqlParameter("@OUTOFFAPP_APISHAREDATA", dataGovernanceModel.outOffApiShareData),
            new SqlParameter("@RAW_DATA_STAGGING", dataGovernanceModel.rawDataStagging),
            new SqlParameter("@STAGGING_STORAGE_LOCATIONTYPE", dataGovernanceModel.staggingStorageLocationType),
            new SqlParameter("@STAGGING_RETENTION_DAYS", dataGovernanceModel.staggingRetentionDays),
            new SqlParameter("@ACTIVE_SOURCE_LOCATION", dataGovernanceModel.activeSourceLocation),
            new SqlParameter("@DESTINATION_WORKSPACE", dataGovernanceModel.destinationWorkspace),
            new SqlParameter("@ACTIVE_DESTINATION_LOCATION", dataGovernanceModel.activeDestinationLocation),
            new SqlParameter("@PASSIVE_DESTINATION_LOCATION", dataGovernanceModel.passiveDestinationLocation),
            new SqlParameter("@DATA_COLLECTING_TYPE", dataGovernanceModel.dataCollectionType),
            new SqlParameter("@OVERRIDE_DATA_SNAPSHOT", dataGovernanceModel.overrideDataSnapshot),
            new SqlParameter("@DATA_STORAGE", dataGovernanceModel.dataStorage),
            new SqlParameter("@DATA_DESTINATION", dataGovernanceModel.dataDestination),
            new SqlParameter("@OVERRIDE_DATA_STORAGE", dataGovernanceModel.overrideDataStorage),
            new SqlParameter("@DESTINATION_RETENTION_DAYS", dataGovernanceModel.destinationRetentionDays),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
            
        };

            string spQuery = StoreProcedureConstants.Sp_SaveDataGovernance + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                " @SCHEMA_MODE, @CHILD_WORKSPACE_INHERITANCE,@WORKSPACE_SHAREDATA, @OUTOFFAPP_WEBSHAREDATA," +
                " @OUTOFFAPP_APISHAREDATA, @RAW_DATA_STAGGING,@STAGGING_STORAGE_LOCATIONTYPE, " +
                "@STAGGING_RETENTION_DAYS," +
                " @ACTIVE_SOURCE_LOCATION, @DESTINATION_WORKSPACE,@ACTIVE_DESTINATION_LOCATION, @PASSIVE_DESTINATION_LOCATION," +
                " @DATA_COLLECTING_TYPE, @OVERRIDE_DATA_SNAPSHOT,@DATA_STORAGE, @DATA_DESTINATION," +
                " @OVERRIDE_DATA_STORAGE, @DESTINATION_RETENTION_DAYS," +
                "  @V_MESSAGE OUTPUT";

            return _dataGovernanceRepository.ExecuteCommand(spQuery, parameters);


        }
    }
}
