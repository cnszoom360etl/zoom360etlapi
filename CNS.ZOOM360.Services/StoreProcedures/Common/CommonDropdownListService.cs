﻿using CNS.ZOOM360.Entities.StoreProcedures.Common;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.Common;
using CNS.ZOOM360.Shared.StoreProcedures.Common.Dto;
using CNS.ZOOM360.Shared.StoreProcedures.Workspace.Dto;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.Common
{
    public class CommonDropdownListService: ICommonDropdownListService
    {
        private readonly IRepositoryBase<DropdownList> _dropdownListRepository;
        private readonly IRepositoryBase<UAMDropdownListModel> _UAMdropdownListRepository;
        private readonly IRepositoryBase<TreeDropDownParentModel> _treedropdownListRepository;
        private readonly IRepositoryBase<TreeDropDownChildModel> _treedropdownChildRepository;
        private readonly IRepositoryBase<FileModelListcs> _FileModelListcs;
        private readonly IRepositoryBase<DropDownWithCategoryModel> _UAMdropdownListCategoryRepository;
        public CommonDropdownListService(IRepositoryBase<DropdownList> dropdownListRepository,
            IRepositoryBase<UAMDropdownListModel> UAMdropdownListRepository,
             IRepositoryBase<TreeDropDownParentModel> treedropdownListRepository,
             IRepositoryBase<TreeDropDownChildModel> treedropdownChildRepository,
             IRepositoryBase<DropDownWithCategoryModel> UAMdropdownListCategoryRepository,
             IRepositoryBase<FileModelListcs> FileModelListcs) {
            _dropdownListRepository = dropdownListRepository;
            _UAMdropdownListRepository = UAMdropdownListRepository;
            _treedropdownListRepository = treedropdownListRepository;
            _treedropdownChildRepository = treedropdownChildRepository;
            _UAMdropdownListCategoryRepository = UAMdropdownListCategoryRepository;
            _FileModelListcs = FileModelListcs;
        }

        public async Task<List<DropdownList>> GetDropDownList(string userId, string dropDownName)
        {
            object[] parameters = {
            new SqlParameter("@USER_ID",userId),
            new SqlParameter("@DROPDOWN_TYPE",dropDownName),
            new SqlParameter("@V_MESSAGE",SqlDbType.NVarChar,4000){Direction = ParameterDirection.Output }
            };
            string spQuery = StoreProcedureConstants.Sp_GetDropdownList + " @USER_ID, @DROPDOWN_TYPE, @V_MESSAGE OUTPUT";
            List<DropdownList> dropdownLists = _dropdownListRepository.ExecuteQuery(spQuery, parameters).ToList();
            return dropdownLists;
        }
        public async Task<List<UAMDropdownListModel>> GetUAMDropDown(string userId, string dropDownName, string subUserID)
        {
            object[] parameters = {
            new SqlParameter("@USER_ID",userId),
            new SqlParameter("@SUB_USER_ID",subUserID),
            new SqlParameter("@DROPDOWN_TYPE",dropDownName),
            new SqlParameter("@V_MESSAGE",SqlDbType.NVarChar,4000){Direction = ParameterDirection.Output }
            };

            string spQuery = StoreProcedureConstants.Sp_GetUAMDropdown + " @USER_ID, @SUB_USER_ID, @DROPDOWN_TYPE, @V_MESSAGE OUTPUT";
            List<UAMDropdownListModel> dropdownLists = _UAMdropdownListRepository.ExecuteQuery(spQuery, parameters).ToList();
            return dropdownLists;
        }
      public List<TreeDropDownParentModel> GetTreeDropDownParent(TreeDropDownInputModel treeDropDownInputModel)
        {
            object[] parameters = {
            new SqlParameter("@USER_ID", treeDropDownInputModel.UserId),
            new SqlParameter("@SUB_USER_ID", treeDropDownInputModel.SubUserId),
            new SqlParameter("@CLIENT_ID", treeDropDownInputModel.ClientId),
             new SqlParameter("@WORKSPACE_ID", treeDropDownInputModel.WorkspaceId),
            new SqlParameter("@DROPDOWN_TYPE", treeDropDownInputModel.DropDownType),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output}
        };
            string spQuery = StoreProcedureConstants.Sp_GetTreeDropDownParentList + " @USER_ID,@SUB_USER_ID,@CLIENT_ID,@WORKSPACE_ID,@DROPDOWN_TYPE " +
                ", @V_MESSAGE OUTPUT";
            List<TreeDropDownParentModel> subMenuSectionlist = _treedropdownListRepository.ExecuteQuery(spQuery, parameters).ToList();
            return subMenuSectionlist;

        }
        public async Task<List<TreeDropDownParentModel>> GetTreeDropDownChild(TreeDropDownInputModel treeDropDownInputModel)
        {
            List<TreeDropDownParentModel> submenusectionList;
            //List<TreeDropDownChildModel> submenusectionList;

            submenusectionList = GetTreeDropDownParent(treeDropDownInputModel);

            int i = 0;
            foreach (var item in submenusectionList)
            {
                item.TreeDropDownChildItems = new List<TreeDropDownChildModel>();
                object[] parameters = {
                    new SqlParameter("@USER_ID", treeDropDownInputModel.UserId),
                    new SqlParameter("@SUB_USER_ID", treeDropDownInputModel.SubUserId),
                    new SqlParameter("@CLIENT_ID", treeDropDownInputModel.ClientId),
                    new SqlParameter("@WORKSPACE_ID", treeDropDownInputModel.WorkspaceId),
                    new SqlParameter("@DROPDOWN_TYPE", treeDropDownInputModel.DropDownType),
                     new SqlParameter("@MAIN_MENU_ID", item.DropdownValue),
                    new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output}
             };
                string spQuery = StoreProcedureConstants.Sp_GetTreeDropDownChildList + " @USER_ID,@SUB_USER_ID,@CLIENT_ID,@WORKSPACE_ID,@DROPDOWN_TYPE,@MAIN_MENU_ID " +
                 ", @V_MESSAGE OUTPUT";

                item.TreeDropDownChildItems = _treedropdownChildRepository.ExecuteQuery(spQuery, parameters).ToList();
                i++;
            }
            return submenusectionList;
        }


        public async Task< List<FileModelListcs>> GetModelTypeList(FileModelListcs input)
        {
            object[] parameters = {
            new SqlParameter("@USER_ID",input.userid),
            new SqlParameter("@WORKSPACE_ID",input.workspaceid),
            new SqlParameter("@CLIENT_ID",input.clientid),
            new SqlParameter("@MODEL_NAME",input.modeltype),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output}
        };
            string spQuery = StoreProcedureConstants.Sp_GETFILEMODELLIST + " @USER_ID,@WORKSPACE_ID,@CLIENT_ID,@MODEL_NAME" +
                ", @V_MESSAGE OUTPUT";
            List<FileModelListcs> ModelNameList = _FileModelListcs.ExecuteQuery(spQuery, parameters).ToList();
            return ModelNameList;

        }
        public async Task<List<FileModelListcs>> GetFieldNameList(string userId, string dropdownName)
        {
            object[] parameters = {
            new SqlParameter("@USER_ID",userId),
            new SqlParameter("@DROPDOWN_TYPE ",dropdownName),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output}
        };
            string spQuery = StoreProcedureConstants.Sp_GETFIELDLISTLIST + " @USER_ID,@DROPDOWN_TYPE " +
                ", @V_MESSAGE OUTPUT";
            List<FileModelListcs> ModelNameList = _FileModelListcs.ExecuteQuery(spQuery, parameters).ToList();
            return ModelNameList;

        }

        public async Task<List<DropDownWithCategoryModel>> GetDropDownListWithCategory(string userId, string dropDownName)
        {
            object[] parameters = {
            new SqlParameter("@USER_ID",userId),
            new SqlParameter("@DROPDOWN_TYPE",dropDownName),
            new SqlParameter("@V_MESSAGE",SqlDbType.NVarChar,4000){Direction = ParameterDirection.Output }
            };
            string spQuery = StoreProcedureConstants.Sp_GetFunctionList + " @USER_ID, @DROPDOWN_TYPE, @V_MESSAGE OUTPUT";
            List<DropDownWithCategoryModel> dropdownLists = _UAMdropdownListCategoryRepository.ExecuteQuery(spQuery, parameters).ToList();
            return dropdownLists;
        }
    }
}
