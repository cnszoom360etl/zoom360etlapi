﻿using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.SourceDescriptionAndConfiguration;
using CNS.ZOOM360.Entities.StoreProcedures.SourceDescriptionAndConfiguration;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using CNS.ZOOM360.Shared.Const;
using System.Data;
using CNS.ZOOM360.Entities.StoreProcedures.Connectors.Databases.SQL;
using CNS.ZOOM360.Entities.StoreProcedures.sourceDescriptionAndConfiguration;
using System.Linq;

namespace CNS.ZOOM360.Services.StoreProcedures.SourceConnectorDescAndConfigure
{
    public class sourceDescriptionAndConfigurationService : ISourceDescriptionAndConfiguration
    {
        private readonly IRepositoryBase<sourceCommonModel> _sourcedescription;
        private readonly IRepositoryBase<SocialMediaBit> _AuthorizationStatus;
        
        public sourceDescriptionAndConfigurationService(IRepositoryBase<sourceCommonModel> sourceRepository, IRepositoryBase<SocialMediaBit> AuthorizationStatus)
        {
            _sourcedescription = sourceRepository;
            _AuthorizationStatus = AuthorizationStatus;

        }
        public async Task<string> SaveSourceDescriptionAndConfiguration(sourceCommonModel inpoutModel)
        {
            var Client_Insert_Date = System.DateTime.Now.ToShortDateString();
            var Client_Insert_Time = System.DateTime.Now.ToShortTimeString();
            var Client_Insert_TimeZone = "null";
            object[] parameters = {

            new SqlParameter("@USER_ID",inpoutModel.userId),
            new SqlParameter("@WORKSPACE_ID", inpoutModel.workspaceId),
            new SqlParameter("@CLIENT_ID", inpoutModel.clientId),
            new SqlParameter("@ACCOUNT_ID", inpoutModel.AccountId),
            new SqlParameter("@CONNECTOR_ID", inpoutModel.connectorId),
            new SqlParameter("@ACCOUNT_DISPLAY_NAME", inpoutModel.SourceInfoModel.accountName),
            new SqlParameter("@WORKSPACE_NAME", inpoutModel.SourceInfoModel.workspace),
            new SqlParameter("@ENABLED_STATUS", inpoutModel.SourceInfoModel.enableConnectoin),
            new SqlParameter("@VISIBILITY_STATUS", inpoutModel.SourceInfoModel.visibilitymode),
            new SqlParameter("@COMMENTS", inpoutModel.SourceInfoModel.commentsection),
            new SqlParameter("@SPECIAL_COMMENTS", inpoutModel.SourceInfoModel.specialcomments),
            new SqlParameter("@CLIENT_DATE", string.IsNullOrEmpty(Client_Insert_Date) ? (object)DBNull.Value: Client_Insert_Date),
            new SqlParameter("@CLIENT_TIME", string.IsNullOrEmpty(Client_Insert_Time) ? (object)DBNull.Value: Client_Insert_Time),
            new SqlParameter("@CLIENT_TIME_ZONE", string.IsNullOrEmpty(Client_Insert_TimeZone) ? (object)DBNull.Value: Client_Insert_TimeZone),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){Direction = ParameterDirection.Output }

        };

            string spQuery = StoreProcedureConstants.Sp_SaveSourceCounfiguration + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
                " @ACCOUNT_ID, @CONNECTOR_ID,@ACCOUNT_DISPLAY_NAME, @WORKSPACE_NAME," +
                " @ENABLED_STATUS, @VISIBILITY_STATUS,@COMMENTS, " +
                "@SPECIAL_COMMENTS,@CLIENT_DATE,@CLIENT_TIME,@CLIENT_TIME_ZONE," +
                "  @V_MESSAGE OUTPUT";

            return _sourcedescription.ExecuteCommand(spQuery, parameters);


        }


        public async Task<string> SaveDBCredentials(SourceAccountConnectionModel inpoutModel)
        {
            var Client_Insert_Date = System.DateTime.Now.ToShortDateString();
            var Client_Insert_Time = System.DateTime.Now.ToShortTimeString();
            var Client_Insert_TimeZone = "null";
            var token_expire_time = "Null";
            object[] parameters = {

            new SqlParameter("@USER_ID",inpoutModel.UserId),
            new SqlParameter("@WORKSPACE_ID", inpoutModel.WorkspaceId),
            new SqlParameter("@CLIENT_ID",  inpoutModel.ClientId),
            new SqlParameter("@CONNECTOR_ID",inpoutModel.Connector_ID),
            new SqlParameter("@ACCOUNT_ID", inpoutModel.Account_Id),
            new SqlParameter("@FIELD_NAME", inpoutModel.HostName),
            new SqlParameter("@FIELD_VALUE",inpoutModel.FieldValue==null ? (object)DBNull.Value : inpoutModel.FieldValue),
            new SqlParameter("@CLIENT_DATE", string.IsNullOrEmpty(Client_Insert_Date) ? (object)DBNull.Value: Client_Insert_Date),
            new SqlParameter("@CLIENT_TIME", string.IsNullOrEmpty(Client_Insert_Time) ? (object)DBNull.Value: Client_Insert_Time),
            new SqlParameter("@CLIENT_TIME_ZONE", string.IsNullOrEmpty(Client_Insert_TimeZone) ? (object)DBNull.Value: Client_Insert_TimeZone),
            new SqlParameter("@CONNECTIVITY_STATUS",inpoutModel.connectivitystatus),
            new SqlParameter("@TOKEN_EXPIRY_TIME", string.IsNullOrEmpty(inpoutModel.tokenExpireTime) ? (object)DBNull.Value: inpoutModel.tokenExpireTime),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){Direction = ParameterDirection.Output }
           };

            string spQuery = StoreProcedureConstants.Sp_SavedbCredential + "  @USER_ID,@WORKSPACE_ID,@CLIENT_ID,  @CONNECTOR_ID," +
                " @ACCOUNT_ID, @FIELD_NAME, @FIELD_VALUE, @CLIENT_DATE, @CLIENT_TIME, @CLIENT_TIME_ZONE, @CONNECTIVITY_STATUS , @TOKEN_EXPIRY_TIME , " +
                 " @V_MESSAGE OUTPUT";

            return _sourcedescription.ExecuteCommand(spQuery, parameters);


        }
        public async Task<string> SaveTokenInfo(SourceAccountConnectionModel inpoutModel)
        {
            var Client_Insert_Date = System.DateTime.Now.ToShortDateString();
            var Client_Insert_Time = System.DateTime.Now.ToShortTimeString();
            DateTime date = new DateTime();
            //string d = date.AddSeconds(inpoutModel.tokenExpireTime);
            date = DateTime.Now.Date.AddSeconds(Convert.ToDouble(inpoutModel.tokenExpireTime));
            string tokenexpiredateformate = String.Format("{0:yyyy-MM-dd}", date);
            var Client_Insert_TimeZone = "null";

            object[] parameters = {

            new SqlParameter("@USER_ID",inpoutModel.UserId),
            new SqlParameter("@WORKSPACE_ID", inpoutModel.WorkspaceId),
            new SqlParameter("@CLIENT_ID",  inpoutModel.ClientId),
            new SqlParameter("@CONNECTOR_ID",inpoutModel.Connector_ID),
            new SqlParameter("@ACCOUNT_ID", inpoutModel.Account_Id),
            new SqlParameter("@FIELD_NAME", inpoutModel.HostName),
            new SqlParameter("@FIELD_VALUE",inpoutModel.FieldValue==null ? (object)DBNull.Value : inpoutModel.FieldValue),
            new SqlParameter("@CLIENT_DATE", string.IsNullOrEmpty(Client_Insert_Date) ? (object)DBNull.Value: Client_Insert_Date),
            new SqlParameter("@CLIENT_TIME", string.IsNullOrEmpty(Client_Insert_Time) ? (object)DBNull.Value: Client_Insert_Time),
            new SqlParameter("@CLIENT_TIME_ZONE", string.IsNullOrEmpty(Client_Insert_TimeZone) ? (object)DBNull.Value: Client_Insert_TimeZone),
            new SqlParameter("@CONNECTIVITY_STATUS",inpoutModel.connectivitystatus),
            new SqlParameter("@TOKEN_EXPIRY_TIME", string.IsNullOrEmpty(tokenexpiredateformate) ? (object)DBNull.Value: tokenexpiredateformate),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){Direction = ParameterDirection.Output }
           };

            string spQuery = StoreProcedureConstants.Sp_SavedbCredential + "  @USER_ID,@WORKSPACE_ID,@CLIENT_ID,  @CONNECTOR_ID," +
                " @ACCOUNT_ID, @FIELD_NAME, @FIELD_VALUE, @CLIENT_DATE, @CLIENT_TIME, @CLIENT_TIME_ZONE, @CONNECTIVITY_STATUS , @TOKEN_EXPIRY_TIME , " +
                 " @V_MESSAGE OUTPUT";

            return _sourcedescription.ExecuteCommand(spQuery, parameters);


        }
        public async Task<string> SaveFileInfo(SourceAccountConnectionModel inpoutModel)
        {
            var Client_Insert_Date = System.DateTime.Now.ToShortDateString();
            var Client_Insert_Time = System.DateTime.Now.ToShortTimeString();
            var Client_Insert_TimeZone = "null";
            object[] parameters = {

            new SqlParameter("@USER_ID",inpoutModel.UserId),
            new SqlParameter("@WORKSPACE_ID", inpoutModel.WorkspaceId),
             new SqlParameter("@CLIENT_ID", inpoutModel.ClientId),
            new SqlParameter("@CONNECTOR_ID", inpoutModel.Connector_ID),
           new SqlParameter("@ACCOUNT_ID", inpoutModel.Account_Id),
            new SqlParameter("@FIELD_NAME", inpoutModel.HostName),
            new SqlParameter("@FIELD_VALUE",inpoutModel.FieldValue),
             new SqlParameter("@CLIENT_DATE", string.IsNullOrEmpty(Client_Insert_Date) ? (object)DBNull.Value: Client_Insert_Date),
            new SqlParameter("@CLIENT_TIME", string.IsNullOrEmpty(Client_Insert_Time) ? (object)DBNull.Value: Client_Insert_Time),
            new SqlParameter("@CLIENT_TIME_ZONE", string.IsNullOrEmpty(Client_Insert_TimeZone) ? (object)DBNull.Value: Client_Insert_TimeZone),
           new SqlParameter("@CONNECTIVITY_STATUS", inpoutModel.connectivitystatus),
           new SqlParameter("@TOKEN_EXPIRY_TIME", string.IsNullOrEmpty(inpoutModel.tokenExpireTime) ? (object)DBNull.Value: inpoutModel.tokenExpireTime),
           new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){Direction = ParameterDirection.Output }
           };

            string spQuery = StoreProcedureConstants.Sp_Savefile + " @USER_ID,@WORKSPACE_ID,@CLIENT_ID, @CONNECTOR_ID," +
                "  @ACCOUNT_ID,@FIELD_NAME, @FIELD_VALUE,@CLIENT_DATE,@CLIENT_TIME,@CLIENT_TIME_ZONE,@CONNECTIVITY_STATUS , @TOKEN_EXPIRY_TIME ," +
                "  @V_MESSAGE OUTPUT";

            return _sourcedescription.ExecuteCommand(spQuery, parameters);


        }
        public  List<SocialMediaBit> SaveSocialMediaInfo(SocialMediaModel inpoutModel)
        {

            object[] parameters = {

            new SqlParameter("@USER_ID",inpoutModel.SourceCommonModel.userId),
            new SqlParameter("@WORKSPACE_ID", inpoutModel.SourceCommonModel.workspaceId),
            new SqlParameter("@CLIENT_ID", inpoutModel.SourceCommonModel.clientId),
            new SqlParameter("@ACCOUNT_ID", inpoutModel.SourceCommonModel.AccountId),
            new SqlParameter("@CONNECTOR_ID", inpoutModel.SourceCommonModel.connectorId),
            new SqlParameter("@ACCOUNT_AUTHORIZATION",inpoutModel.AccountAuthurization),
            new SqlParameter("@EMAIL_ID", inpoutModel.Email),

            new SqlParameter("@EMAIL_MESSAGE", SqlDbType.NVarChar, 4000){Direction = ParameterDirection.Output },
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){Direction = ParameterDirection.Output }
           };

            string spQuery = StoreProcedureConstants.Sp_SaveSocialMedia + " @USER_ID,@WORKSPACE_ID,  @CLIENT_ID, @ACCOUNT_ID, @CONNECTOR_ID," +
                " @ACCOUNT_AUTHORIZATION, @EMAIL_ID," +
                " @EMAIL_MESSAGE, @V_MESSAGE OUTPUT";
           // return _sourcedescription.ExecuteCommand(spQuery, parameters);
            List<SocialMediaBit> fu = _AuthorizationStatus.ExecuteQuery(spQuery, parameters).ToList();
            return fu;



        }
    }
}
