﻿using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Entities.StoreProcedures.Workspace;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Linq;
using CNS.ZOOM360.Entities.StoreProcedures;
using CNS.ZOOM360.Shared.StoreProcedures.Workspace;

namespace CNS.ZOOM360.Services.StoreProcedures.Workspace
{
    public class WorkspaceSetupService: IWorkspaceSetupService
    {
        private IRepositoryBase<WorkspaceSetup> _WorkspaceSetupRepository;
        private IRepositoryBase<WorkspaceSetupList> _WorkspaceSetupListRepository;
        
        public WorkspaceSetupService(IRepositoryBase<WorkspaceSetup> WorkspaceSpRepository, IRepositoryBase<WorkspaceSetupList> WorkspaceSetupListRepository)
        {
            _WorkspaceSetupRepository = WorkspaceSpRepository;
            _WorkspaceSetupListRepository = WorkspaceSetupListRepository;
            
        }
        public async Task<string> SaveWorkspaceSetup(WorkspaceSetup workspaceModel)
        {

            object[] parameters = {
            new SqlParameter("@USER_ID",workspaceModel.userId),
            new SqlParameter("@WORKSPACE_ID",workspaceModel.workSpaceId),
            new SqlParameter("@CLIENT_ID", workspaceModel.CLIENT_ID),
            new SqlParameter("@WORKSPACE_NAME",workspaceModel.WorkSpace_Name),
            new SqlParameter("@WORKSPACE_DISPLAY_NAME",workspaceModel.WorkSpaceDisplayName),
            new SqlParameter("@WORKSPACE_PARENT_NAME",workspaceModel.WorkSpaceParentName),
            new SqlParameter("@CHILD_WORKSPACE_RULE",workspaceModel.ChildWorkSpaceRule),
            new SqlParameter("@EXCLUDE_CHILD_WORKSPACE",workspaceModel.Exclude_Child_WorkSpace),
            new SqlParameter{ ParameterName = "@V_MESSAGE",
            Direction = ParameterDirection.Output,
            SqlDbType = SqlDbType.NVarChar,
            Size = 4000
            }
            };

            string spQuery = StoreProcedureConstants.Sp_SaveWorkspaceSetup + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID, @WORKSPACE_NAME, @WORKSPACE_DISPLAY_NAME,@WORKSPACE_PARENT_NAME, @CHILD_WORKSPACE_RULE, @EXCLUDE_CHILD_WORKSPACE, @V_MESSAGE OUTPUT";
            return _WorkspaceSetupRepository.ExecuteCommand(spQuery, parameters);

        }
        public async Task<WorkspaceSetup> GetWorkspaceSetupSettings(string userId, string client_Id, string workSpaceId)
        {
            object[] parameters = {
                new SqlParameter("@USER_ID", userId),
                new SqlParameter("@WORKSPACE_ID", workSpaceId),
                new SqlParameter("@CLIENT_ID", client_Id),
                new SqlParameter("@V_MESSAGE",SqlDbType.NVarChar,4000){Direction = ParameterDirection.Output}
            };

            string spQuery = StoreProcedureConstants.Sp_GetWorkspaceSetup + " @USER_ID, @WORKSPACE_ID, @CLIENT_ID,  @V_MESSAGE OUTPUT";

            WorkspaceSetup workspaceSetup = _WorkspaceSetupRepository.ExecuteQuery(spQuery, parameters).FirstOrDefault();

            

            return workspaceSetup;
        }

        public async Task<List<WorkspaceSetupList>> GetWorkspaceSetupList(string userId, string workSpaceId, string CLIENT_ID, string workSpaceName, string parentWorkSpace)
        {

            object[] parameters = {
                new SqlParameter("@USER_ID", userId),
                new SqlParameter("@CLIENT_ID", CLIENT_ID),
                new SqlParameter("@WORKSPACE_ID", workSpaceId),
                new SqlParameter("@WORKSPACE_NAME", workSpaceName),
                new SqlParameter("@PARENT_WORKSPACE", parentWorkSpace),
                new SqlParameter("@V_MESSAGE",SqlDbType.NVarChar,4000){Direction = ParameterDirection.Output}
            };

            string spQuery = StoreProcedureConstants.Sp_GetWorkspaceSetupList + " @USER_ID, @CLIENT_ID, @WORKSPACE_ID, @WORKSPACE_NAME, @PARENT_WORKSPACE,   @V_MESSAGE OUTPUT";
            List<WorkspaceSetupList> workspacelist = _WorkspaceSetupListRepository.ExecuteQuery(spQuery, parameters).ToList();
            return workspacelist;
        }

        

    }
}
