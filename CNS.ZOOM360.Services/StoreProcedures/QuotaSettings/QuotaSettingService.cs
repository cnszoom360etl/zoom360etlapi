﻿using CNS.ZOOM360.Entities.StoreProcedures.QuotaSettings;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.QuotaSettings;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.QuotaSettings
{
    public class QuotaSettingService: IQuotaSettingService
    {
        private readonly IRepositoryBase<QuotaSettingsModel> _quotaSettingRepository;
        public QuotaSettingService(IRepositoryBase<QuotaSettingsModel> quotaSettingRepository) {
            _quotaSettingRepository = quotaSettingRepository;
        }

        public async Task<string> SaveQuotaSetting(QuotaSettingsModel quotaSettingsModel)
        {
            object[] parameters = {
            new SqlParameter("@USER_ID", quotaSettingsModel.UserId),
            new SqlParameter("@WORKSPACE_ID", quotaSettingsModel.WorkSpaceId),
            new SqlParameter("@CLIENT_ID", quotaSettingsModel.Client_Id),
            new SqlParameter("@QUOTA_LIMIT", quotaSettingsModel.QuotaLimit),
            new SqlParameter("@QUOTA_TYPE", quotaSettingsModel.QuotaType),
            new SqlParameter("@QUOTA_USAGE_CYCLE", quotaSettingsModel.QuotaUsageCycle),
            new SqlParameter("@QUOTA_START_DATE", quotaSettingsModel.QuotaStartDate),
            new SqlParameter("@QUOTA_END_DATE", quotaSettingsModel.QuotaEndDate),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
        };

            string spQuery = StoreProcedureConstants.Sp_SaveQuotaSetting + " @USER_ID,@WORKSPACE_ID," +
                " @CLIENT_ID, @QUOTA_LIMIT, @QUOTA_TYPE,@QUOTA_USAGE_CYCLE," +
                " @QUOTA_START_DATE, @QUOTA_END_DATE, @V_MESSAGE OUTPUT";
            return _quotaSettingRepository.ExecuteCommand(spQuery, parameters);


        }
    }
}
