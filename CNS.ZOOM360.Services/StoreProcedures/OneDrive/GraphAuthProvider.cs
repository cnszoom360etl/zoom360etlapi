﻿using Microsoft.Graph;
using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.OneDrive
{
  public  class GraphAuthProvider
    {
        public async Task<GraphServiceClient> AuthenticateViaAppIdAndSecret(
       string tenantId,
       string clientId,
       string clientSecret)
        {
            var scopes = new string[] { "https://graph.microsoft.com/.default" };
            string redirectUri = "http://localhost:4200/extract/AddNewConnection/Configuration";
            // Configure the MSAL client as a confidential client
            var confidentialClient = ConfidentialClientApplicationBuilder
                .Create(clientId)
                .WithAuthority($"https://login.microsoftonline.com/"+tenantId+"/v2.0")
                .WithClientSecret(clientSecret)
                .WithRedirectUri(redirectUri)
                .Build();

            // Build the Microsoft Graph client. As the authentication provider, set an async lambda
            // which uses the MSAL client to obtain an app-only access token to Microsoft Graph,
            // and inserts this access token in the Authorization header of each API request. 
            GraphServiceClient graphServiceClient =
                new GraphServiceClient(new DelegateAuthenticationProvider(async (requestMessage) =>
                {

                    // Retrieve an access token for Microsoft Graph (gets a fresh token if needed).
                    var authResult = await confidentialClient
                            .AcquireTokenForClient(scopes)
                            .ExecuteAsync();

                    // Add the access token in the Authorization header of the API request.
                    try
                    {
                        requestMessage.Headers.Authorization =
                         new AuthenticationHeaderValue("Bearer",authResult.AccessToken);
                    }
                    catch (Exception ex)
                    {

                        Console.Write(ex);
                    }
                    
                })
            );

            return graphServiceClient;
        }
    }
}
