﻿using CNS.ZOOM360.Entities.MongoDBModels;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;
using CNS.ZOOM360.Shared.StoreProcedures.MongoDBConnection;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using CNS.ZOOM360.Entities.StoreProcedures.Connectors.Databases.SQL;
using System.Linq;
using Microsoft.Data.SqlClient;
using CNS.ZOOM360.Shared.Const;
using System.Data;
using CNS.ZOOM360.Shared.Repositories;

namespace CNS.ZOOM360.Services.StoreProcedures.MongoDB
{
  public  class MongoDBService: ImongoConnection
    {
        SqlDataReader dataReader;
        SqlConnection cnn;
        SqlCommand command;
        public IMongoDatabase _db { get; set; }
        private MongoClient _mongoClient { get; set; }
        public IClientSessionHandle Session { get; set; }
        private readonly IRepositoryBase<GEtSourceObjectList> _sourceobject;
        public MongoDBService(IOptions<MongoDbEntities> configuration, IRepositoryBase<GEtSourceObjectList> sourceobject)
        {
            _mongoClient = new MongoClient(configuration.Value.ConnectionString);
            _db = _mongoClient.GetDatabase(configuration.Value.Database);
            _sourceobject = sourceobject;


        }
        //public List<string> GetCollections()
        //{
        //    List<string> collections = new List<string>();

        //    foreach (BsonDocument collection in _db.ListCollectionsAsync().Result.ToListAsync<BsonDocument>().Result)
        //    {
        //        string name = collection["name"].AsString;
        //        collections.Add(name);
        //    }

        //    return collections;
        //}
        //public List<string> GetCollectionFields(string collectionName)
        //{
        //    List<string> fields = new List<string>();

        //    var docs = this._db.GetCollection<BsonDocument>(collectionName).Find(new BsonDocument()).FirstOrDefault();
        //    //var list = docs.Names.ToList();
        //    foreach (var key in docs)
        //    {
        //        fields.Add(key.Name.ToString());
        //    };

        //    return fields;
        //}
        //public List<BsonDocument> list()
        //{
        //    var con = "mongodb://Ahsan:CNSE%4012345@192.168.223.111:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false";
        //    MongoClient dbClient = new MongoClient(con);
        //    MongoServer server = dbClient.GetServer();
        //    MongoDatabase database = server.GetDatabase("ZMDB");
        //    MongoClient client = new MongoClient(con);
        //    MongoServer servers = client.GetServer();
        //    MongoDatabase databas = servers.GetDatabase("Test");
        //    //List<MongoCollection> collections = databas.GetCollectionNames<BsonDocument>("");
        //    var dbList = dbClient.ListDatabases().ToList();

        //    Console.WriteLine("The list of databases on this server is: ");
        //    foreach (var db in dbList)
        //    {
        //        Console.WriteLine(db);
        //    }
        //    return dbList;
        //}
        public bool status(string Hostname, string UserName, string Password, string PortNumber, string Database ,string connectortitle , string connectorId, string Account_Id, string userId, string ClientId, string workspaceId)
        {
            var str = "";
            //var con = "mongodb://Ahsan:CNSE%4012345@192.168.223.111:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false";
            if (connectortitle == "My SQL")
            {
                string connect = "server=" + Hostname + "; Port ="+ PortNumber + ";user=" + UserName + ";password =" + Password + ";database=" + Database;

                try
                {
                    using (MySqlConnection conn = new MySqlConnection(connect))
                    {
                        conn.Open();
                        List<GEtSourceObjectList> list = new List<GEtSourceObjectList>();
                        //List<GEtSourceObjectList> list2 = new List<GEtSourceObjectList>();
                        //GEtSourceObjectList myentity1 = new GEtSourceObjectList();
                        MySqlCommand cmdName = new MySqlCommand("SHOW FULL TABLES", conn);
                        MySqlDataReader reader = cmdName.ExecuteReader();
                        while (reader.Read())
                        {
                            GEtSourceObjectList myentity = new GEtSourceObjectList();
                            myentity.OBJECT_NAME = reader[0].ToString() + "-" + reader[1].ToString();
                            list.Add(myentity);
                         }
                        reader.Close();
                          str = String.Join(",", list.Select(p => p.OBJECT_NAME));
                        //myentity1.OBJECT_NAME = str;
                        //list2.Add(myentity1);
                        
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    return false;
                    throw;
                }
                 
            }
            else if(connectortitle == "Mongo DB")
            {
                //var convertpassword = Password.Replace("@", "%40");
                try
                {
                    //var conv = "mongodb://" + UserName + ":" + convertpassword + "@" + Hostname + ":" + PortNumber;
                    var conv = "mongodb://localhost:27017/";
                    MongoClient dbClient = new MongoClient(conv);
                    var server = dbClient.GetServer();
                    server.Ping();
                    List<GEtSourceObjectList> list = new List<GEtSourceObjectList>();
                    foreach (BsonDocument collection in _db.ListCollectionsAsync().Result.ToListAsync<BsonDocument>().Result)
                    {
                        //string name = collection["name"].AsString;
                        GEtSourceObjectList nameofObject = new GEtSourceObjectList();
                        nameofObject.OBJECT_NAME = collection["name"].AsString+"-"+ collection["type"].AsString;
                        list.Add(nameofObject);
                    }
                      str = String.Join(",", list.Select(p => p.OBJECT_NAME));





                    //return true;
                }
                catch (Exception ex)
                {
                    return false;
                }



               

            }
            else if (connectortitle == "Microsoft SQL Server")
            {
                //var sqlcon = "server=" + Hostname + ";Database=" + UserName + ";password =" + Password + ";database=" + Database + " Integrated Security=false";
                 var sqlconnection= "Server="+ Hostname + ";Database="+ Database + ";Trusted_Connection=True;User Id="+ UserName + ";Password="+ Password + ";Integrated Security=false";
                try
                {
                    //SqlDataReader reader;
                    //List<GEtSourceObjectList> list = new List<GEtSourceObjectList>();
                    //SqlConnection SqlConn = new SqlConnection(sqlconnection);
                    //SqlCommand SqlCmd = new SqlCommand("GETSOURCEOBJECTLIST", SqlConn);
                    //SqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //SqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                    //SqlConn.Open();
                    //SqlCmd.Parameters.AddWithValue("@USER_ID", userId);
                    //SqlCmd.Parameters.AddWithValue("@WORKSPACE_ID", workspaceId);
                    //SqlCmd.Parameters.AddWithValue("@CLIENT_ID", ClientId);
                    //SqlCmd.Parameters.AddWithValue("@CONNECTOR_ID", connectorId);
                    //SqlCmd.Parameters.AddWithValue("@DATABASE_NAME",Database );
                    //SqlCmd.Parameters.AddWithValue("@MAPPED_TABLE", "Null");
                    //SqlCmd.Parameters.AddWithValue("@ACCOUNT_ID", Account_Id);
                    //SqlCmd.Parameters.Add(new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000));
                    //SqlCmd.Parameters["@V_MESSAGE"].Direction = ParameterDirection.Output;
                    //SqlCmd.ExecuteNonQuery();
                    return true;
                    //reader =   SqlCmd.ExecuteReader();
                    //while (reader.Read())
                    //{
                    //    GEtSourceObjectList myentity = new GEtSourceObjectList();
                    //        myentity.OBJECT_NAME = dataReader["TABLE_NAME"].ToString() + "-"+ dataReader["OBJECT_TYPE"].ToString();
                    //        list.Add(myentity);
                    //}
                    //SqlConn.Close();
                    //str = String.Join(",", list.Select(p => p.OBJECT_NAME));


                    //cnn = new SqlConnection(sqlconnection);
                    // cnn.Open();
                    // command= new  SqlCommand("select TABLE_NAME,'Table' as OBJECT_TYPE,TABLE_CATALOG  from INFORMATION_SCHEMA.TABLES union  select TABLE_NAME , 'View' as OBJECT_TYPE ,TABLE_CATALOG from INFORMATION_SCHEMA.VIEWS", cnn);

                    // List<GEtSourceObjectList> list = new List<GEtSourceObjectList>();
                    // dataReader = command.ExecuteReader(); 
                    // while (dataReader.Read())
                    // {
                    //     GEtSourceObjectList myentity = new GEtSourceObjectList();
                    //     myentity.OBJECT_NAME = dataReader["TABLE_NAME"].ToString() + "-"+ dataReader["OBJECT_TYPE"].ToString();
                    //     list.Add(myentity);
                    // }
                    // dataReader.Close();
                    // str = String.Join(",", list.Select(p => p.OBJECT_NAME));
                }
                catch (Exception ex)
                {
                    return false;
                }





            }
            object[] parameters = {
                         new SqlParameter("@USER_ID",userId),
                         new SqlParameter("@WORKSPACE_ID",workspaceId),
                         new SqlParameter("@CLIENT_ID",ClientId),
                         new SqlParameter("@CONNECTOR_ID",connectorId),
                         new SqlParameter("@ACCOUNT_ID",Account_Id),
                         new SqlParameter("@DATABASE_NAME",Database),
                         new SqlParameter("@OBJECT_LIST",str),
                         new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000) { Direction = ParameterDirection.Output }
                         };
                string spQuery = StoreProcedureConstants.Sp_SAVESOURCEOBJECTLIST + " @USER_ID, @WORKSPACE_ID, @CLIENT_ID, " +
               "  @CONNECTOR_ID,@ACCOUNT_ID, @DATABASE_NAME,@OBJECT_LIST," +
               "  @V_MESSAGE OUTPUT";
               _sourceobject.ExecuteCommand(spQuery, parameters);
               return true;


        }
    }
}
