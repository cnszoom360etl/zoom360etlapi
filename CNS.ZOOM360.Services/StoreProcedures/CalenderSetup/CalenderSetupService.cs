﻿using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.CalenderSetup;
using CNS.ZOOM360.Entities.StoreProcedures.CalenderSetup;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CNS.ZOOM360.Shared.Const;
using Microsoft.Data.SqlClient;
using System.Data;

namespace CNS.ZOOM360.Services.StoreProcedures.CalenderSetup
{
    public class CalenderSetupService: ICalenderSetupService
    {
        private readonly  IRepositoryBase<CalenderSetupModel> _CalenderSetupRepository;
        public CalenderSetupService(IRepositoryBase<CalenderSetupModel> CalenderSetupRepository) {
            _CalenderSetupRepository = CalenderSetupRepository;
        }


        public async Task<string> SaveCalenderSetup(CalenderSetupModel calenderSetupModel)
        {

            object[] parameters = { 
                new SqlParameter("@USER_ID", calenderSetupModel.UserId),
                new SqlParameter("@WORKSPACE_ID", calenderSetupModel.WorkspaceId),
                new SqlParameter("@CLIENT_ID", calenderSetupModel.ClientId),
                new SqlParameter("@CALENDER_SETUP", calenderSetupModel.CalenderSetup),
                new SqlParameter("@BUSSINESS_YEAR_DATE", calenderSetupModel.BussinessYearDate),
                new SqlParameter("@FINANCIAL_YEAR_DATE", calenderSetupModel.FinacialYearDate),
                new SqlParameter("@REPORTING_YEAR_DATE", calenderSetupModel.ReportingYearDate),
                new SqlParameter("@WEEK_START_DAY", calenderSetupModel.WeekStartDay),
                new SqlParameter("@ANNUAL_HOLIDAY_CALDENER", calenderSetupModel.AnnualHolidayCalender),
                new SqlParameter("@ANNUAL_CAMPAIGNS_CALDENER", calenderSetupModel.AnnualCampaignCalender),
                new SqlParameter("@NOTIFY_CAMPAIGNS_CALDENER", calenderSetupModel.NotifyCampaignsCalender),
                new SqlParameter("@MILESTONE_ANNUAL_HOLIDAY_CALDENER", calenderSetupModel.MilestoneAnnualHolidayCalender),
                new SqlParameter("@NOTIFY_MILESTONE_CALDENER", calenderSetupModel.NotifyMilestoneCalender),
                new SqlParameter("@CALENDER_APPLY_AND_ENFORCE", calenderSetupModel.CalenderApplyAndEnforce),
                new SqlParameter{ ParameterName = "@V_MESSAGE",
            Direction = ParameterDirection.Output,
            SqlDbType = SqlDbType.NVarChar,
            Size = 4000,
            Value = ""
                }
        
        };

            string spQuery = StoreProcedureConstants.Sp_SaveCalenderSetup + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID,  @CALENDER_SETUP,@BUSSINESS_YEAR_DATE, @FINANCIAL_YEAR_DATE, @REPORTING_YEAR_DATE, @WEEK_START_DAY, @ANNUAL_HOLIDAY_CALDENER, @ANNUAL_CAMPAIGNS_CALDENER, @NOTIFY_CAMPAIGNS_CALDENER, @MILESTONE_ANNUAL_HOLIDAY_CALDENER, @NOTIFY_MILESTONE_CALDENER, @CALENDER_APPLY_AND_ENFORCE, @V_MESSAGE OUTPUT";
            return _CalenderSetupRepository.ExecuteCommand(spQuery, parameters);

        }

    }
}
