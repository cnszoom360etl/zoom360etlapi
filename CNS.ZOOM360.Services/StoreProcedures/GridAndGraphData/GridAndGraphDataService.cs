﻿using CNS.ZOOM360.Entities.StoreProcedures.GridAndGraphData;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.GridAndGraphData;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.GridAndGraphData
{
    public class GridAndGraphDataService : IGridAndGraphDataService
    {
        private readonly IRepositoryBase<GraphDataModel> _GraphDataRepository;
        private readonly IRepositoryBase<GridDataModel> _GirdDataRepository;
        private readonly IRepositoryBase<DataTable> _dynamicGridRepository;
        //private object _changeLogListRepository;

        public GridAndGraphDataService(IRepositoryBase<GraphDataModel> GraphDataRepository, IRepositoryBase<GridDataModel> GirdDataRepository, IRepositoryBase<DataTable> dynamicGridRepository)
        {
            _GraphDataRepository = GraphDataRepository;
            _GirdDataRepository = GirdDataRepository;
            _dynamicGridRepository= dynamicGridRepository;
    }

        public async Task<List<GraphDataModel>> getGraphData()
        {
            // object[] parameters = null;
            //    object[] parameters = {
            //    new SqlParameter("@USER_ID", userId),
            //    new SqlParameter("@CLIENT_ID", CLIENT_ID),
            //    new SqlParameter("@WORKSPACE_ID", workSpaceId),
            //    new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
            //};

            string spQuery = "select * from excel_channel_data_processed";
            List<GraphDataModel> graphData = _GraphDataRepository.ExecutePlainQuery(spQuery).ToList();
            return graphData;

        }
        public async Task<List<GridDataModel>> getGridData()
        {
            // object[] parameters = null;
            //    object[] parameters = {
            //    new SqlParameter("@USER_ID", userId),
            //    new SqlParameter("@CLIENT_ID", CLIENT_ID),
            //    new SqlParameter("@WORKSPACE_ID", workSpaceId),
            //    new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
            //};

            string spQuery = "select * from INVOICE_DATA";
            List<GridDataModel> gridData = _GirdDataRepository.ExecutePlainQuery(spQuery).ToList();
            return gridData;

        }
        public async Task<DataTable> dynamicGridData(string userID, string WorkSpaceId, string Client_Id, string analysisType)
        {
            
            //DataTable dataTable = new DataTable();
            DbParameter[] parameters = {
            new SqlParameter("@USER_ID",userID),
            new SqlParameter("@WORKSPACE_ID", WorkSpaceId),
            new SqlParameter("@CLIENT_ID", Client_Id),
            new SqlParameter("@ANALYZE_TYPE", analysisType),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
                };
           string spQuery = StoreProcedureConstants.Sp_GetAnalyzeData + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID," +
               " @ANALYZE_TYPE, @V_MESSAGE OUTPUT";
            DataTable dataTable = _dynamicGridRepository.getDynamicgrid(spQuery, parameters);
            return dataTable;
        }
    
    }
}
