﻿using CNS.ZOOM360.Entities.Model;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.Extraction;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.Extraction
{
   public  class ExtractionServices: IExtraction
    {
        private readonly IRepositoryBase<UserGrantChecked> _checkUserAccessGrant;

		public ExtractionServices(IRepositoryBase<UserGrantChecked> checkUserAccessGrant)
		{

			_checkUserAccessGrant = checkUserAccessGrant;
		}
		public  string CheckUserAccessRights(string UserId, string Account_Id, string Workspaceid, string Clientid, string connectorId)
		{
			 
				object[] parameters = {
			new SqlParameter("@USER_ID",UserId),
			new SqlParameter("@WORKSPACE_ID",Workspaceid),
			new SqlParameter("@CLIENT_ID",Clientid),
			new SqlParameter("@ACCOUNT_ID",Account_Id),
			new SqlParameter("CONNECTOR_ID",connectorId),
			new SqlParameter("@V_RESPONSE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output },
			new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
			 };
             string spQuery = StoreProcedureConstants.Sp_CheckUserRights + " @USER_ID, @WORKSPACE_ID, @CLIENT_ID,@ACCOUNT_ID,  @CONNECTOR_ID," +
					 " @V_RESPONSE OUTPUT, @V_MESSAGE OUTPUT";
				return _checkUserAccessGrant.ExecuteCommand(spQuery, parameters);
			 
			 
			
		}


	}
}
