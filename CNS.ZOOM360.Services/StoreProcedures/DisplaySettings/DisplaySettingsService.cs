﻿using CNS.ZOOM360.Entities.StoreProcedures.DisplaySettings;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.DisplaySettings;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.DisplaySettings
{
    public class DisplaySettingsService:IDisplaySettingsService
    {
        private readonly IRepositoryBase<DisplaySettingsModel> _displaySettingRepository;
        public DisplaySettingsService(IRepositoryBase<DisplaySettingsModel> displaySettingRepository) {
            _displaySettingRepository = displaySettingRepository;
        }


        public async Task<string> SaveDisplaySetting(DisplaySettingsModel displaySettingsModel)
        {
            object[] parameters = {

            new SqlParameter("@USER_ID", displaySettingsModel.UserId),
            new SqlParameter("@WORKSPACE_ID", displaySettingsModel.WorkSpaceId),
            new SqlParameter("@CLIENT_ID", displaySettingsModel.Client_Id),
            new SqlParameter("@WORKSPACE_DISPLAY_MODE", displaySettingsModel.WorkspaceDisplayMode),
            new SqlParameter("@WORKSPACE_LOGO", displaySettingsModel.WorkspaceLogo),
            new SqlParameter("@LOGO_BACKGROUND_COLOR", displaySettingsModel.LogoBackgroundColor),
            new SqlParameter("@WORKSPACE_THEME", displaySettingsModel.WorkspaceTheme),
            new SqlParameter("@COLOR_PALETTE", displaySettingsModel.ColorPallete),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){Direction = ParameterDirection.Output }
        };
            string spQuery = StoreProcedureConstants.Sp_SaveDisplaySettings + " @USER_ID,@WORKSPACE_ID," +
                " @CLIENT_ID, @WORKSPACE_DISPLAY_MODE, @WORKSPACE_LOGO," +
                " @LOGO_BACKGROUND_COLOR, @WORKSPACE_THEME, @COLOR_PALETTE, @V_MESSAGE OUTPUT";
            return _displaySettingRepository.ExecuteCommand(spQuery, parameters);
        }
    }
}
