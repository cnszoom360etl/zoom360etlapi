﻿using CNS.ZOOM360.Entities.TimeZoneSetup;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures.TimeZoneSetup;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures.TimeZoneSetup
{
    
    public class TimeZoneSetupService: ITimeZoneSetupService
    {
        private readonly IRepositoryBase<TimeZoneSetupModel> _timeZOneSetupRepository;

        public TimeZoneSetupService(IRepositoryBase<TimeZoneSetupModel> timeZOneSetupRepository) {
            _timeZOneSetupRepository = timeZOneSetupRepository;
        }

        public async Task<String> SaveTimeZoneSetup(TimeZoneSetupModel TimeZoneModel)
        {
            object[] parameters = {
            new SqlParameter("@USER_ID", TimeZoneModel.UserId),
            new SqlParameter("@WORKSPACE_ID", TimeZoneModel.WorkSpaceId),
            new SqlParameter("@CLIENT_ID", TimeZoneModel.Client_Id),
            new SqlParameter("@DATE_FORMAT_TYPE", TimeZoneModel.DataFormatType),
            new SqlParameter("@DATE_FORMAT", TimeZoneModel.DateFormat),
            new SqlParameter("@CLOCK_IMAGE", TimeZoneModel.ClockImage),
            new SqlParameter("@DATE_COLLECTED_DATA", TimeZoneModel.DateCollectedData),
            new SqlParameter("@DATE_PREPARING_DATA", TimeZoneModel.DatePreparingData),
            new SqlParameter("@DATE_PRESENTING_DATA", TimeZoneModel.DatePresentingData),
            new SqlParameter("@DATE_CONVERSION", TimeZoneModel.DateConversion),
            new SqlParameter("@DATE_CONVERSION_VALUE", TimeZoneModel.DateConversionValue),
            new SqlParameter("@TIME_ZONE", TimeZoneModel.TimeZone),
            new SqlParameter("@TIME_ZONE_TYPE", TimeZoneModel.TimeZoneType),
            new SqlParameter("@DATE_FORMAT_REPORTS", TimeZoneModel.DateFormatReports),
            new SqlParameter("@REPORTS_DATE", TimeZoneModel.ReportsDate),
            new SqlParameter("@DATE_FORMAT_VISUALIZATION", TimeZoneModel.DateFormatVisulization),
            new SqlParameter("@VISUALIZATION_DATE", TimeZoneModel.VisulizationDate),
            new SqlParameter("@TIME_FORMAT_REPORTS", TimeZoneModel.TimeFormatReports),
            new SqlParameter("@REPORTS_TIME", TimeZoneModel.ReportTime),
            new SqlParameter("@TIME_FORMAT_VISUALIZATION", TimeZoneModel.TimeFormatVisualization),
            new SqlParameter("@VISUALIZATION_TIME", TimeZoneModel.VisualizationTime),
            new SqlParameter("@APPLY_AND_ENFORCE_DATETIME", TimeZoneModel.ApplyAndEnforceDatetime),
            new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output }
        };


            string spQuery = StoreProcedureConstants.Sp_SaveTimeZoneSetup + " @USER_ID,@WORKSPACE_ID," +
                 " @CLIENT_ID, @DATE_FORMAT_TYPE, @DATE_FORMAT,@CLOCK_IMAGE, @DATE_COLLECTED_DATA," +
                 " @DATE_PREPARING_DATA ,@DATE_PRESENTING_DATA, @DATE_CONVERSION, @DATE_CONVERSION_VALUE," +
                 " @TIME_ZONE, @TIME_ZONE_TYPE, @DATE_FORMAT_REPORTS, @REPORTS_DATE, @DATE_FORMAT_VISUALIZATION," +
                 " @VISUALIZATION_DATE, @TIME_FORMAT_REPORTS, @REPORTS_TIME, @TIME_FORMAT_VISUALIZATION," +
                 " @VISUALIZATION_TIME, @APPLY_AND_ENFORCE_DATETIME, @V_MESSAGE OUTPUT";
            return _timeZOneSetupRepository.ExecuteCommand(spQuery, parameters);

        }

    }
}
