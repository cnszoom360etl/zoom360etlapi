﻿using CNS.ZOOM360.Entities.StoreProcedures;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures
{
    public class valueTableListServices : IvalueTableList
    {
        private readonly IRepositoryBase<valueTableListModel> _ValueTableListRepository;
        public valueTableListServices(IRepositoryBase<valueTableListModel> ValueTableListRepository)
        {
            _ValueTableListRepository = ValueTableListRepository;
        }
        public async Task<List<valueTableListModel>> GetValueTablelist(valueTableInputModel valueTableInput)
        {
            object[] parameters = {
           new SqlParameter("@USER_ID",valueTableInput.userId),
           new SqlParameter("@WORKSPACE_ID", valueTableInput.workspaceId),
                new SqlParameter("@CLIENT_ID", valueTableInput.ClientId),
                new SqlParameter("@VALUE_TABLE",string.IsNullOrEmpty(valueTableInput.valueTable) ? (object)DBNull.Value: valueTableInput.valueTable),
                new SqlParameter("@V_MESSAGE",SqlDbType.NVarChar,4000){Direction = ParameterDirection.Output }
        };
            string spQuery = StoreProcedureConstants.Sp_GETVALUETABLELIST + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID,@VALUE_TABLE, @V_MESSAGE OUTPUT";
            List<valueTableListModel> valueTableList = _ValueTableListRepository.ExecuteQuery(spQuery, parameters).ToList();
            return valueTableList;
        }
    }
}