﻿using CNS.ZOOM360.Entities.StoreProcedures;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Shared.StoreProcedures;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Services.StoreProcedures
{
    public class ValueTableDataServices : IValueTableDataServices
    {
        private readonly IRepositoryBase<valueTableIDataModel> _ValueTableDataRepository;
        public ValueTableDataServices(IRepositoryBase<valueTableIDataModel> ValueTableDataRepository)
        {
            _ValueTableDataRepository = ValueTableDataRepository;
        }
        public async Task<List<valueTableIDataModel>> GetValueTableData(string userId, string workspaceId, string clientId, string valueTable)
        {
            object[] parameters = {
           new SqlParameter("@USER_ID",userId),
           new SqlParameter("@WORKSPACE_ID",workspaceId),
                new SqlParameter("@CLIENT_ID",clientId),
                new SqlParameter("@VALUE_TABLE",valueTable),
                new SqlParameter("@V_MESSAGE",SqlDbType.NVarChar,4000){Direction = ParameterDirection.Output }
           };
            string spQuery = StoreProcedureConstants.Sp_GETVALUETABLEDATA + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID,@VALUE_TABLE, @V_MESSAGE OUTPUT";
            List<valueTableIDataModel> valueTableData = _ValueTableDataRepository.ExecuteQuery(spQuery, parameters).ToList();
            return valueTableData;
        }

        //    public async Task<string> SaveValueTable(saveValueTableModel _saveValueTableModel)
        //    {
        //        object[] parameters = {
        //            new SqlParameter("@USER_ID", _saveValueTableModel.userId),
        //            new SqlParameter("@WORKSPACE_ID", _saveValueTableModel.workspaceId),
        //            new SqlParameter("@CLIENT_ID", _saveValueTableModel.clientId),
        //            new SqlParameter("@VALUE_TABLE_NAME", _saveValueTableModel.valueTableName),
        //            new SqlParameter("@SOURCE_VALUE", _saveValueTableModel.sourceValue),
        //             new SqlParameter("@TARGET_VALUE", _saveValueTableModel.targetValue),
        //            new SqlParameter("@ENABLED", _saveValueTableModel.targetValue),

        //        new SqlParameter("@CLIENT_DATE",string.IsNullOrEmpty(_saveValueTableModel.ClientDate) ? (object)DBNull.Value: _saveValueTableModel.ClientDate),
        //        new SqlParameter("@CLIENT_TIME",string.IsNullOrEmpty(_saveValueTableModel.ClientTime) ? (object)DBNull.Value: _saveValueTableModel.ClientTime),
        //        new SqlParameter("@CLIENT_TIME_ZONE",string.IsNullOrEmpty(_saveValueTableModel.ClientTimeZone) ? (object)DBNull.Value: _saveValueTableModel.ClientTimeZone),
        //        new SqlParameter("@REMARKS_1",string.IsNullOrEmpty(_saveValueTableModel.remark1) ? (object)DBNull.Value: _saveValueTableModel.remark1),
        //        new SqlParameter("@REMARKS_2",string.IsNullOrEmpty(_saveValueTableModel.remark2) ? (object)DBNull.Value: _saveValueTableModel.remark2),
        //        new SqlParameter("@REMARKS_3",string.IsNullOrEmpty(_saveValueTableModel.remark3) ? (object)DBNull.Value: _saveValueTableModel.remark3),
        //        new SqlParameter("@REMARKS_4",string.IsNullOrEmpty(_saveValueTableModel.remark4) ? (object)DBNull.Value: _saveValueTableModel.remark4),
        //        new SqlParameter("@FLEX_1",string.IsNullOrEmpty(_saveValueTableModel.Flex1) ? (object)DBNull.Value: _saveValueTableModel.Flex1),
        //        new SqlParameter("@FLEX_2",string.IsNullOrEmpty(_saveValueTableModel.Flex2) ? (object)DBNull.Value: _saveValueTableModel.Flex2),
        //        new SqlParameter("@FLEX_3",string.IsNullOrEmpty(_saveValueTableModel.Flex3) ? (object)DBNull.Value: _saveValueTableModel.Flex3),
        //        new SqlParameter("@FLEX_4",string.IsNullOrEmpty(_saveValueTableModel.Flex4) ? (object)DBNull.Value: _saveValueTableModel.Flex4),
        //        new SqlParameter("@FLEX_5",string.IsNullOrEmpty(_saveValueTableModel.Flex5) ? (object)DBNull.Value: _saveValueTableModel.Flex5),
        //        new SqlParameter("@FLEX_6",string.IsNullOrEmpty(_saveValueTableModel.Flex6) ? (object)DBNull.Value: _saveValueTableModel.Flex6),
        //        new SqlParameter("@FLEX_7",string.IsNullOrEmpty(_saveValueTableModel.Flex7) ? (object)DBNull.Value: _saveValueTableModel.Flex7),
        //        new SqlParameter("@FLEX_8",string.IsNullOrEmpty(_saveValueTableModel.Flex8) ? (object)DBNull.Value: _saveValueTableModel.Flex8),
        //        new SqlParameter("@FLEX_9",string.IsNullOrEmpty(_saveValueTableModel.Flex9) ? (object)DBNull.Value: _saveValueTableModel.Flex8),
        //        new SqlParameter("@FLEX_10",string.IsNullOrEmpty(_saveValueTableModel.Flex10) ? (object)DBNull.Value: _saveValueTableModel.Flex10),
        //        new SqlParameter("@FLEX_11",string.IsNullOrEmpty(_saveValueTableModel.Flex11) ? (object)DBNull.Value: _saveValueTableModel.Flex11),
        //        new SqlParameter("@FLEX_12",string.IsNullOrEmpty(_saveValueTableModel.Flex12) ? (object)DBNull.Value: _saveValueTableModel.Flex12),
        //        new SqlParameter("@FLEX_13",string.IsNullOrEmpty(_saveValueTableModel.Flex13) ? (object)DBNull.Value: _saveValueTableModel.Flex13),
        //        new SqlParameter("@FLEX_14",string.IsNullOrEmpty(_saveValueTableModel.Flex14) ? (object)DBNull.Value: _saveValueTableModel.Flex14),
        //        new SqlParameter("@FLEX_15",string.IsNullOrEmpty(_saveValueTableModel.Flex15) ? (object)DBNull.Value: _saveValueTableModel.Flex15),
        //        new SqlParameter("@FLEX_16",string.IsNullOrEmpty(_saveValueTableModel.Flex16) ? (object)DBNull.Value: _saveValueTableModel.Flex16),
        //        new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){Direction = ParameterDirection.Output }



        //    };

        //        string spQuery = StoreProcedureConstants.Sp_SAVEVALUETABLEDATA + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID,@VALUE_TABLE_NAME,@SOURCE_VALUE,@TARGET_VALUE,@ENABLED,@CLIENT_DATE,@CLIENT_TIME, @CLIENT_TIME_ZONE," +
        //            " @REMARKS_1, @REMARKS_2, @REMARKS_3, @REMARKS_4, @FLEX_1, @FLEX_2, @FLEX_3,@FLEX_4,@FLEX_5, @FLEX_6," +
        //            " @FLEX_7, @FLEX_8, @FLEX_9, @FLEX_10, @FLEX_11, @FLEX_12, @FLEX_13, @FLEX_14, @FLEX_15, @FLEX_16 ," +
        //            " @V_MESSAGE OUTPUT";
        //        return _ValueTableDataRepository.ExecuteCommand(spQuery, parameters);

        //    }
        //}




        public async Task<string> SaveValueTable(valueTableIDataModel valueTableIDataModel, string UserId, string workspaceId, string clientId, string valuetable, string ClientDate, string ClientTime, string ClientTimeZone)
        {
            object[] parameters = {

                    new SqlParameter("@USER_ID",UserId),
                    new SqlParameter("@WORKSPACE_ID", workspaceId),
                    new SqlParameter("@CLIENT_ID",clientId),
                    new SqlParameter("@VALUE_TABLE_NAME",valuetable),
                    new SqlParameter("@SOURCE_VALUE", string.IsNullOrEmpty(valueTableIDataModel.sourceValue) ? (object)DBNull.Value:valueTableIDataModel.sourceValue),
                    new SqlParameter("@TARGET_VALUE", string.IsNullOrEmpty(valueTableIDataModel.targetValue) ? (object)DBNull.Value:valueTableIDataModel.targetValue),
                      new SqlParameter("@ENABLED",valueTableIDataModel.bStatus),
                     new SqlParameter("@CLIENT_DATE",ClientDate),
                     new SqlParameter("@CLIENT_TIME",ClientTime),
                     new SqlParameter("@CLIENT_TIME_ZONE",ClientTimeZone),
                    
                      
                new SqlParameter("@REMARKS_1",string.IsNullOrEmpty(valueTableIDataModel.Remark1) ? (object)DBNull.Value: valueTableIDataModel.Remark1),
                new SqlParameter("@REMARKS_2",string.IsNullOrEmpty(valueTableIDataModel.Remark2) ? (object)DBNull.Value: valueTableIDataModel.Remark2),
                new SqlParameter("@REMARKS_3",string.IsNullOrEmpty(valueTableIDataModel.Remark3) ? (object)DBNull.Value: valueTableIDataModel.Remark3),
                new SqlParameter("@REMARKS_4",string.IsNullOrEmpty(valueTableIDataModel.Remark4) ? (object)DBNull.Value: valueTableIDataModel.Remark4),
                new SqlParameter("@FLEX_1",string.IsNullOrEmpty(valueTableIDataModel.Flex1) ? (object)DBNull.Value: valueTableIDataModel.Flex1),
                new SqlParameter("@FLEX_2",string.IsNullOrEmpty(valueTableIDataModel.Flex2) ? (object)DBNull.Value: valueTableIDataModel.Flex2),
                new SqlParameter("@FLEX_3",string.IsNullOrEmpty(valueTableIDataModel.Flex3) ? (object)DBNull.Value: valueTableIDataModel.Flex3),
                new SqlParameter("@FLEX_4",string.IsNullOrEmpty(valueTableIDataModel.Flex4) ? (object)DBNull.Value: valueTableIDataModel.Flex4),
                new SqlParameter("@FLEX_5",string.IsNullOrEmpty(valueTableIDataModel.Flex5) ? (object)DBNull.Value: valueTableIDataModel.Flex5),
                new SqlParameter("@FLEX_6",string.IsNullOrEmpty(valueTableIDataModel.Flex6) ? (object)DBNull.Value: valueTableIDataModel.Flex6),
                new SqlParameter("@FLEX_7",string.IsNullOrEmpty(valueTableIDataModel.Flex7) ? (object)DBNull.Value: valueTableIDataModel.Flex7),
                new SqlParameter("@FLEX_8",string.IsNullOrEmpty(valueTableIDataModel.Flex8) ? (object)DBNull.Value: valueTableIDataModel.Flex8),
                new SqlParameter("@FLEX_9",string.IsNullOrEmpty(valueTableIDataModel.Flex9) ? (object)DBNull.Value: valueTableIDataModel.Flex8),
                new SqlParameter("@FLEX_10",string.IsNullOrEmpty(valueTableIDataModel.Flex10) ? (object)DBNull.Value: valueTableIDataModel.Flex10),
                new SqlParameter("@FLEX_11",string.IsNullOrEmpty(valueTableIDataModel.Flex11) ? (object)DBNull.Value: valueTableIDataModel.Flex11),
                new SqlParameter("@FLEX_12",string.IsNullOrEmpty(valueTableIDataModel.Flex12) ? (object)DBNull.Value: valueTableIDataModel.Flex12),
                new SqlParameter("@FLEX_13",string.IsNullOrEmpty(valueTableIDataModel.Flex13) ? (object)DBNull.Value: valueTableIDataModel.Flex13),
                new SqlParameter("@FLEX_14",string.IsNullOrEmpty(valueTableIDataModel.Flex14) ? (object)DBNull.Value: valueTableIDataModel.Flex14),
                new SqlParameter("@FLEX_15",string.IsNullOrEmpty(valueTableIDataModel.Flex15) ? (object)DBNull.Value: valueTableIDataModel.Flex15),
                new SqlParameter("@FLEX_16",string.IsNullOrEmpty(valueTableIDataModel.Flex16) ? (object)DBNull.Value: valueTableIDataModel.Flex16),
                new SqlParameter("@V_MESSAGE", SqlDbType.NVarChar, 4000){Direction = ParameterDirection.Output }



            };

            string spQuery = StoreProcedureConstants.Sp_SAVEVALUETABLEDATA + " @USER_ID,@WORKSPACE_ID, @CLIENT_ID,@VALUE_TABLE_NAME,@SOURCE_VALUE,@TARGET_VALUE,@ENABLED,@CLIENT_DATE,@CLIENT_TIME,@CLIENT_TIME_ZONE," +
                " @REMARKS_1, @REMARKS_2, @REMARKS_3, @REMARKS_4, @FLEX_1, @FLEX_2, @FLEX_3,@FLEX_4,@FLEX_5, @FLEX_6," +
                " @FLEX_7, @FLEX_8, @FLEX_9, @FLEX_10, @FLEX_11, @FLEX_12, @FLEX_13, @FLEX_14, @FLEX_15, @FLEX_16 ," +
                " @V_MESSAGE OUTPUT";
            return _ValueTableDataRepository.ExecuteCommand(spQuery, parameters);

        }
    }
}

 

