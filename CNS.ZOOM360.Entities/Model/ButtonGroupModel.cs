﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.Model
{
   public  class ButtonGroupModel
    {
        [Column("INPUT_TYPE")]
        public string inputType { get; set; }
        [Column("INPUT_ID")]
        public string inputId { get; set; }
        [Column("INPUT_NAME")]
        public string inputName { get; set; }
        [Column("INPUT_VALUE")]
        public string value { get; set; }
        
        [Column("(ngModel)")]
        public string ngModel { get; set; }
        [Column("LABEL_ID")]
        public string labelId { get; set; }
        [Column("NGCLASS")]
        public string ngClass { get; set; }
        [Column("FOR")]
        public string htmlAttribute { get; set; }
        [Column("CLICK")]
        public string click { get; set; }
        [Column("BUTTON_DISPLAY_TEXT")]
        public string buttonDisplayName { get; set; }

    }
    public class ButtonGroupModelForPerams
    {
        public string userId { get; set; }
        public string clientid { get; set; }
        public string workspaceId { get; set; }
        public string inputButton { get; set; }

    }
}
