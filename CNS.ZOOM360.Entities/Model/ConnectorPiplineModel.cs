﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.Model
{
   public class ConnectorPiplineModel
    {
        public string USER_ID { get; set; }
        public string WORKSPACE_ID { get; set; }
        public string CLIENT_ID { get; set; }
        public string PIPELINE_ID { get; set; }
        public string CONNECTOR_ID { get; set; }
        public string CONNECTORS_NAME { get; set; }
        public string CONNECTOR_DISPLAY_NAME { get; set; }
        public string ACCOUNT_ID { get; set; }
        public string CLIENT_DATE { get; set; }
        public string CLIENT_TIME { get; set; }
        public string CLIENT_TIME_ZONE { get; set; }
        public string REMARKS_1 { get; set; }
        public string REMARKS_2 { get; set; }
        public string REMARKS_3 { get; set; }
        public string REMARKS_4 { get; set; }
        public string FLEX_1 { get; set; }
        public string FLEX_2 { get; set; }
        public string FLEX_3 { get; set; }
        public string FLEX_4 { get; set; }
        public string FLEX_5 { get; set; }
        public string FLEX_6 { get; set; }
        public string FLEX_7 { get; set; }
        public string FLEX_8 { get; set; }
        public string FLEX_9 { get; set; }
        public string FLEX_10 { get; set; }
        public string FLEX_11 { get; set; }
        public string FLEX_12 { get; set; }
        public string FLEX_13 { get; set; }
        public string FLEX_14 { get; set; }
        public string FLEX_15 { get; set; }
        public string FLEX_16 { get; set; }









    }



    public class GETPIPELINEMODEL
    {
        [Column("PIPELINE_ID")]
        public int pipeline { get; set; }
        
        [Column("CONNECTOR_DISPLAY_NAME")]
        public string connectordisplayname { get; set; }
        
        [Column("CONNECTOR_ID")]
        public string connectorid { get; set; }
        
        [Column("ACCOUNT_ID")]
        public string accountid { get; set; }

        [Column("DISPLAY_NAME")]
        public string connectorname { get; set; }

    }
}
