﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.Model
{
    public class extractionobject
    {
        public string accountdisplayname { get; set; }
        public string ConnectorType { get; set; }
        public List<string> tablename { get; set; }

        public credentialarrays fieldvalue { get; set; }


    }
    public class credentialarrays
    {
        public string hostName { get; set; }
        public string databaseName { get; set; }
        public string port { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }

}
