﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.Model
{
  public  class GetDectinationListForLoad
    {
        [Column("CLIENT_CONNECTOR_ID")]
        public int connectorId { get; set; }
        [Column("DES_ACCOUNT_ID")]
        public int accountId { get; set; }
       
        [Column("FIELD_NAME")]
        public string Filedname { get; set; }
        [Column("FIELD_VALUE")]
        public string Fieldvalue { get; set; }
        [Column("CONNECTOR_DISPLAY_NAME")]
        public string connectorname { get; set; }
        
        [Column("OBJECT_NAME")]
        public string objectname { get; set; }
       
        [Column("DATA_OPTION")]
        public string dataoption { get; set; }
    }
}
