﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.Model
{
   public class AccessTokenModel
    {
        public string Access_Token { get; set; }
    }
    public class AccessUser
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }
    }
}
