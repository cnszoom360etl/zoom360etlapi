﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.Model
{
   public class FileManagerModel
    {
        public System.IO.FileInfo[] Files { get; set; }
        public IFormFile file { get; set; }
        public List<IFormFile> IFormFiles { get; set; }
    }
}
