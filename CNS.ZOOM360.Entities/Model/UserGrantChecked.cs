﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.Model
{
   public class UserGrantChecked
    {
        public string account_Id { get; set; }
        public string userId { get; set; }
        public string workspaceid { get; set; }
        public string clientid { get; set; }

        public ExtractionModel[] connectorIds { get; set; }
    }
    public class ExtractionModel
    {
        public string accountid { get; set; }
        public string connectorname { get; set; }
        public string connectorId { get; set; }
        public string displayName { get; set; }
        
    }
}
