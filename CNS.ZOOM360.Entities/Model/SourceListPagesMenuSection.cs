﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.Model
{
    public class SourceListPagesMenuSection
    {
        [Column("SUB_MENU_ID_2")]
        public string submenuid { get; set; }
        [Column("SUB_MENU_NAME_2")]
        public string submenuname { get; set; }
        [Column("URL_2")]
        public string submenuurl { get; set; }
        [Column("BSTATUS")]
        public string bstatus { get; set; }
        [Column("URL_STATUS_2")]
        public string statusurl2 { get; set; }

    }
    public class SourceListPageModel
    {
        public string  userId { get; set; }
        public string subUserId { get; set; }
        public string clientId { get; set; }
        public string workspaceId { get; set; }
        public string mainMenuId { get; set; }
        public string treeLevel { get; set; }
        public string treeNode { get; set; }
         

    }
}
