﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.MongoDBModels
{
   public class MongoDbEntities
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}
