﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.MongoDBModels
{
  public   class _90_Days_Data
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Date { get; set; }
        public string Full_Name { get; set; }
        public string Disposition { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Adset { get; set; }
        public string Ad { get; set; }
        public string Campaign { get; set; }
        public string LeadID { get; set; }

    }
}
