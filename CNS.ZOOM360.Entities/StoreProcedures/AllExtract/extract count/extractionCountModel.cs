﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.AllExtract.extract_count
{
   public class extractionCountModel
    {
        [Column("EXTRACTION_COMPLETE_COUNT")]
        public int extractionCompleteCount { get; set; }
        [Column("EXTRACTION_TOTAL_COUNT")]
        public int extractionTotalCount { get; set; }
        [Column("ENRICHMENT_COMPLETE_COUNT")]
        public int enrichmentCompleteCount { get; set; }
        [Column("ENRICHMENT_TOTAL_COUNT")]
        public int enrichmentTotalCount { get; set; }
        [Column("LOAD_COMPLETE_COUNT")]
        public int loadCompleteCount { get; set; }
        [Column("LOAD_TOTAL_COUNT")]
        public int loadTotalCount { get; set; }
    }
}
