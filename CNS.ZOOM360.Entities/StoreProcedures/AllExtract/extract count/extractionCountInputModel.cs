﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.AllExtract.extract_count
{
   public class extractionCountInputModel
    {
        public string userId { get; set; }
        public string workspaceId { get; set; }
        public string clientId { get; set; }
        public string connectorId { get; set; }
        public string accountId { get; set; }
        public string lastAccess { get; set; }
    }
}
