﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.AllExtract.extract_count
{
   public class connectionListExtractionModel
    {
        [Column("SOURCE_ACCOUNT_ID")]
        public Int32 sourceAccountId { get; set; }
        [Column("APPEARANCE_LOGO")]
        public string AppearanceLogo { get; set; }
        [Column("CONNECTOR_NAME")]
        public string ConnectorName { get; set; }
        [Column("SOURCE_CONNECTOR_ID")]
        public Int32 SourceConnectorId { get; set; }
        [Column("WORKSPACE_NAME")]
        public string WorkspaceName { get; set; }
        [Column("CONNECTOR_GROUPING")]
        public string connectorGrouping { get; set; }
        [Column("ACCOUNT_DISPLAY_NAME")]
        public string AccountDisplayName { get; set; }
        [Column("DESTINATION_ENABLED")]
        public string DestinationEnabled { get; set; }
        [Column("ACCESS_GRANTED")]
        public string Accessgranted { get; set; }
    }
}
