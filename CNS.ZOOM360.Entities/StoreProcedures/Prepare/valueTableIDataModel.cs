﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures
{
   public class valueTableIDataModel
    {
        
        [Column("SOURCE_VALUE")]
        public string? sourceValue { get; set; }
        [Column("TARGET_VALUE")]
        public string? targetValue { get; set; }
        [Column("USER_NAME_INSERT")]
        public string? userNameInsert { get; set; }
        [Column("USER_NAME_UPDATE")]
        public string? userNameUpdate { get; set; }
        [Column("SERVER_INSERT_DATE")]
        public string? serverInsertDate { get; set; }
        [Column("SERVER_INSERT_TIME")]
        public string? serverInsertTime { get; set; }
        [Column("SERVER_INSERT_TIME_ZONE")]
        public string? serverInsertTimeZone { get; set; }
        [Column("SERVER_UPDATE_DATE")]
        public string? serverUpdateDate { get; set; }
        [Column("SERVER_UPDATE_TIME")]
        public string? serverUpdateTime { get; set; }
        [Column("SERVER_UPDATE_TIME_ZONE")]
        public string? serverUpdateTimeZone { get; set; }
        [Column("CLIENT_INSERT_DATE")]
        public string? clientInsertDate { get; set; }
        [Column("CLIENT_INSERT_TIME")]
        public string? clientInsertTime { get; set; }
        [Column("CLIENT_INSERT_TIME_ZONE")]
        public string? clientInsertTimeZone { get; set; }
        [Column("CLIENT_UPDATE_DATE")]
        public string? clientUpdateDate { get; set; }
        [Column("CLIENT_UPDATE_TIME")]
        public string? clientUpdateTime { get; set; }
        [Column("CLIENT_UPDATE_TIME_ZONE")]
        public string? clientUpdateTimeZone { get; set; }
        [Column("BSTATUS")]
        public bool? bStatus { get; set; }
        [Column("BDELETE")]
        public string? bDelete { get; set; }
        [Column("BMAP")]
        public string? bMap { get; set; }
        [Column("REMARKS_1")]
        public string? Remark1 { get; set; }
        [Column("REMARKS_2")]
        public string? Remark2 { get; set; }
        [Column("REMARKS_3")]
        public string? Remark3 { get; set; }
        [Column("REMARKS_4")]
        public string? Remark4 { get; set; }
        [Column("FLEX_1")]
        public string? Flex1 { get; set; }
        [Column("FLEX_2")]
        public string? Flex2 { get; set; }
        [Column("FLEX_3")]
        public string? Flex3 { get; set; }
        [Column("FLEX_4")]
        public string? Flex4 { get; set; }
        [Column("FLEX_5")]
        public string? Flex5 { get; set; }
        [Column("FLEX_6")]
        public string? Flex6 { get; set; }
        [Column("FLEX_7")]
        public string? Flex7 { get; set; }
        [Column("FLEX_8")]
        public string? Flex8 { get; set; }
        [Column("FLEX_9")]
        public string? Flex9 { get; set; }
        [Column("FLEX_10")]
        public string? Flex10 { get; set; }
        [Column("FLEX_11")]
        public string? Flex11 { get; set; }
        [Column("FLEX_12")]
        public string? Flex12 { get; set; }
        [Column("FLEX_13")]
        public string? Flex13 { get; set; }
        [Column("FLEX_14")]
        public string? Flex14 { get; set; }
        [Column("FLEX_15")]
        public string? Flex15 { get; set; }
        [Column("FLEX_16")]
        public string? Flex16 { get; set; }
       

    }
}
