﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures
{
   public class valueTableInputModel
    {
        
        public string userId { get; set; }
        public string workspaceId { get; set; }
        public string ClientId { get; set; }
        public string valueTable { get; set; }
    }
}
