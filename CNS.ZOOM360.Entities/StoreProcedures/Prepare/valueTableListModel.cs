﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures
{
   public class valueTableListModel
    {
        
        [Column("DISPLAY_NAME")]
        public string displayName { get; set; }
        [Column("OBJECT_NAME")]
        public string objectName { get; set; }
        [Column("USER_NAME")]
        public string userName { get; set; }
        [Column("CREATED_ON")]
        public DateTime createdOn { get; set; }
        [Column("ENABLED")]
        public bool enable { get; set; }
    }
}
