﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.DisplaySettings
{
    public class DisplaySettingsModel
    {
        public string UserId { get; set; }
        public int WorkSpaceId { get; set; }
        public string Client_Id { get; set; }
        public string WorkspaceDisplayMode { get; set; }
        public string WorkspaceLogo { get; set; }
        public string LogoBackgroundColor { get; set; }
        public string WorkspaceTheme { get; set; }
        public string ColorPallete { get; set; }
    }
}
