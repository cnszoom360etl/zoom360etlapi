﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.SourceDescriptionAndConfiguration
{
   public  class sourceCommonModel
    {
        [NotMapped]
        public string userId { get; set; }
        [NotMapped]
        public string workspaceId { get; set; }
        [NotMapped]
        public string clientId { get; set; }
        [NotMapped]
        public string AccountId { get; set; }
        [NotMapped]
        public string connectorId { get; set; }

        [NotMapped]
        public bool connectivitystatus { get; set; }
        [NotMapped]
        public sourceInfoModel SourceInfoModel { get; set; }
    }
}
