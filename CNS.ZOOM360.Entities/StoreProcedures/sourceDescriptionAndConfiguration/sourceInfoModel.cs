﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.SourceDescriptionAndConfiguration 
{
   public class sourceInfoModel
    {
        [NotMapped]
        public string accountName { get; set; }
        [NotMapped]
        public bool enableConnectoin { get; set; }
        [NotMapped]
        public string workspace { get; set; }
        [NotMapped]
        public string email { get; set; }
        [NotMapped]
        public string accountAuthurization { get; set; }
        [NotMapped]
        public bool visibilitymode { get; set; }
        [NotMapped]
        public string commentsection { get; set; }
        [NotMapped]
        public string specialcomments { get; set; }
        [NotMapped]
        public string clientInsertDate { get; set; }
        [NotMapped]
        public string clientInsertTime { get; set; }

        [Column("STATUS_NOTIFY_GRANT")]
        public bool authorizstatus { get; set; }




    }
}
