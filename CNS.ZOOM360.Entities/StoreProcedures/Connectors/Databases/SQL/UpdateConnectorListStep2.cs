using CNS.ZOOM360.Entities.Model.TranformationClassObject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Entities.StoreProcedures.Connectors.Databases.SQL
{
    public class UpdateConnectorListStep2
    {
        [Column("SOURCE_ACCOUNT_ID")]
        public string accountId { get; set; }
        [Column("CONNECTOR_DISPLAY_NAME")]
        public string connectorname { get; set; }
        [Column("ACCOUNT_DISPLAY_NAME")]
        public string accountdisplayname { get; set; }


        [Column("WORKSPACE_NAME")]
        public string workspacename { get; set; }


        [Column("ENABLED_CONNECTION")]
        public string enableconnection { get; set; }
        [Column("EMAIL_ID")]
        public string? Emailid { get; set; }
        [Column("AUTHORIZATION_GRANT")]
        public string authorizationgrant { get; set; }
        [Column("status_notify_grant")]
        public string StatusnotifyGrant { get; set; }
        [Column("FIELD_NAME")]
        public string Filedname { get;set; }
        [Column("FIELD_VALUE")]
        public string Fieldvalue { get;set; }
        [Column("BSTATUS")]
        public string BStatusforvarification { get; set; }
        [Column("VISIBILITY_STATUS")]
        public string visibileconnection { get; set; }
        [Column("COMMENTS")]
        public string comments { get; set; }
        [Column("SPECIAL_COMMENTS")]
        public string spcomments { get; set; }
        [Column("CONNECTIVITY_STATUS")]
        public string connectivitystatus { get; set; }

        
        [Column("OBJECT_NAME")]
        public string objectname { get; set; }

      

        [NotMapped]
        public transform transformed { get; set; }
    [NotMapped]
    public string Hostname { get; set; }
    [NotMapped]
    
    public string Database { get; set; }
    [NotMapped]
    public string Username { get; set; }
    [NotMapped]
    public string Password { get; set; }
    [NotMapped]
    public string Port { get; set; }
  }
  public class ListData
  {
    public int accountid { get; set; }
    public string connectorname { get; set; }
    public int connectorId { get; set; }
    public string displayName { get; set; }
    public string piplineid { get; set; }
    public string accessgranted { get; set; }
     
  }
}
