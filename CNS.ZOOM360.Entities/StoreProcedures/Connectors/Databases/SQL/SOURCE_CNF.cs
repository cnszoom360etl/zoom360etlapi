﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Entities.StoreProcedures.Connectors.Databases.SQL
{
    public class SOURCE_CNF
    {
        public string Hostname { get; set; }
        public string Database { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PortNumber { get; set; }
         




    }
    public class Token
    {
        public string token { get; set; }
         

    }
    public class AccountInfo
    {
        public string userId { get; set; }
        public string workspaceId { get; set; }
        public string clientId { get; set; }
        public string AccountId { get; set; }
        public string connectorId { get; set; }
        public string access_key { get; set; }
        public string expire_time { get; set; }

    }
}
