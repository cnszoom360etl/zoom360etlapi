﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.Connectors.Databases.SQL
{
   public class SourceObjectGetList
    {
        [Column("ACCOUNT_ID")]
        public string accountId { get; set; }
        [Column("CONNECTOR_ID")]
        public string connectorId { get; set; }
        [Column("WORKSPACE_NAME")]
        public string WorkspaceName { get; set; }
        [Column("OBJECT_NAME")]
        public string ObjectName { get; set; }
        [Column("ACCESS_GRANTED")]
        public bool AccessGrant { get; set; }



                
    }
}
