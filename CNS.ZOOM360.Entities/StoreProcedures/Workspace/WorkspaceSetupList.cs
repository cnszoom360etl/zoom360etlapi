﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.Workspace
{
    public class WorkspaceSetupList
    {
        [Column("WORKSPACE_ID")]
        public Int32 WorkspaceId { get; set; }
        [Column("WORKSPACE_NAME")]
        public string WorkspaceName { get; set; }
        [Column("DISPLAY_NAME")]
        public string DisplayName { get; set; }
        [Column("PARENT_WORKSPACE")]
        public string ParentWorkspace { get; set; }
        [Column("CHILD_APPLY_AND_ENFORCE")]
        public string ChildApplyandEnforce { get; set; }
        [Column("EXCLUDE_CHILD_WORKSPACE")]
        public string ExcludeChildWorkspace { get; set; }
        [Column("BSTATUS")]
        public string bStatus { get; set; }
    }
}
