﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.Workspace
{
    public class WorkspaceSetup
    {
        public string userId { get; set; }
        public int workSpaceId { get; set; }
        public string CLIENT_ID { get; set; }
        public string WorkSpace_Name { get; set; }
        public string WorkSpaceDisplayName { get; set; }
        public string WorkSpaceParentName { get; set; }
        public string ChildWorkSpaceRule { get; set; }
        public bool Exclude_Child_WorkSpace { get; set; }
    }
}
