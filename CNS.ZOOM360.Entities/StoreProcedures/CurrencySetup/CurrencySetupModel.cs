﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.CurrencySetup
{
    public class CurrencySetupModel
    {
        public string userId { get; set; }
        public int workSpaceId { get; set; }
        public string CLIENT_ID { get; set; }
        public string currencyType { get; set; }
        public string currenceyTypeSign { get; set; }
        public string currencyImage { get; set; }
        public bool currencyCollectedData { get; set; }
        public bool currencyPrepareData { get; set; }
        public bool currencyPresentingData { get; set; }
        public bool currencyConversion { get; set; }
        public bool exchangeRateAndDataConversion { get; set; }
        public string currencyReportHeaders { get; set; }
        public string currencyVisulization { get; set; }
        public string currencyValue { get; set; }
        public string currencyApplyAndEnforce { get; set; }
    }
}
