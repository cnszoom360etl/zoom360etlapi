﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.IdentityAccessManagment
{
   public class MultiFactorAuthenticationModel
    {
		public string UserId { get; set; }
		public int ClientId { get; set; }
		public int WorkspaceId { get; set; }
		public string SecurityQuestion { get; set; }
		public bool SQFirstTimeLogin { get; set; }
		public bool SQPasswordUpdation { get; set; }
		public string SecurityQestionOption { get; set; }
		public string SecurityQestionAnswer { get; set; }
		public string PasscodeAuthentication { get; set; }
		public bool PAFirsttimeLogin { get; set; }
		public bool PAPasswordUpdation { get; set; }
		public bool PasscodeEmail { get; set; }
		public bool PasscodeSMS { get; set; }
		public bool PasscodeSinglepart { get; set; }
		public bool PasscodeTwopart { get; set; }
		public int PasscodeValidityTime { get; set; }
		public int PasscodeValidityAttempts { get; set; }
		public string CryptographicTokens { get; set; }
		public bool CTFirsttimeLogin { get; set; }
		public bool CTPasswordUpdation { get; set; }
		public string QRCode { get; set; }
		public bool QRFirsttimeLogin { get; set; }
		public bool QRPasswordUpdation { get; set; }
		public string FaceID { get; set; }
		public bool FIFirsttimeLogin { get; set; }
		public bool FIPasswordUpdation { get; set; }
		public string SupervisoryApproval { get; set; }
		public bool SAFirsttimeLogin { get; set; }
		public bool SAPasswordUpdation { get; set; }
		public string AppearanceLogo { get; set; }
		public string AppearanceColor { get; set; }
		public string UsernameInsert { get; set; }
		public string UsernameUpdate { get; set; }
		public string ServerInsertDate { get; set; }
		public string ServerInsertTime { get; set; }
		public string ServerInsertTimeZone { get; set; }
		public string ServerUpdateDate { get; set; }
		public string ServerUpdateTime { get; set; }
		public string ServerUpdateTimeZone { get; set; }
		public string ClientInsertDate { get; set; }
		public string ClientInsertTime { get; set; }
		public string ClientInsertTimeZone { get; set; }
		public string ClientUpdateDate { get; set; }
		public string ClientUpdateTime { get; set; }
		public string ClientUpdateTimeZone { get; set; }
		public string BStatus { get; set; }
		public string BDelete { get; set; }
		public string BMap { get; set; }
		public string Remark1 { get; set; }
		public string Remark2 { get; set; }
		public string Remark3 { get; set; }
		public string Remark4 { get; set; }
		public string Flex1 { get; set; }
		public string Flex2 { get; set; }
		public string Flex3 { get; set; }
		public string Flex4 { get; set; }
		public string Flex5 { get; set; }
		public string Flex6 { get; set; }
		public string Flex7 { get; set; }
		public string Flex8 { get; set; }
		public string Flex9 { get; set; }
		public string Flex10 { get; set; }
		public string Flex11 { get; set; }
		public string Flex12 { get; set; }
		public string Flex13 { get; set; }
		public string Flex14 { get; set; }
		public string Flex15 { get; set; }
		public string Flex16 { get; set; }
	}
}
