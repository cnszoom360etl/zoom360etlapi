﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.IdentityAccessManagment
{
   public class RiskBaseAuthenticationModel
    {
		public string UserId { get; set; }
		public int ClientId { get; set; }
		public int WorkspaceId { get; set; }
		public string RBAActivation { get; set; }
		public string RBAAccess { get; set; }
		public bool SupervisorSummary { get; set; }
		public bool UserRiskSummary { get; set; }
		public string IPSummary { get; set; }
		public string IPVerification { get; set; }
		public bool IPAccess { get; set; }
		public bool MacAccess { get; set; }
		public string MacVerification { get; set; }
		public bool TimeZoneAccess { get; set; }
		public string TimeZoneVerification { get; set; }
		public bool TimeSlotAccess { get; set; }
		public string StartTimeVerification { get; set; }
		public string EndTimeVerification { get; set; }
		public bool DeviceTypeAcces { get; set; }
		public string DeviceTypeVerification { get; set; }
		public bool ConnectionTypeAccess { get; set; }
		public string ConnectionTypeVerification { get; set; }
		public bool SupervisorAccess { get; set; }
		public string SupervisorVerification { get; set; }
		public string AppearanceLogo { get; set; }
		public string AppearanceColor { get; set; }
		public string UsernameInsert { get; set; }
		public string UsernameUpdate { get; set; }
		public string ServerInsertDate { get; set; }
		public string ServerInsertTime { get; set; }
		public string ServerInsertTimeZone { get; set; }
		public string ServerUpdateDate { get; set; }
		public string ServerUpdateTime { get; set; }
		public string ServerUpdateTimeZone { get; set; }
		public string ClientInsertDate { get; set; }
		public string ClientInsertTime { get; set; }

		public string ClientInsertTimeZone { get; set; }
		public string ClientUpdateDate { get; set; }
		public string ClientUpdateTime { get; set; }
		public string ClientUpdateTimeZone { get; set; }
		public string BStatus { get; set; }
		public string BDelete { get; set; }
		public string BMap { get; set; }
		public string Remark1 { get; set; }
		public string Remark2 { get; set; }
		public string Remark3 { get; set; }
		public string Remark4 { get; set; }
		public string Flex1 { get; set; }
		public string Flex2 { get; set; }
		public string Flex3 { get; set; }
		public string Flex4 { get; set; }
		public string Flex5 { get; set; }
		public string Flex6 { get; set; }
		public string Flex7 { get; set; }
		public string Flex8 { get; set; }
		public string Flex9 { get; set; }
		public string Flex10 { get; set; }
		public string Flex11 { get; set; }
		public string Flex12 { get; set; }
		public string Flex13 { get; set; }
		public string Flex14 { get; set; }
		public string Flex15 { get; set; }
		public string Flex16 { get; set; }
	}
}
