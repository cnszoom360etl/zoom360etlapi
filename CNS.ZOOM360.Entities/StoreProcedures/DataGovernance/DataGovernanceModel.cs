﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.DataGovernance
{
    public class DataGovernanceModel
    {
        public string userId { get; set; }
        public int workSpaceId { get; set; }
        public string CLIENT_ID { get; set; }
        public string schemaMode { get; set; }
        public string childWorkspaceInheritance { get; set; }
        public string workspaceShareData { get; set; }
        public string outOffAppWebShareData { get; set; }
        public string outOffApiShareData { get; set; }
        public string rawDataStagging { get; set; }
        public string staggingStorageLocationType { get; set; }
        public int staggingRetentionDays { get; set; }
        public string activeSourceLocation { get; set; }
        public string destinationWorkspace { get; set; }
        public string activeDestinationLocation { get; set; }
        public string passiveDestinationLocation { get; set; }
        public string dataCollectionType { get; set; }
        public bool overrideDataSnapshot { get; set; }
        public string dataStorage { get; set; }
        public string dataDestination { get; set; }
        public bool overrideDataStorage { get; set; }
        public int destinationRetentionDays { get; set; }
    }
}
