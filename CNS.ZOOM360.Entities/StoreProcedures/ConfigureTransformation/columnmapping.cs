﻿using CNS.ZOOM360.Entities.StoreProcedures.EnrichDynamicModel;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.ConfigureTransformation
{
    public class columnmapping
    {
        [BsonElement("source_name")]
        public string source_name { get; set; }
        [BsonElement("target_name")]
        public string target_name { get; set; }
        [BsonElement("field_type")]
        public string field_type { get; set; }
        [BsonElement("visibility")]
        public bool visibility { get; set; }

    }

    public class contentmapping
    {
        [BsonElement("source_column_name")]
        public string source_column_name { get; set; }

        [BsonElement("lookup_table_name")]
        public string lookup_table_name { get; set; }

        [BsonElement("lookup_field_name")]
        public string lookup_field_name { get; set; }


    }

    public class JsonAppledFor_Mapping_Entirichment_Load
    {
        [BsonElement("userId")]
        public string userId { get; set; }
        [BsonElement("workspaceId")]
        public string workspaceId { get; set; }
        [BsonElement("clientId")]
        public string clientId { get; set; }


        [BsonElement("accountId")]
        public string accountId { get; set; }
        [BsonElement("modelName")]
        public string modelName { get; set; }

        [BsonElement("column_mapping")]
        public columnmapping[] column_mapping { get; set; }

        [BsonElement("content_mapping")]
        public contentmapping[] content_mapping { get; set; }

        [BsonElement("transformation")]
        public mongoscripttemplate[] transformation { get; set; }
        [BsonElement("load")]
        public Db_Object_Save_InMongo[] load { get; set; }

    }

     public class process
    {
        public Dictionary<string, object> Load { get; set; }
         

    }
    public class Destination_load
    {
        public string Database { get; set; }
        public string serviceName { get; set; }
        public string host { get; set; }
        public string dbname { get; set; }
        public string user { get; set; }
        public string password { get; set; }
        public string tablename { get; set; }
        public string port { get; set; }
       }




    public class Destination_Json
    {
      
        public string DesConnectorType { get; set; }
        public string DesFilepath { get; set; }
        public string DesFilename { get; set; }
        public string Desaccountdisplayname { get; set; }
        public CreaditionalData Desfieldvalue { get; set; }
        public string Desfiledname { get; set; }
        public string Destablename { get; set; }
         
    }
    public class CreaditionalData
    {
        public string DesdatabaseName { get; set; }
        public string DeshostName { get; set; }
        public string Despassword { get; set; }
        public string Desport { get; set; }
        public string Desusername { get; set; }

         
     
    }

    public class  Db_Object_Save_InMongo
    {
        [BsonElement("Database")]
        public string Database { get; set; }
        [BsonElement("serviceName")]
        public string serviceName { get; set; }
        [BsonElement("host")]
        public string host { get; set; }
        [BsonElement("dbname")]
        public string dbname { get; set; }
        [BsonElement("user")]
        public string user { get; set; }
        [BsonElement("password")]
        public string password { get; set; }
        [BsonElement("tablename")]
        public string tablename { get; set; }
        [BsonElement("port")]
        public string port { get; set; }
        [BsonElement("insert")]
        public string insert { get; set; }

    }


}
