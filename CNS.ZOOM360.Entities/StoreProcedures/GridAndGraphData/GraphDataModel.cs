﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.GridAndGraphData
{
    public class GraphDataModel
    {
        [Column("PLATFORM")]
        public string Platform            { get; set; }
        [Column("PERIOD_DATE")]
        public Int64 Period_Date         { get; set; }
        [Column("PERIOD_DISPLAY_DATE")]
        public string Period_Display_Date { get; set; }
        [Column("IMPRESSIONS")]
        public Int64 Imperssions        { get; set; }
        [Column("CLICKS")]
        public Int64 Clicks             { get; set; }
        [Column("CTR")]
        public Int64 CTR                { get; set; }
        [Column("CONNECTOR_IMAGE")]
        public string Connectorimage      { get; set; }
    }
}
