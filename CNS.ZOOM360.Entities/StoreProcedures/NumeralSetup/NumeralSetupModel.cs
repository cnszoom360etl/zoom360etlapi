﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.NumeralSetup
{
    public class NumeralSetupModel
    {
        public string UserId { get; set; }
        public int WorkSpaceId { get; set; }
        public string Client_Id { get; set; }
        public string NumberingSystemFormat { get; set; }
        public string NumberSignType { get; set; }
        public string SignFormat { get; set; }
        public string PositiveNumbeColorCode { get; set; }
        public string NegitiveNumberColorCode { get; set; }
        public string NumberConversion { get; set; }
        public string NumberValueConversion { get; set; }
        public string NumberValue { get; set; }
        public int SelectiveDecimalPlaces { get; set; }
        public bool FullDecimalPlaces { get; set; }
        public string RoundOffNumbers { get; set; }
        public int SelectiveRoundOffPlace { get; set; }
        public string NumberApplyAndEnforce { get; set; }
    }
}
