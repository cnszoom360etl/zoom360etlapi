﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.EnrichDynamicModel
{
   public class ETLStatus
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        [BsonElement("Extract")]
        public Int32 Extract { get; set; }
        [BsonElement("Load")]
        public Int32 Load { get; set; }
        [BsonElement("Transform")]
        public Int32 Transform { get; set; }
    }
}
