﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.Transformation
{
   public class functionPerametersValueModel
    {
        public string userId { get; set; }
        public string workspaceId { get; set; }
        public string clientID { get; set; }
        public string functionGroupId { get; set; }
        public string functionId { get; set; }
         
    }
}
