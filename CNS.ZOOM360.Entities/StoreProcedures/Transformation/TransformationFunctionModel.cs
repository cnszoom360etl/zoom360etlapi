﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.Transformation
{
   public class TransformationFunctionModel
    {

        [Column("GROUP_ID")]
        public string groupId { get; set; }
        [Column("FUNCTION_GROUP")]
        public string functionGroup { get; set; }
        [Column("FUNCTION_ID")]
        public string functionId { get; set; }
        [Column("FUNCTION_NAME")]
        public string functionName { get; set; }
        [Column("FUNCTION_PARAMETER")]
        public string functionPerameter { get; set; }
        [Column("PARAMETER_VALUE")]
        public string  PerameterValues { get; set; }
    }
}
