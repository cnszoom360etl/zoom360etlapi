﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.QuotaSettings
{
    public class QuotaSettingsModel
    {
        public string UserId { get; set; }
        public int WorkSpaceId { get; set; }
        public string Client_Id { get; set; }
        public string QuotaLimit { get; set; }
        public string QuotaType { get; set; }
        public string QuotaUsageCycle { get; set; }
        public string QuotaStartDate { get; set; }
        public string QuotaEndDate { get; set; }
    }
}
