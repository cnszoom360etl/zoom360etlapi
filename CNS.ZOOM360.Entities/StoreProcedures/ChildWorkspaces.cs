﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures
{
    public class ChildWorkspaces
    {
        public string UserId { get; set; }
        public int WorkspaceId { get; set; }
        public string ClientId { get; set; }
        public string ChildWorkspace { get; set; }
        public bool ChildSelectedOption { get; set; }
        public bool ChildChange { get; set; }
        public bool ChildOverrideSelectedOption { get; set; }
    }
}
