﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.StoreProcedures.CalenderSetup
{
    public class CalenderSetupModel 
    {
        public string UserId { get; set; }
        public int WorkspaceId { get; set; }
        public string ClientId { get; set; }
        public string CalenderSetup { get; set; }
        public string BussinessYearDate { get; set; }
        public string FinacialYearDate { get; set; }
        public string ReportingYearDate { get; set; }
        public string WeekStartDay { get; set; }
        public string AnnualHolidayCalender { get; set; }
        public string AnnualCampaignCalender { get; set; }
        public bool NotifyCampaignsCalender { get; set; }
        public string MilestoneAnnualHolidayCalender { get; set; }
        public bool NotifyMilestoneCalender { get; set; }
        public string CalenderApplyAndEnforce { get; set; }
    }
}
