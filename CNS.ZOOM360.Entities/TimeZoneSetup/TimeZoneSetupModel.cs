﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CNS.ZOOM360.Entities.TimeZoneSetup
{
    public class TimeZoneSetupModel
    {
        public string UserId { get; set; }
        public int WorkSpaceId { get; set; }
        public string Client_Id { get; set; }
        public string DataFormatType { get; set; }
        public string DateFormat { get; set; }
        public string ClockImage { get; set; }
        public bool DateCollectedData { get; set; }
        public bool DatePreparingData { get; set; }
        public bool DatePresentingData { get; set; }
        public bool DateConversion { get; set; }
        public bool DateConversionValue { get; set; }
        public string TimeZone { get; set; }
        public string TimeZoneType { get; set; }
        public string DateFormatReports { get; set; }
        public string ReportsDate { get; set; }
        public string DateFormatVisulization { get; set; }
        public string VisulizationDate { get; set; }
        public string TimeFormatReports { get; set; }
        public string ReportTime { get; set; }
        public string TimeFormatVisualization { get; set; }
        public string? VisualizationTime { get; set; } 
        public string ApplyAndEnforceDatetime { get; set; }
    }
}
