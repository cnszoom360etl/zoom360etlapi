using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.LoggerServices;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
 
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Text;
using Microsoft.VisualBasic.FileIO;
using System.Net.Http;
using System.Net;
using CNS.ZOOM360.Services.StoreProcedures.ConnectTOShareFolderDirectory;

namespace CNS.ZOOM360.Host.CSVArchive
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.CSVHelper.RouteName)]
    [ApiController]
    public class CSVHelperController : ControllerBase
    {
        //private readonly ICSVhelperService _ICSVhelperService;
        private readonly ILoggerManager _logger;
        private readonly ConnectToSharedFolder share; 
        public CSVHelperController(ILoggerManager logger, ConnectToSharedFolder _share)
        {
            //_ICSVhelperService = ICSVhelperService;
            _logger = logger;
            share = _share;
        }
        [Route(ServiceConstants.CSVHelper.GetCSVFileData)]
        [HttpGet]
        public async Task<IActionResult> getCSVDataToJson(string name)
        {
            /// file download from server and save in local folder 
            /// #Start section
           // var remoteurl = @"//Bd-dev-vm2/d/zoom_files_dump/" + name;
            var remoteurl = @"//192.168.223.100/d/zoom_files_dump/"+name;
            //var destinationurl = @"E:\Zoom360MargProject\Project\CNS.ZOOOM360.API\cnszoom360\zoom360Api\CNS.ZOOM360.Host\CSVArchive\" + name;
            //WebClient webClient = new WebClient();
            //webClient.DownloadFile(remoteurl, destinationurl);
            ///# end section 
               
            

          //  var lines = System.IO.File.ReadAllLines(@"E:\Zoom360MargProject\Project\CNS.ZOOOM360.API\cnszoom360\zoom360Api\CNS.ZOOM360.Host\CSVArchive\"+name);
           var lines = System.IO.File.ReadAllLines(remoteurl);

        var csv = new List<string[]>();

            foreach (var line in lines)
            {
                csv.Add(line.Split(','));
            }
            var properties = lines[0].Split(',');

            var listObjResult = new List<Dictionary<string, string>>();

            for (int i = 1; i < lines.Length; i++)
            {
                var objResult = new Dictionary<string, string>();
                for (int j = 0; j < properties.Length; j++)
                    objResult.Add(properties[j], csv[i][j]);

                listObjResult.Add(objResult);
            }

            return Ok(JsonConvert.SerializeObject(listObjResult));

        }

        [Route(ServiceConstants.CSVHelper.getCSVExtractFileDataToJson)]
        [HttpGet]
        public async Task<IActionResult> getCSVExtractFileDataToJson(string name)
        {
            /// file download from server and save in local folder 
            /// #Start section
            /// //Bd-dev-vm2/d/zoom_files_dump/
            var remoteurl = @"//192.168.223.100/d/zoom_files_dump/extract/"+name;
           // var remoteurl = @"//Bd-dev-vm2/d/zoom_files_dump/extract/" + name;

            //var destinationurl = @"E:\Zoom360MargProject\Project\CNS.ZOOOM360.API\cnszoom360\zoom360Api\CNS.ZOOM360.Host\CSVArchive\" + name;
            //WebClient webClient = new WebClient();
            // webClient.DownloadFile(remoteurl, destinationurl);
            //var lines = System.IO.File.ReadAllLines(@"E:\Zoom360MargProject\Project\CNS.ZOOOM360.API\cnszoom360\zoom360Api\CNS.ZOOM360.Host\CSVArchive\30-06-2020 Trial Balance.xls");
            var lines = System.IO.File.ReadAllLines(remoteurl);
            try
            {
                var csvDataRead = new List<string[]>();
                foreach (var line in lines)
                {
                    csvDataRead.Add(line.Split(','));
                }
                var propertiesOfFiles = lines[0].Split(',');

                var listFileObjResult = new List<Dictionary<string, string>>();

                for (int i = 1; i < lines.Length; i++)
                {
                    var objResultFile = new Dictionary<string, string>();
                    for (int j = 0; j < propertiesOfFiles.Length; j++)
                        objResultFile.Add(propertiesOfFiles[j], csvDataRead[i][j]);

                    listFileObjResult.Add(objResultFile);
                }

                return Ok(JsonConvert.SerializeObject(listFileObjResult));
            }
            catch (IOException e )
            {

                 
                return Ok(e.ToString());
            }
            

        }




        [Route(ServiceConstants.CSVHelper.SaveJsonDataToCSV)]
        [HttpPost]
        public async Task<IActionResult> JsonStringToCSV(string data)
        {
            XmlNode xml = JsonConvert.DeserializeXmlNode("{records:{record:" + data + "}}");
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml.InnerXml);
            XmlReader xmlReader = new XmlNodeReader(xml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(xmlReader);
            var dataTable = dataSet.Tables[0];

            //Datatable to CSV
            var lines = new List<string>();
            string[] columnNames = dataTable.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName).
                                              ToArray();
            var header = string.Join(",", columnNames);
            lines.Add(header);
            var valueLines = dataTable.AsEnumerable()
                               .Select(row => string.Join(",", row.ItemArray));
            lines.AddRange(valueLines);
            System.IO.File.WriteAllLines(@"D:\FrameWorks\Office\Shaan\Project\CNS.ZOOOM360.API\cnszoom360\zoom360Api\CNS.ZOOM360.Host\CSVArchive\_cam1_1509438801455_000004.csv", lines);
            return Ok("Success");
        }
    }
}
