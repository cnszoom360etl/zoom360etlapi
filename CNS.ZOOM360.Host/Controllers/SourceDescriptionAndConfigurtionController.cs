﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.LoggerServices;
using Microsoft.AspNetCore.Mvc;

using CNS.ZOOM360.Shared.StoreProcedures.SourceDescriptionAndConfiguration;
using CNS.ZOOM360.Entities.StoreProcedures.SourceDescriptionAndConfiguration;
using CNS.ZOOM360.Entities.StoreProcedures.Connectors.Databases.SQL;
using CNS.ZOOM360.Entities.StoreProcedures.sourceDescriptionAndConfiguration;
using System.Reflection;
using System.IO;
using System.Net.Http.Headers;
using System.Net;
using System.Net.Mail;
using System.Security.Authentication;
using Google.Apis.Drive.v3;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Services;
using Microsoft.AspNetCore.Hosting;
using CNS.ZOOM360.Entities.Model;
using Google.Apis.Download;
using Microsoft.AspNetCore.Http;
using CNS.ZOOM360.Services.StoreProcedures.OneDrive;
using Google.Apis.Sheets.v4;
using CNS.ZOOM360.Services.StoreProcedures.MongoDB;
using CNS.ZOOM360.Entities.MongoDBModels;
using MongoDB.Bson;

using CNS.ZOOM360.Entities.Model.TranformationClassObject;
using Newtonsoft.Json;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Google.Apis.Auth.AspNetCore3;
using Microsoft.AspNetCore.Http.Features;

namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.SourceDescAndConfiration.RouteName)]
    [ApiController]

    public class SourceDescriptionAndConfigurtionController : ControllerBase
    {
        private readonly ISourceDescriptionAndConfiguration _sourceConfiguration;
        //private readonly MongoDBService _MongoServices;
        private readonly ILoggerManager _logger;
        private IWebHostEnvironment _hostingEnvironment;
        private IHostingEnvironment hostingEnv;
        




        public SourceDescriptionAndConfigurtionController(ISourceDescriptionAndConfiguration sourceDescandConfiguration, ILoggerManager logger, IWebHostEnvironment environment , IHostingEnvironment env)
        {
            _sourceConfiguration = sourceDescandConfiguration;
            _logger = logger;
            this.hostingEnv = env;
            //_MongoServices = MongoServices;
            _hostingEnvironment = environment;







        }
        [Route(ServiceConstants.SourceDescAndConfiration.saveDescriptionInfo)]
        [HttpPost]
        public async Task<IActionResult> saveDescriptionInfo(sourceCommonModel InputModel)
        {
            var sourceVmessageData = _sourceConfiguration.SaveSourceDescriptionAndConfiguration(InputModel);
            if (sourceVmessageData.Result == null)
            {
                _logger.LogError("");
                return NotFound(sourceVmessageData);
            }

            return Ok(sourceVmessageData.Result);
        }

        [Route(ServiceConstants.SourceDescAndConfiration.saveDbCredentialInfo)]
        [HttpPost]
        public async Task<IActionResult> saveDbCredentialInfo(DBCredentialSourceDescriptonAndConfigModel DbAccount)
        {
            string msg = "";
            Type myClassType = DbAccount.sOURCE_CNF.GetType();
            PropertyInfo[] properties = myClassType.GetProperties();
            SourceAccountConnectionModel accountmodel = new SourceAccountConnectionModel();
            accountmodel.UserId = DbAccount.SourceCommonModel.userId;
            accountmodel.WorkspaceId = DbAccount.SourceCommonModel.workspaceId;
            accountmodel.ClientId = DbAccount.SourceCommonModel.clientId;
            accountmodel.Connector_ID = DbAccount.SourceCommonModel.connectorId;
            accountmodel.Account_Id = DbAccount.SourceCommonModel.AccountId;
            accountmodel.connectivitystatus = DbAccount.SourceCommonModel.connectivitystatus;
            foreach (PropertyInfo property in properties)
            {
                //property.Name 
                accountmodel.HostName = property.Name;
                accountmodel.FieldValue = property.GetValue(DbAccount.sOURCE_CNF, null);
                var SaveCredential = _sourceConfiguration.SaveDBCredentials(accountmodel);
                msg = SaveCredential.Result;
            }
            if (msg == null)
            {
                _logger.LogError("");
                return NotFound(msg);
            }

            return Ok(msg);
        }
        [Route(ServiceConstants.SourceDescAndConfiration.DriveFiles)]
        [HttpGet]
        [GoogleScopedAuthorize(DriveService.ScopeConstants.DriveReadonly)]

        public async Task<IActionResult> DriveFileList([FromServices] IGoogleAuthProvider auth)
        {
            List<string> driveFileList = new List<string>();
            GoogleCredential cred = await auth.GetCredentialAsync();
            var service = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = cred
            });
            var files = await service.Files.List().ExecuteAsync();
            driveFileList = files.Files.Select(x => x.Name).ToList();
            return Ok();
        }
        [Route(ServiceConstants.SourceDescAndConfiration.tokeninfo)]
        [HttpPost]
        public async Task<IActionResult> tokeninfo(TokenInfo DbAccount)
        {
            string msg = "";
            Type myClassType = DbAccount.sOURCE_CNF.GetType(); 
            PropertyInfo[] properties = myClassType.GetProperties();
            SourceAccountConnectionModel accountmodel = new SourceAccountConnectionModel();
            accountmodel.UserId = DbAccount.SourceCommonModel.userId;
            accountmodel.WorkspaceId = DbAccount.SourceCommonModel.workspaceId;
            accountmodel.ClientId = DbAccount.SourceCommonModel.clientId;
            accountmodel.Connector_ID = DbAccount.SourceCommonModel.connectorId;
            accountmodel.Account_Id = DbAccount.SourceCommonModel.AccountId;
            accountmodel.connectivitystatus =Convert.ToBoolean(DbAccount.SourceCommonModel.access_key);
            accountmodel.tokenExpireTime = DbAccount.SourceCommonModel.expire_time;
            foreach (PropertyInfo property in properties)
            {
                //property.Name 
                accountmodel.HostName = property.Name;
                accountmodel.FieldValue = property.GetValue(DbAccount.sOURCE_CNF, null);
                var SaveCredential = _sourceConfiguration.SaveTokenInfo(accountmodel);
                msg = SaveCredential.Result;
            }
            if (msg == null)
            {
                _logger.LogError("");
                return NotFound(msg);
            }

            return Ok(msg);
        }




        /// <summary>
        /// File Upload on server and database using Html input Tage
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="WORKSPACEID"></param>
        /// <param name="CLIENTID"></param>
        /// <param name="ConnectorId"></param>
        /// <param name="AccountId"></param>
        /// <returns></returns>

        [Route(ServiceConstants.SourceDescAndConfiration.savefile)]
        [HttpPost, DisableRequestSizeLimit]
        public IActionResult savefile(string UserId, string WORKSPACEID, string CLIENTID, string ConnectorId, string AccountId)
        {
            SourceAccountConnectionModel accountmodel = new SourceAccountConnectionModel();
            try
            {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine("Resources", "files");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                if (file.Length > 0)
                {

                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                        var filereading = file.OpenReadStream();
                    }
                    var fileserversavepath = @"//192.168.223.100/zoom_files_dump/" + file.FileName;

                    using (var stream = new FileStream(fileserversavepath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                        var filereading = file.OpenReadStream();
                    }
                    accountmodel.UserId = UserId;
                    accountmodel.ClientId = CLIENTID;
                    accountmodel.Account_Id = AccountId;
                    accountmodel.Connector_ID = ConnectorId;
                    accountmodel.WorkspaceId = WORKSPACEID;
                    accountmodel.HostName = fileName;
                    accountmodel.FieldValue = fullPath;
                    var list = _sourceConfiguration.SaveFileInfo(accountmodel);
                    return Ok(new { list });
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }


        [Route(ServiceConstants.SourceDescAndConfiration.saveSocialMedia)]
        [HttpPost]
        public async Task<IActionResult> saveSocialMedia(SocialMediaModel socialmedia)
        {
           
           var saveSocalMedia = _sourceConfiguration.SaveSocialMediaInfo(socialmedia);
            if (saveSocalMedia== null)
            {
                _logger.LogError("");
                return NotFound(saveSocalMedia);
            }
            try
            {

                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("med360@cns-e.com");
                message.To.Add(new MailAddress(socialmedia.Email));
                message.Subject = "Test";
                message.IsBodyHtml = true;
                //message.Body = "test";
                // var returnpath = "http://localhost:4200/Success/id=" + socialmedia.SourceCommonModel.AccountId;
                message.Body = "Please Authorize this email"+ "<br><br><button><a href='https://www.facebook.com/v9.0/dialog/oauth?client_id=261213182411450&redirect_uri=http://localhost:4200/Success/" + socialmedia.SourceCommonModel.AccountId + "&state=0' value='" + socialmedia.Email + "' style='text-decoration:none;color:rgb(33, 155, 156);'>Authorize</a></button> ";
                //message.Body = "Authorize this email"+ "<br><br><button><a href='https://www.facebook.com/v9.0/dialog/oauth?client_id=763254611045170&redirect_uri=http://192.168.223.111:9095/auth/login&state=0' value='" + socialmedia.Email + "' style='text-decoration:none;color:rgb(33, 155, 156);'>Authorize</a></button> ";

                smtp.Port = 587;
                smtp.EnableSsl = true;
                //smtp.Host = "smtp.gmail.com";
                smtp.Host = "mail.cns-e.com";
                smtp.UseDefaultCredentials = false;
                //smtp.Credentials = new NetworkCredential("cnszoom360@gmail.com", "cnse@12345");
                smtp.Credentials = new NetworkCredential("med360@cns-e.com", "med360@cns");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return Ok(saveSocalMedia);
            }
            return Ok(saveSocalMedia);
        }
        //Google Sheet Services

        public static string[] sheetscop = { SheetsService.Scope.Spreadsheets };

        private static SheetsService GetSheetServices()
        {

            UserCredential credential;

            using (var stream = new FileStream(@"E:\client_secret.json", FileMode.Open, FileAccess.Read))
            {
                string FilePath = Path.Combine("Assests", "DriveServiceCredential.json");
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    sheetscop,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(FilePath, true)).Result;
            }

            SheetsService service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Zoom360",
            });
            return service;
        }




        //Google Drive
        public static string[] Scopes = { DriveService.Scope.Drive };
        private static DriveService GetService()
        {

            UserCredential credential;

            using (var stream = new FileStream(@"E:\client_secret.json", FileMode.Open, FileAccess.Read))
            {
                string FilePath = Path.Combine("Assests", "DriveServiceCredential.json");
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(FilePath, true)).Result;
            }

            DriveService service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Zoom360",
            });
            return service;
        }



        [Route(ServiceConstants.SourceDescAndConfiration.UploadFIle)]

        [HttpPost, DisableRequestSizeLimit]
        public IActionResult FileUpload(string UserId, string WORKSPACEID, string CLIENTID, string ConnectorId, string AccountId)

        {
            try
            {
                SourceAccountConnectionModel accountmodel = new SourceAccountConnectionModel();

                var file = Request.Form.Files[0];
                if (file != null && file.Length > 0)
                {
                    DriveService service = GetService();

                    string webRootPath = _hostingEnvironment.ContentRootPath;

                    var folderName = Path.Combine("Resources", "files");

                    //var fileserversavepath = Path.Combine(folderName, file.FileName);
                    var fileserversavepath = @"//192.168.223.100/d/zoom_files_dump/" + file.FileName;

                    using (var stream = new FileStream(fileserversavepath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                        var filereading = file.OpenReadStream();
                    }
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var path = Path.Combine(folderName, fileName);
                    var FileMetaData = new Google.Apis.Drive.v3.Data.File();
                    FileMetaData.Name = Path.GetFileName(file.FileName);
                    FileMetaData.MimeType = file.ContentType;
                    FilesResource.CreateMediaUpload request;


                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                        var filereading = file.OpenReadStream();
                    }
                    accountmodel.UserId = UserId;
                    accountmodel.ClientId = CLIENTID;
                    accountmodel.Account_Id = AccountId;
                    accountmodel.Connector_ID = ConnectorId;
                    accountmodel.WorkspaceId = WORKSPACEID;
                    accountmodel.HostName = fileName;
                    accountmodel.FieldValue = fullPath;
                    var list = _sourceConfiguration.SaveFileInfo(accountmodel);
                     

                    using (var stream = new FileStream(path, FileMode.Open))
                    {
                        request = service.Files.Create(FileMetaData, stream, FileMetaData.MimeType);
                        request.Fields = "id";
                        request.Upload();
                    }
                    return Ok(new { list });
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception)
            {

                throw;
            }

        }
        [Route(ServiceConstants.SourceDescAndConfiration.GetFileList)]
        [HttpGet]
        public List<GoogleDriveFiles> GetFileList()
        {
            DriveService service = GetService();

            // Define parameters of request.
            FilesResource.ListRequest FileListRequest = service.Files.List();
             

            //FilesResource.ListRequest FileListReques = service.Drives.G("*.csv$");




            //listRequest.PageSize = 10;
            //listRequest.PageToken = 10;
            FileListRequest.Fields = "nextPageToken, files(id, name, size, version, trashed, createdTime, mimeType)";

            // List files.
            IList<Google.Apis.Drive.v3.Data.File> files = FileListRequest.Execute().Files;
             

            List<GoogleDriveFiles> FileList = new List<GoogleDriveFiles>();

            if (files != null && files.Count > 0 )
            {
                foreach (var file in files)
                {
                    string fileextention = Path.GetExtension(file.Name);
                     
                    if (fileextention == ".csv" || fileextention == ".xlsx")
                    {
                        GoogleDriveFiles File = new GoogleDriveFiles
                        {
                            Id = file.Id,
                            Name = file.Name,
                            Size = file.Size,
                            Version = file.Version,
                            CreatedTime = file.CreatedTime
                        };
                        FileList.Add(File);
                    }
                    
                }
            }
            return FileList;
        }


        /// <summary>
        /// Get Sheets File
        /// </summary>
        /// <param name="id"></param>
        /// 
        //[Route(ServiceConstants.SourceDescAndConfiration.Getsheetlist)]
        //[HttpGet]
        //public List<SheetsService> Getsheetlist()
        //{
        //    SheetsService service = GetSheetServices();

        //    // Define parameters of request.
        //    SpreadsheetsResource.ValuesResource.GetRequest request = service.Spreadsheets.Values;

        //    //listRequest.PageSize = 10;
        //    //listRequest.PageToken = 10;
        //    //FileListRequest.Fields = "nextPageToken, files(id, name, size, version, trashed, createdTime)";

        //    // List files.
        //    IList<Google.Apis.Drive.v3.Data.File> files = FileListRequest.Execute().Files;
        //    List<SheetsService> FileList = new List<SheetsService>();

        //    if (files != null && files.Count > 0)
        //    {
        //        foreach (var file in files)
        //        {
        //            GoogleDriveFiles File = new GoogleDriveFiles
        //            {
        //                Id = file.Id,
        //                Name = file.Name,
        //                Size = file.Size,
        //                Version = file.Version,
        //                CreatedTime = file.CreatedTime
        //            };
        //            FileList.Add(File);
        //        }
        //    }
        //    return FileList;
        //}

        [Route(ServiceConstants.SourceDescAndConfiration.DownloadFile)]
        [HttpGet]
        public void DownloadFile(string id)
        {
            string FilePath = DownloadGoogleFile(id);

            string FolderPath = Path.Combine("Resources", "files");
            //var FolderPath1 = Path.Combine(Directory.GetCurrentDirectory(), FolderPath) + Path.GetFileName(FilePath);
            var FolderPath1 = Path.GetFileName(FilePath);
             var bytes = System.Text.Encoding.UTF8.GetBytes(FolderPath1);
            Response.ContentType = "application/zip";

            Response.Headers.Add("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
           // Response.Body.Read(bytes);
            Response.Body.Flush();
        }

        

        private string DownloadGoogleFile(string fileId)
        {
            DriveService service = GetService();
            string serverpath = _hostingEnvironment.ContentRootPath;
            //string FolderPath = System.Web.HttpContext.Current.Server.MapPath("/GoogleDriveFiles/");
            string FolderPath = Path.Combine("Resources", "files");
            var FolderPath1 = Path.Combine(Directory.GetCurrentDirectory(), FolderPath);

            FilesResource.GetRequest request = service.Files.Get(fileId);

            string FileName = request.Execute().Name;
            string fileserversavepath = @"//192.168.223.100/d/zoom_files_dump/";
            //string FilePath = System.IO.Path.Combine(FolderPath1, FileName);
            string FilePath = System.IO.Path.Combine(fileserversavepath, FileName);

            MemoryStream stream1 = new MemoryStream();

            request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
            {
                switch (progress.Status)
                {
                    case DownloadStatus.Downloading:
                        {
                            //Console.WriteLine(progress.BytesDownloaded);
                            break;
                        }
                    case DownloadStatus.Completed:
                        {
                            //Console.WriteLine("Download complete.");
                            SaveStream(stream1, FilePath);
                            break;
                        }
                    case DownloadStatus.Failed:
                        {
                            //Console.WriteLine("Download failed.");
                            break;
                        }
                }
            };
            request.Download(stream1);
            return FilePath;
        }

        private void SaveStream(MemoryStream stream, string FilePath)
        {
            using (System.IO.FileStream file = new FileStream(FilePath, FileMode.Create, FileAccess.ReadWrite))
            {
                stream.WriteTo(file);
            }
        }

        public static string[] Scope = { DriveService.Scope.Drive };

        

        [Route(ServiceConstants.SourceDescAndConfiration.uploadOneDrive)]

        [HttpPost, DisableRequestSizeLimit]
        public async Task<Microsoft.Graph.DriveItem> FileUploadonedrive(IFormFile File)
        {
            GraphAuthProvider graph = new GraphAuthProvider();
            var file = Request.Form.Files[0];
            IFormFile fileToUpload = file;
            Stream ms = new MemoryStream();
            using (ms = new MemoryStream()) //this keeps the stream open
            {
                fileToUpload.CopyToAsync(ms);
                ms.Seek(0, SeekOrigin.Begin);
                var buf2 = new byte[ms.Length];
                ms.Read(buf2, 0, buf2.Length);
                Microsoft.Graph.DriveItem uploadedFile = null;

                ms.Position = 0;
                string tenantId = "f8cdef31-a31e-4b4a-93e4-5f571e91255a";
                string ClientId = "a2920504-b334-48b2-97b4-49cff376622e";
                string SecretId = "B1So-gt~E~pO7c6r34og3l~OZAy-diaim9";
                Microsoft.Graph.GraphServiceClient _graphServiceClient = await graph.AuthenticateViaAppIdAndSecret(tenantId, ClientId, SecretId);

                try
                {
                    uploadedFile = await _graphServiceClient
                   //.Users["cnszoom360@gmail.com"]
                   .Me
                   .Drive
                   .Root
                   .ItemWithPath(fileToUpload.FileName)
                   .Content.Request()
                   .PutAsync<Microsoft.Graph.DriveItem>(ms);

                }
                catch (Exception ex)
                {

                    Console.Write(ex);
                }



                ms.Dispose(); //clears memory
                return uploadedFile; //returns a DriveItem. 
            }

        }
        [Route(ServiceConstants.SourceDescAndConfiration.Get90DaysDataListFromMong)]
        [HttpGet]
        public void Get90DaysDataListFromMong()
        {
            // FacebookOAuthClient oAuthClient = new FacebookOAuthClient();
            //oAuthClient.AppId = "...";
            //oAuthClient.AppSecret = "...";
            //oAuthClient.RedirectUri = new Uri("https://.../....aspx");

            //dynamic tokenResult = oAuthClient.ExchangeCodeForAccessToken(code);
            //return tokenResult.access_token;
            //var fb = new FacebookClient();
            //dynamic result = fb.Get("oauth/access_token", new
            //{
            //    client_id = "411387786868121",
            //    client_secret = "63e3267e5f4be512b2ec585f89b0c52d",
            //    redirect_uri = "http://localhost:",
            //    code = code
            //});
            //return result;







            //var rez = Task.Run(async () =>
            //{
            //    string url = "https://graph.facebook.com/oauth/access_token?client_id=411387786868121&client_secret=63e3267e5f4be512b2ec585f89b0c52d&grant_type=client_credentials";

            //    using (var http = new HttpClient())
            //    {
            //        var httpResponse = await http.GetAsync(url);
            //        var httpContent = await httpResponse.Content.ReadAsStringAsync();

            //        return httpContent;
            //    }
            //});
            //var rezJson = JObject.Parse(rez.Result);
            //string urls = "https://graph.facebook.com/411387786868121?fields=access_token&access_token=411387786868121|QoiOuu5umVUbgetmV_UlvfyBXeo";
            //Console.WriteLine(rezJson);








             

        }
        [Route(ServiceConstants.SourceDescAndConfiration.uploadfile)]
       //[HttpPost, DisableRequestSizeLimit]
        [AcceptVerbs("Post")]
        [RequestFormLimits(MultipartBodyLengthLimit = 409715200)]
        [RequestSizeLimit(409715200)]
        public IActionResult uploadfile()
        {
            
            try
            {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine("Resources", "files");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                if (file.Length > 0)
                {

                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                        var filereading = file.OpenReadStream();
                    }
                    var fileserversavepath = @"//192.168.223.100/zoom_files_dump/" + file.FileName;

                    using (var stream = new FileStream(fileserversavepath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                        var filereading = file.OpenReadStream();
                    }
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }


        /// <summary>
        ///   This Method is Work with syncfusion File uploader Control 
        /// </summary>
        /// <param name="UploadFiles"></param>
        /// <returns></returns>
        [Route(ServiceConstants.SourceDescAndConfiration.FilesUploader)]

        //[AcceptVerbs("Post")]
        [HttpPost()]
        [RequestFormLimits(MultipartBodyLengthLimit = 999999999)]
        [RequestSizeLimit(999999999)]
        public IActionResult FilesUploader(IList<IFormFile> UploadFiles)
        {
            SourceAccountConnectionModel accountmodel = new SourceAccountConnectionModel();
            List<string> FilesNameList =new  List<string>();
          
            string filename = string.Empty;
            string filepath = string.Empty;
            string fileName = string.Empty;
            string UserId = HttpContext.Request.Form["UserId"];
            string WORKSPACEID = HttpContext.Request.Form["WORKSPACEID"];
            string CLIENTID = HttpContext.Request.Form["CLIENTID"];
            string ConnectorId = HttpContext.Request.Form["ConnectorId"];
            string AccountId = HttpContext.Request.Form["AccountId"];
            int count =Convert.ToInt32(HttpContext.Request.Form["selectedfileCount"]);
            string FileName = HttpContext.Request.Form["filename"];
            try
            {
                foreach (var file in UploadFiles)
                {
                    if (UploadFiles != null)
                    {
                        // Save in System Directory 
                       fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                        

                        FilesNameList.Add(filename);

                        filename = hostingEnv.WebRootPath + $@"\{fileName}";
                        if (!System.IO.File.Exists(filename))
                        {
                            filepath = @"//192.168.223.100/d/zoom_files_dump/";
                            var fileserversavepath = @"//192.168.223.100/d/zoom_files_dump/" + file.FileName;

                            using (var stream = new FileStream(fileserversavepath, FileMode.Create))
                            {
                                file.CopyTo(stream);
                                var filereading = file.OpenReadStream();
                            }
                        }

                        else
                        {
                            Response.Clear();
                            Response.StatusCode = 204;
                            Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File already exists.";
                        }
                    }
                    
                }
                accountmodel.UserId = UserId;
                accountmodel.ClientId = CLIENTID;
                accountmodel.Account_Id = AccountId;
                accountmodel.Connector_ID = ConnectorId;
                accountmodel.WorkspaceId = WORKSPACEID;
                accountmodel.HostName = fileName;
                accountmodel.FieldValue = filepath;
                var list = _sourceConfiguration.SaveFileInfo(accountmodel);
                return Ok(new { list });



            }
            catch (Exception e)
            {
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.StatusCode = 204;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "No Content";
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
            }
            return Content("");
        }



    }
}