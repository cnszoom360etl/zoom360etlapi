﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.LoggerServices;
using CNS.ZOOM360.Shared.StoreProcedures.Common;
using CNS.ZOOM360.Shared.StoreProcedures.Common.Dto;
using CNS.ZOOM360.Shared.StoreProcedures.Workspace.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.CommonDropdown.RouteName)]
    [ApiController]
    public class CommonDropdownListController : ControllerBase
    {
        private readonly ICommonDropdownListService _commonDropdownListService;
        private readonly ILoggerManager _logger;
       public CommonDropdownListController(ICommonDropdownListService commonDropdownListService, ILoggerManager logger) {

            _commonDropdownListService = commonDropdownListService;
            _logger = logger;
        }

        [Route(ServiceConstants.CommonDropdown.GetDropdownList)]
        [HttpGet]
        public async Task<IActionResult> GetDropdownList(string userId, string dropdownName)
        {
            var worksapceResult = await _commonDropdownListService.GetDropDownList(userId, dropdownName);
            if (worksapceResult.Count == 0)
            {
                _logger.LogInfo($"Workspace data doesn't exist in the database.");
                return NotFound();
            }

            return Ok(worksapceResult);
        }
        [Route(ServiceConstants.CommonDropdown.GetUAMDropdown)]
        [HttpGet]
        public async Task<IActionResult> GetUAMDropdown(string userId, string dropdownName, string subUserID)
        {
            var worksapceResult = await _commonDropdownListService.GetUAMDropDown(userId, dropdownName, subUserID);
            if (worksapceResult.Count == 0)
            {
                _logger.LogInfo($"Workspace data doesn't exist in the database.");
                return NotFound();
            }

            return Ok(worksapceResult);
        }
    
    [Route(ServiceConstants.CommonDropdown.GetTreeDropdown)]
    [HttpGet]
    public async Task<IActionResult> GetTreeDropDown([FromQuery] TreeDropDownInputModel treeDropDownInputModel)
    {
        var worksapceResult = await _commonDropdownListService.GetTreeDropDownChild(treeDropDownInputModel);
        if (worksapceResult.Count == 0)
        {
            _logger.LogInfo($"Workspace data doesn't exist in the database.");
            return NotFound();
        }

        return Ok(worksapceResult);
    }
        [Route(ServiceConstants.CommonDropdown.GetModelNameDropdown)]
        [HttpGet]
        public async Task<IActionResult> GetModelNameDropdown([FromQuery] FileModelListcs input)
        {
            var modelNameResult = await _commonDropdownListService.GetModelTypeList(input);
            if (modelNameResult.Count == 0)
            {
                _logger.LogInfo($"Model  Name doesn't exist in the database.");
                return NotFound();
            }

            return Ok(modelNameResult);
        }
        [Route(ServiceConstants.CommonDropdown.GetFIELDNameDropdown)]
        [HttpGet]
        public async Task<IActionResult> GetFIELDNameDropdown(string userId, string dropdownName)
        {
            var modelNameResult = await _commonDropdownListService.GetFieldNameList(userId, dropdownName);
            if (modelNameResult.Count == 0)
            {
                _logger.LogInfo($"Field Name doesn't exist in the database.");
                return NotFound();
            }

            return Ok(modelNameResult);
        }


        [Route(ServiceConstants.CommonDropdown.GetDropdownWithCategory)]
        [HttpGet]
        public async Task<IActionResult> GetDropdownWithCategory(string userId, string dropdownName)
        {
            var worksapceResult = await _commonDropdownListService.GetDropDownListWithCategory(userId, dropdownName);
            if (worksapceResult.Count == 0)
            {
                _logger.LogInfo($"Workspace data doesn't exist in the database.");
                return NotFound();
            }

            return Ok(worksapceResult);
        }
    }
}