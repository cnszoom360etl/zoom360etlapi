﻿using CNS.ZOOM360.Entities.StoreProcedures.ConfigureTransformation;
using CNS.ZOOM360.Entities.StoreProcedures.EnrichDynamicModel;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.StoreProcedures.EnrichDynamivMetadata;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.DynamicEnrichScript.RouteName)]
    [ApiController]
    public class EnrichDynamicScriptController : ControllerBase
    {
        private readonly IEnrichDynamicMetadata _context;
        protected List<string> _dbCollection;
        private IMongoDatabase database;


        public EnrichDynamicScriptController(IEnrichDynamicMetadata context)
        {
            _context = context;
            //  _dbCollection = _context.GetCollections();
        }
        [Route(ServiceConstants.DynamicEnrichScript.getFunctionTemplate)]
        [HttpGet]
        public async Task<ActionResult> GetFunctionTemplate()
        {
            return Ok(this._context.getFunctionTemplate());
        }
        //[HttpGet]
        //[Route("GetObject")]
        //public async Task<ActionResult<IEnumerable<Collection>>> GetObject(string user_id, string workspace_id, string client_id, string record)
        //{
        //    return Ok(this._context.GetObject(user_id, workspace_id, client_id, record));
        //}


        [Route(ServiceConstants.DynamicEnrichScript.saveEnrichScript)]
        [HttpPost]
        public async Task<string> SaveEnrichScript(EnrichDynamicModel collection)
        {
            var msg = await this._context.SaveEnrichScript(collection);
            return msg;
        }
        
        // get object
        [Route(ServiceConstants.DynamicEnrichScript.getEnrichScript)]
        [HttpGet]
        public async Task<ActionResult> GetEnrichScript(string userId, string workspaceId, string clientId, string templateId)
        {
            return Ok(this._context.GetEnrichScript(userId, workspaceId, clientId, templateId));
        }
        //updating selected script
        [Route(ServiceConstants.DynamicEnrichScript.updateEnrichScript)]
        [HttpPut]
        public async Task<ActionResult> UpdateEnrichScript(EnrichDynamicModel Script, string _id)
        {
            return Ok(this._context.UpdateEnrichScript(Script, _id));
        }

        //Delete selected script
        [Route(ServiceConstants.DynamicEnrichScript.DeleteScript)]
        [HttpDelete]
        public async Task<string> DeleteScript(string UserId, string scriptid)
        {
            var DeleteStatus = this._context.DeleteEnrichScript(UserId, scriptid);
            return DeleteStatus;
        }

        [Route(ServiceConstants.DynamicEnrichScript.getScriptList)]
        [HttpGet]
        public async Task<ActionResult> GetScriptList(string userId, string workspaceId, string clientId)
        {
            return Ok(this._context.GetScriptList(userId, workspaceId, clientId));
        }

        [Route(ServiceConstants.DynamicEnrichScript.GetTransformationList)]
        [HttpPost]
        public List<dynamic> GetTransformationList(function_IdsArray[] _functionIds)
        {
            var list = this._context.getscriptlistbyusingIds(_functionIds);
            return list;
        }

        [Route(ServiceConstants.DynamicEnrichScript.DestinationList)]
        [HttpPost]
        public string DestinationList(Destination_Json[] _Des , string userId, string workspaceId, string clientId, string accountId)
        {
            var list = this._context.GetDestinationList(_Des,userId,workspaceId,clientId,accountId);
            return "update";
        }

        [Route(ServiceConstants.DynamicEnrichScript.savescriptFor_Transformation_Mapping_Loading_Data)]
        [HttpPost]
        public async Task<ActionResult> savescriptFor_Transformation_Mapping_Loading_Data(JsonAppledFor_Mapping_Entirichment_Load collection ,string accountId)
        {
            var message = this._context.SaveTransformation_Mapping_Loading_Script(collection , accountId);
            return Ok(message.Result);
        }

        [Route(ServiceConstants.DynamicEnrichScript.ETLStatus)]
        [HttpGet]
        public async Task<ActionResult> ETLStatus()
        {
            var message = this._context.getStatusForETL();
            return Ok(message);
        }
        [Route(ServiceConstants.DynamicEnrichScript.GetMappedListForEditMode)]
        [HttpGet]
        public async Task<ActionResult> GetMappedListForEditMode(string userId, string workspaceId, string clientId, string accountId, string EditscreenName)
        {
            var message = this._context.GetContentMappedList(userId,workspaceId,clientId,accountId,EditscreenName);
            return Ok(message);
        }
        [Route(ServiceConstants.DynamicEnrichScript.ColumnMappingEditMode)]
        [HttpGet]
        public async Task<ActionResult> ColumnMappingEditMode(string userId, string workspaceId, string clientId, string accountId, string EditscreenName)
        {
            var message = this._context.GetColumnMappedList(userId, workspaceId, clientId, accountId, EditscreenName);
            return Ok(message);
        }
        [Route(ServiceConstants.DynamicEnrichScript.LoadMappingEditMode)]
        [HttpGet]
        public async Task<ActionResult> LoadMappingEditMode(string userId, string workspaceId, string clientId, string accountId)
        {
            var message = this._context.GetDestinationMappedList(userId, workspaceId, clientId, accountId);
            return Ok(message);
        }
    }
}
