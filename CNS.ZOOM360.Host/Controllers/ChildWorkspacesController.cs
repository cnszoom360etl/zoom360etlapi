﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.Repositories;
using CNS.ZOOM360.Entities.Model;
using CNS.ZOOM360.Entities.StoreProcedures;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;

namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.ChildWorkSpace.RouteName)]
    [ApiController]
    public class ChildWorkspacesController : ControllerBase
    {
        private IRepositoryBase<ChildWorkspaces> _ExtractListModal;
        public ChildWorkspacesController(IRepositoryBase<ChildWorkspaces> ExtractListModal) {
            _ExtractListModal = ExtractListModal;
        }

        [HttpPost]
        [Route(ServiceConstants.ChildWorkSpace.SaveChildWorkspaces)]
        public  string SaveChildWorkspaces(ChildWorkspaces childWorkspacesModel)
        {

                        
            object[] parameters = {
            new SqlParameter("@USER_ID", childWorkspacesModel.UserId),
            new SqlParameter("@WORKSPACE_ID", childWorkspacesModel.WorkspaceId),
            new SqlParameter("@CLIENT_ID", childWorkspacesModel.ClientId),
            new SqlParameter("@CHILD_WORKSPACE", childWorkspacesModel.ChildWorkspace),
            new SqlParameter("@CHILD_SELECTED_OPTIONS", childWorkspacesModel.ChildSelectedOption),
            new SqlParameter("@CHILD_CHANGES", childWorkspacesModel.ChildChange),
            new SqlParameter("@CHILD_OVERRIDE_SELECTED_OPTIONS", childWorkspacesModel.ChildOverrideSelectedOption),
             new SqlParameter
             {
                ParameterName = "@V_MESSAGE",
                SqlDbType = SqlDbType.NVarChar,
                Direction = ParameterDirection.Output,
                Size = 4000

             }
            };
            string responseMessage = string.Empty;
            string spQuery = "SAVECHILDWORKSPACES @USER_ID, @WORKSPACE_ID, @CLIENT_ID, @CHILD_WORKSPACE, @CHILD_SELECTED_OPTIONS, @CHILD_CHANGES, @CHILD_OVERRIDE_SELECTED_OPTIONS ,@V_MESSAGE OUTPUT";
            
            responseMessage = _ExtractListModal.ExecuteCommand(spQuery, parameters);
            return "";
            
        }
    }
}