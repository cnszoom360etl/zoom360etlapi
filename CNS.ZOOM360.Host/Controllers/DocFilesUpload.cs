﻿using CNS.ZOOM360.Services.StoreProcedures.ConnectTOShareFolderDirectory;
using CNS.ZOOM360.Shared.Const;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.upload.RouteName)]
    [ApiController]
    public class DocFilesUpload : Controller
    {
        private IHostingEnvironment hostingEnv;
         
        public DocFilesUpload(IHostingEnvironment env)
        {
            this.hostingEnv = env;
          
        }
        [Route(ServiceConstants.upload.Save)]
         
        //[AcceptVerbs("Post")]
        [HttpPost()]
        [RequestFormLimits(MultipartBodyLengthLimit = 999999999)]
        [RequestSizeLimit(999999999)]
        public IActionResult Save(IList<IFormFile> UploadFiles)
        {
            try
            {
                foreach (var file in UploadFiles)
                {
                    if (UploadFiles != null)
                    {
                        var filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                        filename = hostingEnv.WebRootPath + $@"\{filename}";
                        if (!System.IO.File.Exists(filename))
                        {
                            //var fileserversavepath = @"//192.168.223.111//Bd-dev-vm2/d/zoom_files_dump/" + file.FileName;
                            //NetworkCredential credentials = new NetworkCredential(@"CNSE\dev-admin", "@dmin3210");
                            //credentials.Domain = @"//192.168.223.111//Bd-dev-vm2/d/zoom_files_dump/";
                             var fileserversavepath = @"//192.168.223.100/d/zoom_files_dump/" + file.FileName;
                            //using (new ConnectToSharedFolder(fileserversavepath, credentials))
                            //{
                                using (var stream = new FileStream(fileserversavepath, FileMode.Create))
                                {
                                    file.CopyTo(stream);
                                    var filereading = file.OpenReadStream();
                                }
                            //}

                            //    using (var stream = new FileStream(fileserversavepath, FileMode.Create))
                            //{
                            //    file.CopyTo(stream);
                            //    var filereading = file.OpenReadStream();
                            //}
                        }
                        else
                        {
                            Response.Clear();
                            Response.StatusCode = 204;
                            Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "File Upload Successfully..!";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.Clear();
                Response.ContentType = "application/json; charset=utf-8";
                Response.StatusCode = 208;
                Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "Server Error";
                // Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = e.Message;
                return Content("");
            }
            return Content("");
        }
    }
}
