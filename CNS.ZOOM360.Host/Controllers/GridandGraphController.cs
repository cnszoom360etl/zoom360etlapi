﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.LoggerServices;
using CNS.ZOOM360.Shared.StoreProcedures.GridAndGraphData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.GridAndGraph.RouteName)]
    [ApiController]
    public class GridandGraphController : ControllerBase
    {
        private readonly IGridAndGraphDataService _GridAndGraphDataService;
        private readonly ILoggerManager _logger;

        public GridandGraphController(IGridAndGraphDataService GridAndGraphDataService, ILoggerManager logger)
        {
            _GridAndGraphDataService = GridAndGraphDataService;
            _logger = logger;
        }
        [Route(ServiceConstants.GridAndGraph.GraphData)]
        [HttpGet]
        public async Task<IActionResult> GetGraphData()
        {
            var graphData = _GridAndGraphDataService.getGraphData();

            return Ok(graphData.Result);
        }
        [Route(ServiceConstants.GridAndGraph.GridData)]
        [HttpGet]
        public async Task<IActionResult> GetGridData()
        {
            var graphData = _GridAndGraphDataService.getGridData();

            return Ok(graphData.Result);
        }
        [Route(ServiceConstants.GridAndGraph.GridDynamicData)]
        [HttpGet]
        public async Task<IActionResult> GridDynamicData(string userId, string WorkSpaceId, string Client_Id, string analysisType)
        {
            var graphData = _GridAndGraphDataService.dynamicGridData(userId,WorkSpaceId, Client_Id,analysisType);
            return Ok(graphData.Result);
        }
    }
}
