﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CNS.ZOOM360.Entities.TimeZoneSetup;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.LoggerServices;
using CNS.ZOOM360.Shared.StoreProcedures.TimeZoneSetup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.TimeZoneSetup.RouteName)]
    [ApiController]
    public class TimeZoneSetupController : ControllerBase
    {
        private readonly ITimeZoneSetupService _timeZoneSetupService;
        private readonly ILoggerManager _logger;
        public TimeZoneSetupController(ITimeZoneSetupService timeZoneSetupService, ILoggerManager logger)
        {
            _timeZoneSetupService = timeZoneSetupService;
            _logger = logger;

        }

        [Route(ServiceConstants.TimeZoneSetup.SaveTimeZoneSetup)]
        [HttpPost]
        public async Task<IActionResult> SaveTimeZoneSetup(TimeZoneSetupModel modal)
        {
            var worksapceData = _timeZoneSetupService.SaveTimeZoneSetup(modal);

            return Ok(worksapceData.Result);
        }
    }
}