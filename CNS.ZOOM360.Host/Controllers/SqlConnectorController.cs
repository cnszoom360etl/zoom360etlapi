using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using Google.Apis.Drive.v3;
using System.Threading;
using System.Threading.Tasks;
using CNS.ZOOM360.Entities.Model;
using CNS.ZOOM360.Entities.StoreProcedures.Connectors.Databases.SQL;
using CNS.ZOOM360.Services.StoreProcedures.Connectors.Databases.SQL;
using CNS.ZOOM360.Services.StoreProcedures.NetworkDrive;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.StoreProcedures.Connector.Database.SQL;
using CNS.ZOOM360.Shared.StoreProcedures.Connector.Database.SQL.Dto;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleImpersonation;
using CNS.ZOOM360.Entities.Model.TranformationClassObject;
using Newtonsoft.Json;
using CNS.ZOOM360.Entities.StoreProcedures.Transformation;
using Google.Apis.Download;
using Microsoft.AspNetCore.Hosting;
using CNS.ZOOM360.Shared.LoggerServices;
using Google.Apis.Auth.AspNetCore3;
 
namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.SqlConnector.RouteName)]
    [ApiController]
    public class SqlConnectorController : ControllerBase
    {
       
        private readonly ISqlConnectorService _sqlConnectorService;
        private IWebHostEnvironment _hostingEnvironment;
        private IHostingEnvironment hostingEnv;
        private readonly ILoggerManager _logger;
        //private IGoogleAuthProvider _googleAuth;
        //private readonly SqlConnectorService _connectorservices;

        public SqlConnectorController(ISqlConnectorService sqlConnectorService,  IWebHostEnvironment environment, IHostingEnvironment env, ILoggerManager logger) {
            _sqlConnectorService = sqlConnectorService;
            this.hostingEnv = env;
            _logger = logger;
            //_MongoServices = MongoServices;
            _hostingEnvironment = environment;
            //_googleAuth = googleAuth;

        }

        [Route(ServiceConstants.SqlConnector.SaveStep1Data)]
        // POST: api/SqlConnector/SaveStep1Data
        [HttpPost]
        public void SaveStep1Data(SOURCE_CNF ChildData, string AccountDisplayName, string EnableConnectoins, string Email, string AccountAuthurization, string Workspace, string UserId, string WORKSPACEID,bool connectivitystatus, string CLIENTID, string AccountId, string ConnectorId)
        {

            EmailAuthenticationSourceAccount EmailAuthentication = new EmailAuthenticationSourceAccount();
            SourceAccountConnectionModel sourceAccountConnectionModel = new SourceAccountConnectionModel();
            Type myClassType = ChildData.GetType();
            PropertyInfo[] properties = myClassType.GetProperties();
            var list = "";
            //string UserId = "admin";
            //string WORKSPACEID = "1";
            //string CLIENTID = "1002";
            //string AccountId = "9003";
            //string ConnectorId = "214";l
            sourceAccountConnectionModel.Connector_ID = ConnectorId;
            sourceAccountConnectionModel.UserId = UserId;
            sourceAccountConnectionModel.WorkspaceId = WORKSPACEID;
            sourceAccountConnectionModel.ClientId = CLIENTID;
            sourceAccountConnectionModel.Account_Id = AccountId;
            sourceAccountConnectionModel.connectivitystatus = connectivitystatus;
            foreach (PropertyInfo property in properties)
            {
                //property.Name 
                sourceAccountConnectionModel.HostName = property.Name;
                sourceAccountConnectionModel.FieldValue = property.GetValue(ChildData, null);
                list = _sqlConnectorService.SaveConnection(sourceAccountConnectionModel);

            }
            EmailAuthentication.UserId = UserId;
            EmailAuthentication.WorkspaceId = WORKSPACEID;
            EmailAuthentication.ClientId = CLIENTID;
            EmailAuthentication.Account_Id = AccountId;
            EmailAuthentication.Email = Email;
            EmailAuthentication.AccountAuthurization = AccountAuthurization;
            if (EmailAuthentication.Email != null || EmailAuthentication.AccountAuthurization != null)
            {
                var list2 = _sqlConnectorService.saveEmailAuthenticationForConnection(EmailAuthentication);

            }
            SourceAccountModel sourceAccountModel = new SourceAccountModel();
            sourceAccountModel.UserId = UserId;
            sourceAccountModel.WorkspaceId = WORKSPACEID;
            sourceAccountModel.WorkspaceName = Workspace;
            sourceAccountModel.ClientId = CLIENTID;
            sourceAccountModel.Account_Id = AccountId;
            sourceAccountModel.ConnectorId = ConnectorId;
            sourceAccountModel.DisplayName = AccountDisplayName;
            if(EnableConnectoins=="1")
            {
                sourceAccountModel.EnableStatus = Convert.ToBoolean(Convert.ToInt32(EnableConnectoins));
            }
            else
            {
                sourceAccountModel.EnableStatus = Convert.ToBoolean(Convert.ToInt32(EnableConnectoins));
            }
            
            var list3 = _sqlConnectorService.SaveAccountDispaly(sourceAccountModel);
            return ;
        }
        // POST: api/SqlConnector/SaveStep4Data
        //savestep4data
        [Route(ServiceConstants.SqlConnector.SaveSourceObjects)]
        [HttpPost]
        public string SaveSourceObjects(ConnectionObjectStep4Model Connector,string account_id,string ConnectorId)
        {
            string UserId = "admin";
            string WORKSPACEID = "1";
            string CLIENTID = "1002";

            SourceObjectModel sourceObjectModel = new SourceObjectModel();
            sourceObjectModel.UserId = UserId;
            sourceObjectModel.WorkSpaceID = WORKSPACEID;
            sourceObjectModel.ClientId = CLIENTID;
            sourceObjectModel.WorkspaceName = Connector.WorkspaceName;
            sourceObjectModel.ObjectName = Connector.SourceDisplayName;
            sourceObjectModel.AccessGrant = Connector.Authorizationgranted;
            sourceObjectModel.AccountId = account_id;
            sourceObjectModel.ConnectorId = ConnectorId;
             return _sqlConnectorService.SaveSourceObjects(sourceObjectModel);
         }
        // GET: api/SqlConnector/GetSourceObjectListForStep4Grid
        [Route(ServiceConstants.SqlConnector.GetSourceObjectListForStep4Grid)]
        [HttpGet]
        public async Task<IEnumerable<GEtSourceObjectList>> GetSourceObjectListForStep4Grid([FromQuery]GetSourceObjectListInput  input)
        {

            
            //if (input.connectortitle== "My SQL")
            //{
            //    var list1 = _sqlConnectorService.GetAllMySqlData();
            //    return list1;
            //}
              var  list = await _sqlConnectorService.GetSourceObjectList(input);
              return list;
             
            
        }
        // GET: api/SqlConnector/GetEntityNameDropdownStep6

        [Route(ServiceConstants.SqlConnector.GetEntityNameDropdownStep6)]
        [HttpGet]
        public async Task<IEnumerable<GEtSourceObjectList>> GetEntityNameDropdownStep6([FromQuery]GetSourceObjectListInput input)
        {

            var list = await _sqlConnectorService.GetSourceObjectList(input);
            return list;
        }
        // GET: api/SqlConnector/StepForWorkspaceDropdown

        // GET: api/SqlConnector/GetObjectFieldsListStep6

        [Route(ServiceConstants.SqlConnector.GetObjectFieldsListStep6)]
        [HttpGet]
        public async Task<IEnumerable<EntityObjectFieldsList>> GetObjectFieldsListStep6([FromQuery]GetObjectFieldListInput input)
        {

            var list = await _sqlConnectorService.GetObjectFieldsList(input);
            return list;
        }
        // Post: api/SqlConnector/ObjectEntityFieldUpdateStep6

        [Route(ServiceConstants.SqlConnector.ObjectEntityFieldUpdateStep6)]
        [HttpPost]
        public async Task<IEnumerable<EntityObjectFieldsList>> ObjectEntityFieldUpdateStep6(UpdateEntityObjectFieldInputs[] inputs, string UserId, string Workspaceid, string Clientid, string ConnectorId, string objectName)
        {
            List<EntityObjectFieldsList> list2 = new List<EntityObjectFieldsList>();
           
            for (int i = 0; i < inputs.Length; i++)
            {
                list2 = await _sqlConnectorService.UpdateEntityObjectField(inputs[i],  UserId,  Workspaceid,  Clientid,  ConnectorId,  objectName);
            }

            
            return list2;
        }


        // Post: api/SqlConnector/SourceObjectGridListUpdateStep4
        [Route(ServiceConstants.SqlConnector.SourceObjectGridListUpdateStep4)]
        [HttpPost]
        public async Task<List<UpdateSourceObjectGridModel>> SourceObjectGridListUpdateStep4(GEtSourceObjectList[] inputs,string UserId, string Workspaceid, string Clientid, string ConnectorId)
        {
            List<UpdateSourceObjectGridModel> list2 = new List<UpdateSourceObjectGridModel>();
            GEtSourceObjectList sourcelist = new GEtSourceObjectList();
            sourcelist.UserId = UserId;
            sourcelist.WORKSPACE_ID = Convert.ToInt32(Workspaceid);
            sourcelist.CLIENT_ID = Convert.ToInt32(Clientid);
            sourcelist.CONNECTOR_ID = Convert.ToInt32(ConnectorId);

            for (int i = 0; i < inputs.Length; i++)
            {
                list2 = await _sqlConnectorService.UpdateGridSourceObjectList(inputs[i], UserId, Convert.ToInt32(Workspaceid), Convert.ToInt32(Clientid), Convert.ToInt32(ConnectorId));
            }


            return list2;
        }
        // Post: api/SqlConnector/ExtractPageDataSaveForStep8
        [Route(ServiceConstants.SqlConnector.ExtractPageDataSaveForStep8)]

        [HttpPost]
        public string ExtractPageDataSaveForStep8(SaveExtractsInputs inputs)
        {
             
           // var datelink = Data.date_LINK + "To" + Data.date_LINK1;
            var list2 = _sqlConnectorService.SaveExtracts(inputs);


            return list2;
        }
        public static string[] Scopes = { DriveService.Scope.Drive };
        private static DriveService GetService()
        {

            UserCredential credential;
           //using (var stream = new FileStream(@"E:\desktopclient.json", FileMode.Open, FileAccess.Read))

           using (var stream = new FileStream(@"E:\client_secret.json", FileMode.Open, FileAccess.Read))
            {
                 

                //string FilePath = Path.Combine("Assests", "DriveServiceCredential.json");
                string FilePath = Path.Combine("Assests", "token.json");
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(FilePath, true)
                    ).Result;
            }

            DriveService service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Zoom360",
            });
            return service;
        }

        [Route(ServiceConstants.SqlConnector.testgoogle)]
        [HttpGet]
        [GoogleScopedAuthorize(DriveService.ScopeConstants.DriveReadonly)]
        
        public async Task<IActionResult> testgoogle([FromServices] IGoogleAuthProvider auth)
        {
            List<string> driveFileList = new List<string>();
            GoogleCredential cred = await auth.GetCredentialAsync();
            var service = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = cred
            });
            var files = await service.Files.List().ExecuteAsync();
            driveFileList = files.Files.Select(x => x.Name).ToList();
            return  Ok();
        }
        [Route(ServiceConstants.SqlConnector.GetSqlConnectorList)]
        // GET: api/SqlConnector/GetSqlConnectorList
        [HttpGet]
        public async Task<List<UpdateConnectorListStep2>> GetSqlConnectorList(string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId, string connectorname)
        {
            List<UpdateConnectorListStep2> list = new List<UpdateConnectorListStep2>();
            List<UpdateConnectorListStep2> list2 = new List<UpdateConnectorListStep2>();
            var GetfileStatus ="NO";
            list = await _sqlConnectorService.GetConnectorData(Account_Id, UserId, Workspaceid, Clientid, ConnectorId);
            
            List<string> colsList = new List<string>();
            colsList.Add("email_address");
            colsList.Add("user_login_id");

            List<string> colList = new List<string>();
            colList.Add("*");
            foreach (var im in list)
            {
                var transform = new transform
                {
                    e1 = new e1
                    {
                        fill_null_values = new fill_null_values
                        {
                            replacement_dict = new replacement_dict { accountId = "0006" },
                            create_col = false,
                            save_previous_changes = false
                        },
                        convert_to_upper_case = new convert_to_upper_case
                        {
                            cols = colsList.ToArray(),
                            create_col = false,
                            save_previous_changes = false
                        },
                        remove_multiline = new remove_multiline
                        {
                            cols = colList.ToArray(),
                            create_col = false,
                            save_previous_changes = false
                        },
                        remove_unicode_characters = new remove_unicode_characters
                        {
                            cols = colList.ToArray(),
                            create_col = false,
                            save_previous_changes = false
                        }
                    }
                };
                // string jsonData = JsonConvert.SerializeObject(transform);
                im.transformed = transform;
             }
            if (connectorname == "Google Drive Download")
            {
                var filename = "";
                var filevalue = "";
                List<GoogleDriveFiles> FileList = new List<GoogleDriveFiles>();
                 Task<List<string>> DriveFiles=Task.FromResult(new List<string>());

                DriveService service = GetService();
                FilesResource.ListRequest FileListRequest = service.Files.List(); ;

                FileListRequest.Fields = "nextPageToken, files(id, name, size, version, trashed, createdTime, mimeType)";
                IList<Google.Apis.Drive.v3.Data.File> files = FileListRequest.Execute().Files;
                //dynamic DriveFiles = DriveFileList(_googleAuth);
                foreach (var item in list)
                {
                    filename = item.Filedname;
                    filevalue = item.Fieldvalue;
                     
                }

                if (files != null && files.Count!=0)
                {
                     foreach (var file in FileList)
                    {
                        
                        if (file.Name == filename)
                        {
                            GetfileStatus = "YES";
                            GoogleDriveFiles File = new GoogleDriveFiles
                            {
                                Id = file.Id,
                                Name = file.Name,
                                Size = file.Size,
                                Version = file.Version,
                                CreatedTime = file.CreatedTime
                                
                            };
                       DownloadFileforserver(File.Id);

                              return list;

                        }
                         

                        }
                   

                }
                if(GetfileStatus == "NO")
                {

                    foreach (var item in list)
                    {
                        item.Filedname = "File Not Found";
                    }

                    return list;

                }

            }
         return list;

        }

       
        [Route(ServiceConstants.SqlConnector.GetSqlConnectorListForExtraction)]
        // GET: api/SqlConnector/GetSqlConnectorList
        [HttpPost]
        public async Task<List<UpdateConnectorListStep2>> GetSqlConnectorListForExtraction(extractionModelClass[] Account_Id, string UserId, string Workspaceid, string Clientid)
        { 
             
                var list = await _sqlConnectorService.GetConnectorDataForExtraction(Account_Id, UserId, Workspaceid, Clientid);
                
            return list;
        }

        // GET: api/SqlConnector/GetSqlExtractPageList

        [Route(ServiceConstants.SqlConnector.GetSqlExtractPageList)]
        [HttpPost]
        public async Task<IEnumerable<ExtractModel>> GetSqlExtractPageList(GetExtractDataInputs Inputs)
        {

            var list = await _sqlConnectorService.GetExtractData(Inputs);
            return list;
        }


        [Route(ServiceConstants.SqlConnector.UpdateSourceObject)]
        [HttpGet]
        public async Task<List<SourceObjectGetList>> SourceObjectUpdate(string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId)
        {

            var list = await _sqlConnectorService.GetSourceObjectforUpdate( Account_Id,  UserId,  Workspaceid,  Clientid,  ConnectorId);
            return list;
        }

        [Route(ServiceConstants.SqlConnector.GetSqlConnectorListforDriveFile)]
        [HttpGet]
        public   List<GoogleDriveFiles> GetFileList(string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId, string connectorname)
        {
             
            var filename = "";
            var filevalue = "";
            
            List<GoogleDriveFiles> FileList = new List<GoogleDriveFiles>();
            //if (connectorname == "Google Drive Download")
            //{
                var list = _sqlConnectorService.GetConnectorDataForDriveFiles(Account_Id, UserId, Workspaceid, Clientid, ConnectorId);
                foreach (var item in list)
                {
                    filename = item.Filedname;
                    filevalue = item.Fieldvalue;
                }
            
           DriveService service = GetService();
                FilesResource.ListRequest FileListRequest = service.Files.List();
                FileListRequest.Fields = "nextPageToken, files(id, name, size, version, trashed, createdTime, mimeType)";
                IList<Google.Apis.Drive.v3.Data.File> files = FileListRequest.Execute().Files;
                
                if (files != null && files.Count > 0)
                {
                    foreach (var file in files)
                    {
                    
                    if (file.Name == filename)
                        {
                       
                        GoogleDriveFiles File = new GoogleDriveFiles
                        {
                            Id = file.Id,
                            Name = file.Name,
                            Size = file.Size,
                            Version = file.Version,
                            CreatedTime = file.CreatedTime
                        };
                        string FolderPath = Path.Combine("Resources", "files");
                        var FolderPath1 = Path.Combine(Directory.GetCurrentDirectory(), FolderPath);
                        FilesResource.GetRequest request = service.Files.Get(File.Id);
                        string FileName = request.Execute().Name;
                        string FilePath = System.IO.Path.Combine(FolderPath1, FileName);
                        MemoryStream stream1 = new MemoryStream();
                        SaveStream(stream1, FilePath);
                        FileList.Add(File);


                    }
 
                        
                           
                         

                    }
                }
                
            //}
                
            return FileList;
        }

        private void SaveStream(MemoryStream stream, string Path)
        {
            using (FileStream file = new FileStream(Path, FileMode.Create))
            {
                stream.CopyTo(file);
            }

        }
        private void SavestreamtoNetwork(Stream stream, string Path)
        {
            using (FileStream file = new FileStream(Path, FileMode.Create))
            {
                stream.CopyTo(file);
            }

        }

        private string DownloadFileforserver(string id)
        {
            string FilePath = DownloadGoogleFile(id);

           // string FolderPath = Path.Combine("Resources", "files");
            //var FolderPath1 = Path.Combine(Directory.GetCurrentDirectory(), FolderPath) + Path.GetFileName(FilePath);
           // var FolderPath1 =Path.GetFileName(FilePath);

           // var bytes = System.Text.Encoding.UTF8.GetBytes(FolderPath1);
            //Response.ContentType = "application/zip";

           // Response.Headers.Add("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            //Response.Body.Read(bytes);
            //Response.Body.Flush();
            return "Done";
        }

        private string DownloadGoogleFile(string fileId)
        {
            DriveService service = GetService();
            string serverpath = _hostingEnvironment.ContentRootPath;
            //string FolderPath = System.Web.HttpContext.Current.Server.MapPath("/GoogleDriveFiles/");
            string FolderPath = Path.Combine("Resources", "files");
            //var FolderPath1 = Path.Combine(Directory.GetCurrentDirectory(), FolderPath);
            string fileserversavepath = @"//192.168.223.100/zoom_files_dump/";
            FilesResource.GetRequest request = service.Files.Get(fileId);

            string FileName = request.Execute().Name;
            string FilePath = System.IO.Path.Combine(fileserversavepath, FileName);

            MemoryStream stream1 = new MemoryStream();

            request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
            {
                switch (progress.Status)
                {
                    case DownloadStatus.Downloading:
                        {
                            //Console.WriteLine(progress.BytesDownloaded);
                            break;
                        }
                    case DownloadStatus.Completed:
                        {
                            //Console.WriteLine("Download complete.");
                            SaveStreamonserver(stream1, FilePath);
                            break;
                        }
                    case DownloadStatus.Failed:
                        {
                            //Console.WriteLine("Download failed.");
                            break;
                        }
                }
            };
            request.Download(stream1);
            return FilePath;
        }

        private void SaveStreamonserver(MemoryStream stream, string FilePath)
        {
            using (System.IO.FileStream file = new FileStream(FilePath, FileMode.Create, FileAccess.ReadWrite))
            {
                stream.WriteTo(file);
            }
        }



        // GET: api/SqlConnector/getFunctionList
        [Route(ServiceConstants.SqlConnector.getFunctionList)]
        [HttpGet]
        public async Task<IEnumerable<TransformationFunctionModel>> getFunctionList([FromQuery] functionPerametersValueModel input)
        {
           var list = await _sqlConnectorService.getFunctionDetails(input);
            return list;
        }




        [Route(ServiceConstants.SqlConnector.GetConnectorListForEdit)]
        // GET: api/SqlConnector/GetSqlConnectorList
        [HttpGet]
        public async Task<List<UpdateConnectorListStep2>> GetConnectorListForEdit(string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId, string connectorname)
        {
            List<UpdateConnectorListStep2> list = new List<UpdateConnectorListStep2>();
            list = await _sqlConnectorService.GetConnectorListForEdit(Account_Id, UserId, Workspaceid, Clientid, ConnectorId);

            if (list.Count == 0)
            {
                _logger.LogError("");
                return list;
            }

            return list;

        }


    [Route(ServiceConstants.SqlConnector.ETLConnectorList)]
    // GET: api/SqlConnector/GetSqlConnectorList
    [HttpPost]
    public async Task<List<UpdateConnectorListStep2>> ETLConnectorList(ListData[] listData, string UserId, string Workspaceid, string Clientid)
    {
        List<UpdateConnectorListStep2> list = new List<UpdateConnectorListStep2>();
         List<UpdateConnectorListStep2> ConnectorData = new List<UpdateConnectorListStep2>();
      string connectorname = string.Empty;
      var GetfileStatus = "NO";
      for (int i = 0; i < listData.Length; i++)
      {
        ConnectorData = await _sqlConnectorService.ETLGetConnectorData(listData[i].accountid, UserId, Workspaceid, Clientid, listData[i].connectorId);
        for (int y = 0; y < ConnectorData.Count; y++)
        {
          if(ConnectorData[y].Filedname=="Database")
          {
            UpdateConnectorListStep2 obj = new UpdateConnectorListStep2();
            obj.accountId = ConnectorData[y].accountId;
            obj.BStatusforvarification = ConnectorData[y].BStatusforvarification;
            obj.accountdisplayname = ConnectorData[y].accountdisplayname;
            obj.authorizationgrant = ConnectorData[y].authorizationgrant;
            obj.connectorname = ConnectorData[y].connectorname;
            obj.enableconnection = ConnectorData[y].enableconnection;
            obj.Database = ConnectorData[0].Fieldvalue;
            obj.Username = ConnectorData[4].Fieldvalue;
            obj.Hostname = ConnectorData[1].Fieldvalue;
            obj.Password = ConnectorData[2].Fieldvalue;
            obj.Port= ConnectorData[3].Fieldvalue;
            obj.StatusnotifyGrant = ConnectorData[y].StatusnotifyGrant;
            list.Add(obj);
            y = ConnectorData.Count - 1;
            }
          else
          {
            UpdateConnectorListStep2 obj = new UpdateConnectorListStep2();
            obj.accountId = ConnectorData[y].accountId;
            obj.BStatusforvarification = ConnectorData[y].BStatusforvarification;
            obj.accountdisplayname = ConnectorData[y].accountdisplayname;
            obj.authorizationgrant = ConnectorData[y].authorizationgrant;
            obj.connectorname = ConnectorData[y].connectorname;
            obj.enableconnection = ConnectorData[y].enableconnection;
            obj.Fieldvalue = ConnectorData[y].Fieldvalue;
            obj.Filedname = ConnectorData[y].Filedname;
            obj.StatusnotifyGrant = ConnectorData[y].StatusnotifyGrant;
            list.Add(obj);
          }
        }
      }

      
      if (connectorname == "Google Drive Download")
      {
        var filename = "";
        var filevalue = "";
        List<GoogleDriveFiles> FileList = new List<GoogleDriveFiles>();
        Task<List<string>> DriveFiles = Task.FromResult(new List<string>());

        DriveService service = GetService();
        FilesResource.ListRequest FileListRequest = service.Files.List(); ;

        FileListRequest.Fields = "nextPageToken, files(id, name, size, version, trashed, createdTime, mimeType)";
        IList<Google.Apis.Drive.v3.Data.File> files = FileListRequest.Execute().Files;
        //dynamic DriveFiles = DriveFileList(_googleAuth);
        foreach (var item in list)
        {
          filename = item.Filedname;
          filevalue = item.Fieldvalue;

        }

        if (files != null && files.Count != 0)
        {
          foreach (var file in FileList)
          {

            if (file.Name == filename)
            {
              GetfileStatus = "YES";
              GoogleDriveFiles File = new GoogleDriveFiles
              {
                Id = file.Id,
                Name = file.Name,
                Size = file.Size,
                Version = file.Version,
                CreatedTime = file.CreatedTime

              };
              DownloadFileforserver(File.Id);

              return list;

            }


          }


        }
        if (GetfileStatus == "NO")
        {

          foreach (var item in list)
          {
            item.Filedname = "File Not Found";
          }

          return list;

        }

      }
      return list;

    }


        [Route(ServiceConstants.SqlConnector.GetConnectorStatus)]
        [HttpPost]
        public async Task<IActionResult> GetConnectorStatus(connectorStatusModel Input)
        {
            var worksapceData = _sqlConnectorService.GetConnectorStatus(Input);

            return Ok(worksapceData);
        }

    }
}
