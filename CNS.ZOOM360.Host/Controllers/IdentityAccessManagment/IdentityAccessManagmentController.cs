﻿using CNS.ZOOM360.Entities.StoreProcedures.IdentityAccessManagment;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.LoggerServices;
using CNS.ZOOM360.Shared.StoreProcedures.IdentityAccessManagment;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Host.Controllers.IdentityAccessManagment
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.IndentityAndAccessmanagement.RouteName)]
    [ApiController]
    public class IdentityAccessManagmentController : Controller
    {
        private readonly IidentityControlService _iidentityControlService;
        private readonly IPasswordControlSetupService _IPasswordControlSetupService;
        private readonly IAccessLockingService _IAccessLockingService;
        private readonly IMultiFactorAuthenticationService _IMultiFactorAuthenticationService;
        private readonly IRiskBaseAuthenticationService _IRiskBaseAuthenticationService;
        private readonly ILoggerManager _logger;
        public IdentityAccessManagmentController(
            IidentityControlService iidentityControlService,
            IPasswordControlSetupService IPasswordControlSetupService,
            IAccessLockingService IAccessLockingService,
            IMultiFactorAuthenticationService IMultiFactorAuthenticationService,
            IRiskBaseAuthenticationService iRiskBaseAuthenticationService,
            ILoggerManager logger)
        {
            _iidentityControlService = iidentityControlService;
            _IPasswordControlSetupService = IPasswordControlSetupService;
            _IAccessLockingService = IAccessLockingService;
             _IMultiFactorAuthenticationService= IMultiFactorAuthenticationService;
            _IRiskBaseAuthenticationService = iRiskBaseAuthenticationService;
            _logger = logger;
        }
        [Route(ServiceConstants.IndentityAndAccessmanagement.SaveIdentityControlSetup)]
        [HttpPost]
        public async Task<IActionResult> SaveUserAccessManagementSetup(IdentityControlModel identityControlModel)
        {
            var worksapceData = _iidentityControlService.SaveIdentityControlSetup(identityControlModel);
            if (worksapceData == null)
            {

                _logger.LogInfo($"Workspace data doesn't exist in the database.");
                return NotFound();
            }
            return Ok(worksapceData);
        }
        [Route(ServiceConstants.IndentityAndAccessmanagement.SavePasswordControlSetup)]
        [HttpPost]
        public async Task<IActionResult> SavePasswordControlSetup(PasswordControlSetupModel passwordControlSetupModel)
        {
            var worksapceData = _IPasswordControlSetupService.SavePasswordControlSetup(passwordControlSetupModel);
            if (worksapceData == null)
            {

                _logger.LogInfo($"Workspace data doesn't exist in the database.");
                return NotFound();
            }
            return Ok(worksapceData);
        }
        [Route(ServiceConstants.IndentityAndAccessmanagement.SaveAccessLockingSetup)]
        [HttpPost]
        public async Task<IActionResult> SaveAccessLockingSetup(AccessLockingModel accessLockingModel)
        {
            var worksapceData = _IAccessLockingService.SaveAccessLookingSetup(accessLockingModel);
            if (worksapceData == null)
            {

                _logger.LogInfo($"Workspace data doesn't exist in the database.");
                return NotFound();
            }
            return Ok(worksapceData);
        }
        [Route(ServiceConstants.IndentityAndAccessmanagement.SaveMultiFactorAuthenticationSetup)]
        [HttpPost]
        public async Task<IActionResult> SaveMultiFactorAuthenticationSetup(MultiFactorAuthenticationModel multiFactorAuthenticationModel)
        {
            var worksapceData = _IMultiFactorAuthenticationService.SaveMultiFactorAuthenticationSetup(multiFactorAuthenticationModel);
            if (worksapceData == null)
            {

                _logger.LogInfo($"Workspace data doesn't exist in the database.");
                return NotFound();
            }
            return Ok(worksapceData);
        }
        [Route(ServiceConstants.IndentityAndAccessmanagement.SaveRiskBasedAuthenticationSetup)]
        [HttpPost]
        public async Task<IActionResult> SaveRiskBasedAuthSetup(RiskBaseAuthenticationModel riskBaseAuthenticationModel)
        {
            var worksapceData = _IRiskBaseAuthenticationService.SaveRiskBaseAuthenticationSetup(riskBaseAuthenticationModel);
            if (worksapceData == null)
            {

                _logger.LogInfo($"Workspace data doesn't exist in the database.");
                return NotFound();
            }
            return Ok(worksapceData);
        }
    }
}
