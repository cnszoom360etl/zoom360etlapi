﻿using CNS.ZOOM360.Entities.Model;
using CNS.ZOOM360.Services.StoreProcedures.Extraction;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.LoggerServices;
using CNS.ZOOM360.Shared.StoreProcedures.Extraction;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.Extract.RouteName)]
    [ApiController]
    public class ExtractionController : ControllerBase
    {
        private readonly IExtraction _extractionService;
        private readonly ILoggerManager _logger;
        public ExtractionController(IExtraction extractionService, ILoggerManager logger)
        {
            _logger = logger;
            _extractionService = extractionService;
        }


        [Route(ServiceConstants.Extract.checkuserRights)]
        // POST: api/SqlConnector/SaveStep1Data
        [HttpPost]
        public async Task<string> checkuserRights(string connectorIds,string Account_Id,string UserId,string Workspaceid,string Clientid)
        {
            //List<string> vmessages = new List<string>();
            //// ExtractionModel[] connectorIds = new ExtractionModel();
            //////var destinationList =""; 
            var destinationList = _extractionService.CheckUserAccessRights(UserId, Account_Id, Workspaceid, Clientid, connectorIds);
            //vmessages.Add(destinationList);
            //for (int i = 0; i < connectorIds.Length; i++)
            //{

            //}
            return destinationList;
            

        }
    }
}
