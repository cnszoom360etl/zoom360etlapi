﻿using CNS.ZOOM360.Entities.StoreProcedures;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.LoggerServices;
using CNS.ZOOM360.Shared.StoreProcedures;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.Enrich.RouteName)]
    [ApiController]
    public class ValueTableDataController : ControllerBase
    {
        private readonly IValueTableDataServices _valueTableDataServices;
        private readonly ILoggerManager _logger;
        public ValueTableDataController(IValueTableDataServices ivalueTableData, ILoggerManager logger)
        {
            _valueTableDataServices = ivalueTableData;
            _logger = logger;
        }
        [Route(ServiceConstants.Enrich.SaveValeTableData)]
        [HttpPost]
        //public async Task<IActionResult> SaveWorkspaceSetup(saveValueTableModel modal)
        //{
        //    var worksapceData = _valueTableDataServices.SaveValueTable(modal);

        //    return Ok(worksapceData.Result);
        //}


        public async Task<IActionResult> saveValueTable(valueTableIDataModel[] UserData,string UserId,string workspaceId,string clientId , string valuetable,string ClientDate,string ClientTime,string ClientTimeZone)
        {

            var msg = "";
            for (var i = 0; i < UserData.Length; i++)
            {
                var worksapceData = _valueTableDataServices.SaveValueTable(UserData[i],UserId,workspaceId,clientId,valuetable, ClientDate, ClientTime, ClientTimeZone);
                msg = worksapceData.Result;
            }


            return Ok(msg);
        }
        [Route(ServiceConstants.Enrich.GetValueTableData)]
            [HttpGet]
        public async Task<IActionResult> GetValueTableData(string userId, string workspaceId, string clientId, string valueTable)
        {
            var valueTableData = _valueTableDataServices.GetValueTableData(userId,workspaceId,clientId,valueTable);
            if (valueTableData.Result.Count == 0)
            {
                _logger.LogInfo($"Extract List data doesn't exist in the database.");
                return NotFound();
            }

            return Ok(valueTableData.Result);
        }
    }
   
}
