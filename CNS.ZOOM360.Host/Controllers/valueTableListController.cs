﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CNS.ZOOM360.Entities.StoreProcedures;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.LoggerServices;
using CNS.ZOOM360.Shared.StoreProcedures;
using Microsoft.AspNetCore.Mvc;

namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.Enrich.RouteName)]
    [ApiController]
    public class valueTableListController : Controller
    {
   
            private readonly IvalueTableList _valueTableListServices;
            private readonly ILoggerManager _logger;
            public valueTableListController(IvalueTableList ivalueTableList, ILoggerManager logger)
            {
                _valueTableListServices = ivalueTableList;
                _logger = logger;
            }

            [Route(ServiceConstants.Enrich.GetValueTableList)]
            [HttpPost]
            public async Task<IActionResult> getValueTableList(valueTableInputModel Input)
            {
                var valueTableListData = _valueTableListServices.GetValueTablelist(Input);
                if (valueTableListData.Result.Count == 0)
                {
                    _logger.LogInfo($"Extract List data doesn't exist in the database.");
                    return NotFound();
                }

                return Ok(valueTableListData.Result);
            }
        }
    }
