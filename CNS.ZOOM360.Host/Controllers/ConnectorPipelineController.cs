﻿using CNS.ZOOM360.Entities.Model;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.LoggerServices;
using CNS.ZOOM360.Shared.StoreProcedures.piplineConnector;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.ConnectorPipeline.RouteName)]
    [ApiController]
    public class ConnectorPipelineController : ControllerBase
    {
        private readonly IConnectorPipeline _IConnectorpipeline;
        private readonly ILoggerManager _logger;
        public ConnectorPipelineController(IConnectorPipeline IConnectorpipeline, ILoggerManager logger)
        {
            _IConnectorpipeline = IConnectorpipeline;
            _logger = logger;
        }

        [Route(ServiceConstants.ConnectorPipeline.SaveConnectorType)]
        [HttpPost]
        public async Task<IActionResult> SaveConnectorType(ConnectorPiplineModel pipline)
        {
            var savepipelineList = _IConnectorpipeline.save(pipline);
            //if (destinationList.Result.Count == 0)
            //{

            //    _logger.LogInfo($"Destination List doesn't exist in the database.");
            //    return NotFound();
            //}

            return Ok(savepipelineList.Result);
        }

        [Route(ServiceConstants.ConnectorPipeline.GetConnectorList)]
        [HttpGet]
        public async Task<IActionResult> GetConnectorList(string pipelineId, string UserId, string WorkspaceId, string ClientId)
        {
            var pipelineList = _IConnectorpipeline.GetData(pipelineId,   UserId,   WorkspaceId,   ClientId);
            //if (destinationList.Result.Count == 0)
            //{

            //    _logger.LogInfo($"Destination List doesn't exist in the database.");
            //    return NotFound();
            //}

            return Ok(pipelineList.Result);
        }
    }
}
