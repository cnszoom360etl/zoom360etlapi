﻿using CNS.ZOOM360.Entities.Model;
using CNS.ZOOM360.Entities.StoreProcedures.Connectors.Databases.SQL;
using Google.Apis.Auth.AspNetCore3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Download;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using GoogleDriveAPiForWEB.ModelRepository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleDriveAPiForWEB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly ILogger<ValuesController> _logger;
        private IWebHostEnvironment _hostingEnvironment;
        private IHostingEnvironment hostingEnv;
        private IGoogleAuthProvider _drive;
        List<Google.Apis.Drive.v3.Data.File> filesdata = new List<Google.Apis.Drive.v3.Data.File>();
        DriveService services = new DriveService();
        DataLayer d = new DataLayer();
        public ValuesController(ILogger<ValuesController> logger, IGoogleAuthProvider drive, IWebHostEnvironment environment, IHostingEnvironment env)
        {
            _logger = logger;
            this.hostingEnv = env;
            _logger = logger;
            _drive = drive;
            _hostingEnvironment = environment;
        }
        [HttpGet]
        [GoogleScopedAuthorize(DriveService.ScopeConstants.Drive)]
        public IActionResult Drive([FromServices] IGoogleAuthProvider auth, string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId)
        {
            DriveFileList(auth, Account_Id,  UserId,  Workspaceid,  Clientid,  ConnectorId);
            return Ok();
        }

        
        private async Task<List<UpdateConnectorListStep2>> DriveFileList([FromServices] IGoogleAuthProvider auth, string Account_Id, string UserId, string Workspaceid, string Clientid, string ConnectorId)
        {
            var GetfileStatus = "NO";
            var filename = "";
            var filevalue = "";
            //string Account_Id = "9015";
            //string UserId = "Admin";
            //string Workspaceid = "1";
            //string Clientid = "1002";
            //string ConnectorId = "222";

            GoogleCredential cred = await auth.GetCredentialAsync();
            services = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = cred
            });
            List<UpdateConnectorListStep2> list = new List<UpdateConnectorListStep2>();
            list = d.GetConnectorData(Account_Id, UserId, Workspaceid, Clientid, ConnectorId);
            var files = await services.Files.List().ExecuteAsync();
            filesdata = files.Files.ToList();
            foreach (var item in list)
            {
                filename = item.Filedname;
                filevalue = item.Fieldvalue;

            }

            if (files != null && filesdata.Count > 0)
            {
                foreach (var file in filesdata)
                {

                    if (file.Name == filename)
                    {
                        GetfileStatus = "YES";
                        GoogleDriveFiles File = new GoogleDriveFiles
                        {
                            Id = file.Id,
                            Name = file.Name,
                            Size = file.Size,
                            Version = file.Version,
                            CreatedTime = file.CreatedTime

                        };
                        DownloadFileforserver(File.Id);



                    }


                }


            }
            if (GetfileStatus == "NO")
            {

                foreach (var item in list)
                {
                    item.Filedname = "File Not Found";
                }

                return list;

            }
            return list;
        }


        private string DownloadFileforserver(string id)
        {
            string FilePath = DownloadGoogleFile(id);
            //string FolderPath = Path.Combine("Resources", "files");
            ////var FolderPath1 = Path.Combine(Directory.GetCurrentDirectory(), FolderPath) + Path.GetFileName(FilePath);
            //var FolderPath1 = Path.GetFileName(FilePath);
            //var bytes = System.Text.Encoding.UTF8.GetBytes(FolderPath1);
            //Response.ContentType = "application/zip";

            //Response.Headers.Add("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            //// Response.Body.Read(bytes);
            //Response.Body.Flush();
            return "Done";
        }

        private string DownloadGoogleFile(string fileId)
        {

            string serverpath = _hostingEnvironment.ContentRootPath;
            //string FolderPath = System.Web.HttpContext.Current.Server.MapPath("/GoogleDriveFiles/");
            string FolderPath = Path.Combine("Resources", "files");
            //var FolderPath1 = Path.Combine(Directory.GetCurrentDirectory(), FolderPath);
            string fileserversavepath = @"//192.168.223.100/zoom_files_dump/";
            FilesResource.GetRequest request = services.Files.Get(fileId);

            string FileName = request.Execute().Name;
            string FilePath = System.IO.Path.Combine(fileserversavepath, FileName);
            string FilePathforlocal = System.IO.Path.Combine(FolderPath, FileName);
            MemoryStream stream1 = new MemoryStream();
            MemoryStream stream2 = new MemoryStream();
            request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
            {
                switch (progress.Status)
                {
                    case DownloadStatus.Downloading:
                        {
                            //Console.WriteLine(progress.BytesDownloaded);
                            break;
                        }
                    case DownloadStatus.Completed:
                        {
                            //Console.WriteLine("Download complete.");
                            SaveStreamonserver(stream1, FilePath);
                            SaveStreamforlocal(stream2, FilePathforlocal);
                            break;
                        }
                    case DownloadStatus.Failed:
                        {
                            //Console.WriteLine("Download failed.");
                            break;
                        }
                }
            };
            request.Download(stream1);
            request.Download(stream2);
            return FilePath;
        }

        private void SaveStreamonserver(MemoryStream stream, string FilePath)
        {
            using (System.IO.FileStream file = new FileStream(FilePath, FileMode.Create, FileAccess.ReadWrite))
            {
                stream.WriteTo(file);
            }
        }
        private void SaveStreamforlocal(MemoryStream stream, string Path)
        {
            using (FileStream file = new FileStream(Path, FileMode.Create))
            {
                stream.WriteTo(file);
            }

        }
    }
}
