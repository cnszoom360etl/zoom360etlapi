﻿using CNS.ZOOM360.Entities.StoreProcedures.Connectors.Databases.SQL;
using CNS.ZOOM360.Services.StoreProcedures.MongoDB;
using CNS.ZOOM360.Shared.Const;
using CNS.ZOOM360.Shared.StoreProcedures.MongoDBConnection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CNS.ZOOM360.Host.Controllers
{
    [Route(ServiceConstants.ApiPrefix + ServiceConstants.checkMongoCon.RouteName)]
    [ApiController]
    public class MongoDBConnectionStatus : ControllerBase
    {
        private readonly ImongoConnection _context;
        protected List<string> _dbCollection;
        private IMongoDatabase database;
       
        public MongoDBConnectionStatus(ImongoConnection context)
        {
            _context = context;

            //_dbCollection = context.GetCollections();
             
        }

        [Route(ServiceConstants.checkMongoCon.getstatus)]
        [HttpPost]
        public bool connectionstatus(SOURCE_CNF obj,string connectortitle,string connectorId, string Account_Id, string userId, string ClientId, string workspaceId)
        {
               
             var status = _context.status(obj.Hostname, obj.UserName, obj.Password, obj.PortNumber, obj.Database , connectortitle , connectorId , Account_Id , userId, ClientId, workspaceId);
            return status;
        }




    }
}
